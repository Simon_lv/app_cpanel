package com.baplay.schedule;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import tw.tw360.common.encrypt.MD5Util;

import com.alibaba.fastjson.JSONObject;
import com.baplay.constant.URL;
import com.baplay.entity.PushRecord;
import com.baplay.service.IPushManager;
import com.baplay.util.HttpUtil;

@Component
public class ScheduledTaskManager {
	private final Object PUSH_LOCK = new Object();
	private static Logger log = LoggerFactory
			.getLogger(ScheduledTaskManager.class);
	@Autowired
	private IPushManager pushManager;
	
	@Scheduled(cron = "0 0/3 * * * ? ")
	public void scheulePush() {
		log.info("three minute run one scheulePush task : heartbeat....");
		synchronized (PUSH_LOCK) {
			log.info("scheulePush in sync block");
			List<PushRecord> pushRecords = pushManager.scheulePush();
			
			for(PushRecord pr : pushRecords) {
				Map<String, String> map = new HashMap<String, String>();
				try {
					String id = pr.getId().toString();
					String key = pushManager.getAppKey("app");
					String token = MD5Util.crypt(id+key);
					map.put("appGameCode", "app");
					map.put("id", id);
					map.put("token", token);
					JSONObject jsonObject = HttpUtil.sendGetByMapForm(URL.URL_PUSH.toString(), map );
					
					if(jsonObject!=null){
						log.info("[scheulePush]result="+jsonObject.toJSONString());
					}					
					
				} catch (UnsupportedEncodingException e) {
					log.info("uri:"+URL.URL_PUSH.toString()+"\tmap:"+map);
					e.printStackTrace();
				}
			}
			
		}
		
	}
}
