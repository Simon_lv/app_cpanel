package com.baplay.entity;

import com.baplay.dto.BaseDto;

public class RoleFunction extends BaseDto {
	/**
	 * 
	 */
	private static final long serialVersionUID = -994797204643504794L;
	private Long roleId;
	private Long functionId;

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getFunctionId() {
		return functionId;
	}

	public void setFunctionId(Long functionId) {
		this.functionId = functionId;
	}
}
