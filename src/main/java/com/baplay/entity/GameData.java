package com.baplay.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.baplay.dto.BaseDto;

public class GameData extends BaseDto {
	private static final long serialVersionUID = 1L;

	private String gameCode;
	private String gameName;
	private Long companyId;
	private Double licensingFee;
	private String lfCurrency;
	private Float lfRate;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date lfDate;
	private String lfEndMonth;
	private Float advanceRate;
	private Integer splitMode;
	private String stepCurrency;
	private Double step1Money;
	private Float step1Scale;
	private Double step2Money;
	private Float step2Scale;
	private String gameCodes; // for order

	private String modifier;
	private Date updateTime;
	private String creator;
	private Date createTime;
	private Integer status;

	public String getGameCode() {
		return gameCode;
	}

	public void setGameCode(String gameCode) {
		this.gameCode = gameCode;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Double getLicensingFee() {
		return licensingFee;
	}

	public void setLicensingFee(Double licensingFee) {
		this.licensingFee = licensingFee;
	}

	public String getLfCurrency() {
		return lfCurrency;
	}

	public void setLfCurrency(String lfCurrency) {
		this.lfCurrency = lfCurrency;
	}

	public Float getLfRate() {
		return lfRate;
	}

	public void setLfRate(Float lfRate) {
		this.lfRate = lfRate;
	}

	public Date getLfDate() {
		return lfDate;
	}

	public void setLfDate(Date lfDate) {
		this.lfDate = lfDate;
	}

	public String getLfEndMonth() {
		return lfEndMonth;
	}

	public void setLfEndMonth(String lfEndMonth) {
		this.lfEndMonth = lfEndMonth;
	}

	public Float getAdvanceRate() {
		return advanceRate;
	}

	public void setAdvanceRate(Float advanceRate) {
		this.advanceRate = advanceRate;
	}

	public Integer getSplitMode() {
		return splitMode;
	}

	public void setSplitMode(Integer splitMode) {
		this.splitMode = splitMode;
	}

	public String getStepCurrency() {
		return stepCurrency;
	}

	public void setStepCurrency(String stepCurrency) {
		this.stepCurrency = stepCurrency;
	}

	public Double getStep1Money() {
		return step1Money;
	}

	public void setStep1Money(Double step1Money) {
		this.step1Money = step1Money;
	}

	public Float getStep1Scale() {
		return step1Scale;
	}

	public void setStep1Scale(Float step1Scale) {
		this.step1Scale = step1Scale;
	}

	public Double getStep2Money() {
		return step2Money;
	}

	public void setStep2Money(Double step2Money) {
		this.step2Money = step2Money;
	}

	public Float getStep2Scale() {
		return step2Scale;
	}

	public void setStep2Scale(Float step2Scale) {
		this.step2Scale = step2Scale;
	}

	public String getGameCodes() {
		return gameCodes;
	}

	public void setGameCodes(String gameCodes) {
		this.gameCodes = gameCodes;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}
