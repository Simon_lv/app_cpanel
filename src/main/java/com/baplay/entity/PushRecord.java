package com.baplay.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.baplay.dto.BaseDto;

public class PushRecord extends BaseDto {
	private static final long serialVersionUID = 1L;
	private String title;
	private String content;
	private int type;
	private String traceId;
	private String imgUrl;
	private String modifier;
	private String creator;
	private int status;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date pushTime;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date runTime;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date iosRunTime;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getTraceId() {
		return traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}

	public Date getPushTime() {
		return pushTime;
	}

	public void setPushTime(Date pushTime) {
		this.pushTime = pushTime;
	}

	public Date getRunTime() {
		return runTime;
	}

	public void setRunTime(Date runTime) {
		this.runTime = runTime;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getIosRunTime() {
		return iosRunTime;
	}

	public void setIosRunTime(Date iosRunTime) {
		this.iosRunTime = iosRunTime;
	}
}
