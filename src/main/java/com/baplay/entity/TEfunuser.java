package com.baplay.entity;

import java.io.Serializable;
import java.util.Date;

import com.baplay.dto.BaseDto;

/**
 * TEfunuser entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TEfunuser extends BaseDto implements Serializable {

	private static final long serialVersionUID = 1L;
	// Fields
	private Long userid;
	private String username;
	private String funname;
	private String mobile;
	private Integer status;
	private String cruser;
	private Date cretime;
	private String password;
	private String remake;
	private Long delesign;
	private Long deptid;
	private String gameCode;
	private String icon;

	private String name;

	// Constructors

	/** default constructor */
	public TEfunuser() {
	}

	/** full constructor */
	public TEfunuser(String username, String funname, String mobile,
			Integer status, String cruser, Date cretime, String password,
			String remake, Long delesign, Long deptid, String gameCode,
			String icon) {
		this.username = username;
		this.funname = funname;
		this.mobile = mobile;
		this.status = status;
		this.cruser = cruser;
		this.cretime = cretime;
		this.password = password;
		this.remake = remake;
		this.delesign = delesign;
		this.deptid = deptid;
		this.gameCode = gameCode;
		this.icon = icon;
	}

	// Property accessors

	public Long getUserid() {
		return this.userid;
	}

	public void setUserid(Long userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFunname() {
		return this.funname;
	}

	public void setFunname(String funname) {
		this.funname = funname;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCruser() {
		return this.cruser;
	}

	public void setCruser(String cruser) {
		this.cruser = cruser;
	}

	public Date getCretime() {
		return this.cretime;
	}

	public void setCretime(Date cretime) {
		this.cretime = cretime;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRemake() {
		return this.remake;
	}

	public void setRemake(String remake) {
		this.remake = remake;
	}

	public Long getDelesign() {
		return this.delesign;
	}

	public void setDelesign(Long delesign) {
		this.delesign = delesign;
	}

	public Long getDeptid() {
		return this.deptid;
	}

	public void setDeptid(Long deptid) {
		this.deptid = deptid;
	}

	public String getGameCode() {
		return this.gameCode;
	}

	public void setGameCode(String gameCode) {
		this.gameCode = gameCode;
	}

	public String getIcon() {
		return this.icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}