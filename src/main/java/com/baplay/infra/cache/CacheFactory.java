package com.baplay.infra.cache;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.danga.MemCached.MemCachedClient;
import com.danga.MemCached.SockIOPool;

public class CacheFactory {

	private static final Map<String, Cache> map = new ConcurrentHashMap();
	private static final Logger logger = LoggerFactory.getLogger(CacheFactory.class);
	  
	  static
	  {
	    CompositeConfiguration config = new CompositeConfiguration();
	    try
	    {
	      config.addConfiguration(new PropertiesConfiguration(
	        "cache.properties"));
	      logger.info("加載cache.properties....");
	    }
	    catch (ConfigurationException e)
	    {
	      logger.info("can't find cache.properties" + e);
	    }
	    String serverStr = config.getString("servers", "");
	    
	    List<String> servers = new ArrayList();
	    for (String s : serverStr.split(","))
	    {
	      s = s.trim();
	      if (StringUtils.isNotEmpty(s)) {
	        servers.add(s);
	      }
	    }
	    if (servers.size() < 1) {
	      throw new RuntimeException("");
	    }
	    SockIOPool pool = SockIOPool.getInstance();
	    pool.setServers((String[])servers.toArray(new String[0]));
	    pool.setFailover(Boolean.valueOf(config.getString("failover", "true")).booleanValue());
	    pool.setInitConn(Integer.valueOf(config.getString("initConn", "100")).intValue());
	    pool.setMinConn(Integer.valueOf(config.getString("minConn", "25")).intValue());
	    pool.setMaxConn(Integer.valueOf(config.getString("maxConn", "250")).intValue());
	    pool.setMaintSleep(Integer.valueOf(config.getString("maintSleep", "30")).intValue());
	    pool.setNagle(Boolean.valueOf(config.getString("nagle", "false")).booleanValue());
	    pool.setSocketTO(Integer.valueOf(config.getString("socketTO", "3000")).intValue());
	    pool.setAliveCheck(Boolean.valueOf(config.getString("aliveCheck", 
	      "true")).booleanValue());
	    pool.setHashingAlg(Integer.valueOf(config.getString("hashingAlg", "0")).intValue());
	    pool.setSocketConnectTO(Integer.valueOf(config.getString(
	      "socketConnectTO", "3000")).intValue());
	    String wStr = config.getString("weights", "");
	    Object weights = new ArrayList();
	    for (String s : wStr.split(","))
	    {
	      s = s.trim();
	      if (StringUtils.isNotEmpty(s)) {
	        ((List)weights).add(Integer.valueOf(s));
	      }
	    }
	    if (((List)weights).size() == servers.size()) {
	      pool.setWeights((Integer[])((List)weights).toArray(new Integer[0]));
	    }
	    logger.info("緩存池 pool.initialize....");
	    pool.initialize();
	  }
	  
	  private static MemCachedClient memCachedClient = new MemCachedClient();
	  
	  public static <T> Cache<T> getCommonCache(Class<T> t)
	  {
	    Cache<T> cache = (Cache)map.get(t.getName());
	    if (cache == null) {
	      cache = createCommonCache(t);
	    }
	    return cache;
	  }
	  
	  private static synchronized <T> Cache<T> createCommonCache(Class<T> t)
	  {
	    Cache<T> cache = (Cache)map.get(t.getName());
	    if (cache == null)
	    {
	      cache = new CommonCache(t, memCachedClient);
	      map.put(t.getName(), cache);
	    }
	    return cache;
	  }
}
