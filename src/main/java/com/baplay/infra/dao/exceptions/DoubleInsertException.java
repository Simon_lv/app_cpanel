package com.baplay.infra.dao.exceptions;

import com.baplay.infra.dao.enums.IEnum;

/**
 * 自定义异常类
 */
public class DoubleInsertException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	protected String resultCode = "UN_KNOWN_EXCEPTION";
	
	protected String resultMsg = "未知異常";
	
	public DoubleInsertException() {
		super();
	}
	
	public DoubleInsertException(IEnum e) {
		super(e.getDesc());
		this.resultCode = e.getCode();
		this.resultMsg = e.getDesc();
	}
	
	public DoubleInsertException(Throwable e) {
		super(e);
		this.resultMsg = e.getMessage();
	}
	
	public DoubleInsertException(String message) {
		super(message);
		
	}
	
	public DoubleInsertException(String code, String message) {
		super(message);
		this.resultCode = code;
		this.resultMsg = message;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultMsg() {
		return resultMsg;
	}

	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	
}
