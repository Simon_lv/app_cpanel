package com.baplay.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springside.modules.orm.Page;

import com.baplay.constant.Config;
import com.baplay.entity.ExchangeRecord;
import com.baplay.form.ExchangeRecordForm;
import com.baplay.service.IExchangeRecordManager;

@Controller
@RequestMapping("/exchangeRecord")
public class ExchangeRecordController {

	private static final Logger LOG = LoggerFactory.getLogger(ExchangeRecordController.class);

	@Autowired
	private IExchangeRecordManager exchangeRecordManager;
	
	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		return "/exchangeRecord/init";
	}
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request, ExchangeRecordForm exchangeRecordForm) throws Exception {
		exchangeRecordForm.setPageSize(Config.PAGE_SIZE);
		exchangeRecordForm.setExport(false);
		Page<ExchangeRecord> page = exchangeRecordManager.list(exchangeRecordForm);
		
		request.setAttribute("page", page);
		return "/exchangeRecord/list";
	}
	
}
