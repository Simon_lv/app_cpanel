package com.baplay.web.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springside.modules.orm.Page;

import com.baplay.constant.Config;
import com.baplay.constant.URL;
import com.baplay.entity.PushRecord;
import com.baplay.entity.TEfunuser;
import com.baplay.form.PushForm;
import com.baplay.service.IPushManager;
import com.baplay.util.HandleFileUpload;
import com.baplay.util.HttpUtil;
import com.google.common.collect.Maps;

import tw.tw360.common.encrypt.MD5Util;

@Controller
@RequestMapping("/push")
public class PushController {
	private static final Logger LOG = LoggerFactory.getLogger(PushController.class);
	private static final int PAGE_SIZE = 20;
	
	@Autowired
	private IPushManager pushManager;
	
	@RequestMapping("/init")
	public String init(HttpSession session, Model model) {
		return "push/init";
	}
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request, PushForm deviceForm) {
		deviceForm.setPageSize(PAGE_SIZE);
		deviceForm.setExport(false);
		Page<PushRecord> page = pushManager.list(deviceForm);
		request.setAttribute("page", page);
		return "/push/list";
	}
	
	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		if(id != null) {
			request.setAttribute("push", pushManager.findOneById(id));
		}
		return "/push/edit";
	}
	
	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(HttpServletRequest request, PushRecord pushRecord) {
		Map<String, Object> result = Maps.newHashMap();

		boolean flag = false;
		if(pushRecord != null) {
			TEfunuser user = (TEfunuser) SecurityUtils.getSubject().getSession().getAttribute("loginUser");
			if(pushRecord.getId() == null || pushRecord.getId() <= 0) {
				pushRecord.setCreator(user.getUsername());
				pushRecord = pushManager.add(pushRecord);
				flag = (pushRecord!=null);
			} else {
				pushRecord.setModifier(user.getUsername());
				flag = (pushManager.update(pushRecord)>0);
			}
		}
		if (flag) {
			result.put("code", "0000");
			result.put("id", pushRecord.getId());
			result.put("message", "保存成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");	
		}
		return result;
	}
	
	@RequestMapping(value="/upload", method=RequestMethod.POST)
	public @ResponseBody HashMap<String, String> handleFileUpload( @RequestParam("file") MultipartFile file,
			@RequestParam(value="json", required = false, defaultValue = "") String json) throws IOException, Exception{
		HashMap<String, String> result = new HashMap<String, String>();
		LOG.info("[PushController.handleFileUpload][filename="+file.getOriginalFilename()+"]");
		String url = HandleFileUpload.upload(file, URL.URL_IMG_PUSH.toString(), URL.FTP_IMG_PUSH.toString(), Config.UPLOAD_VIDEO_FILE_LIMIT);
		
		// Parse Json
		if(StringUtils.isNotEmpty(json)) {
			LOG.info("json:" + json);
			JSONObject jsonObj = new JSONObject(json);
			LOG.info("jsonObj:" + jsonObj.toString());
		}
		
		result.put("url", url);
		return result;
    }
	
	@RequestMapping("/delete")
	public @ResponseBody Map<String, Object> deleteCP(HttpServletRequest request, Long id) {
		Map<String, Object> result = Maps.newHashMap();

		boolean flag = false;
		if (id != null && id > 0) {
			TEfunuser user = (TEfunuser) SecurityUtils.getSubject().getSession().getAttribute("loginUser");
			flag = (pushManager.delete(id, user.getUsername()) > 0);
		}
		if (flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		
		return result;
	}
	
	@RequestMapping("/doPush")
	public @ResponseBody Map<String, Object> doPush(HttpServletRequest request, PushRecord pushRecord) {
		Map<String, Object> result = Maps.newHashMap();

		boolean flag = false;
		
		if(pushRecord != null) {
			final Object PUSH_LOCK = new Object();
			
			PushRecord pr = pushManager.findOneById(pushRecord.getId());
			
			synchronized (PUSH_LOCK) {
				Map<String, String> map = new HashMap<String, String>();
				
				try {
					String id = pr.getId().toString();
					String key = pushManager.getAppKey("app");
					String token = MD5Util.crypt(id+key);
					map.put("appGameCode", "app");
					map.put("id", id);
					map.put("token", token);
					
					com.alibaba.fastjson.JSONObject jsonObject = HttpUtil.sendGetByMapForm(URL.URL_PUSH.toString(), map );
					
					LOG.info("[PushController doPush]result="+jsonObject.toJSONString());
					
					if(jsonObject.getString("code").equals("0000")){
						flag = true;
					}
					
				} catch (UnsupportedEncodingException e) {
					LOG.error("[PushController][doPush]", e);					
				}
			}
			
		}
		if (flag) {
			result.put("code", "0000");
			result.put("id", pushRecord.getId());
			result.put("message", "推播成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "推播失敗！");	
		}
		return result;
	}
	
}
