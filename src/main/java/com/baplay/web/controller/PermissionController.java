package com.baplay.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.baplay.entity.Function;
import com.baplay.entity.Role;
import com.baplay.entity.TEfunuser;
import com.baplay.form.RoleForm;
import com.baplay.form.UserForm;
import com.baplay.service.IPermissionManager;
import com.baplay.util.PrintUtil;

@Controller
@RequestMapping("/permission")
public class PermissionController {
	private static final Logger LOG = LoggerFactory
			.getLogger(PermissionController.class);
	private static final String TAG = "PermissionController";
	@Autowired
	private IPermissionManager manager;

	@RequestMapping("/roleList")
	public String roleList(HttpSession session, Model model) {
		List<Role> roles = null;
		List<Function> functions = null;
		functions = manager.getFunctions(0, 1);
		roles = manager.getRoles(0);
		model.addAttribute("roles", roles);
		model.addAttribute("functions", functions);
		return "permission/roleList";
	}

	@RequestMapping("/getFunctions")
	@ResponseBody
	public List<Function> getFunctions(
			@RequestParam(required = true) Long roleId) {
		List<Function> functions = null;
		functions = manager.getFunctions(roleId, 0);
		return functions;
	}

	@RequestMapping("/addRole")
	@ResponseBody
	public Map<String, String> addRole(RoleForm form) {
		try {
			manager.addRole(form);
		} catch (Exception e) {
			LOG.error(e.getMessage());
			e.printStackTrace();
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("code", "0");
		map.put("msg", "success");
		return map;
	}

	@RequestMapping("/updateRole")
	@ResponseBody
	public Map<String, String> updateRole(RoleForm form) {
		manager.updateRole(form);
		Map<String, String> map = new HashMap<String, String>();
		map.put("code", "0");
		map.put("msg", "success");
		return map;
	}

	@RequestMapping("/updateRoleStatus")
	public String updateRoleStatus(@RequestParam(required = true) Long roleId,
			@RequestParam(required = true) Integer status) {
		try {
			manager.updateRoleStatus(roleId, status);
		} catch (Exception e) {
			LOG.error(e.getMessage());
			e.printStackTrace();
		}
		return "redirect:/permission/roleList";
	}

	@RequestMapping("/userList")
	public String userList(HttpSession session, String userName, Model model,
			HttpServletRequest request) throws Exception {
		PrintUtil.printRequest(TAG, "userList", request);
		List<TEfunuser> list = manager.getUsers(userName);

		model.addAttribute("list", list);
		return "permission/userList";
	}

	@RequestMapping("/updateUserStatus")
	public String updateUserStatus(
			@RequestParam(required = true) Integer status,
			@RequestParam(required = true) Long userId) {
		try {
			manager.updateUserStatus(status, userId);
		} catch (Exception e) {
			LOG.error(e.getMessage());
			e.printStackTrace();
		}
		return "redirect:/permission/userList";
	}

	@RequestMapping("/getUser")
	public ModelAndView getUser(ModelAndView mv, Long id) {
		UserForm user = manager.getUser(id);
		List<Role> roles = manager.getRoles(1);
		mv.addObject("user", user);
		mv.addObject("roles", roles);
		mv.setViewName("permission/userDetail");
		return mv;
	}

	@RequestMapping("/saveOrUpdate")
	@ResponseBody
	public Map<String, String> addUser(UserForm form, HttpServletRequest request)
			throws Exception {
		PrintUtil.printRequest(TAG, "addUser", request);
		if (form.getUserId() == null) {
			manager.addUser(form);
		} else {
			manager.updateUser(form);
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("code", "0");
		map.put("msg", "success");
		return map;
	}

	@RequestMapping("/updateUserPwdInit")
	public String updateUserPwdInit() {
		return "permission/updateUserPwd";
	}

	@RequestMapping("/updateUserPwd")
	@ResponseBody
	public Map<String, String> updateUserPwd(
			@RequestParam(required = true) Long userId,
			@RequestParam(required = true) String password) {
		manager.updatePwd(userId, password);
		Map<String, String> map = new HashMap<String, String>();
		map.put("code", "0");
		map.put("msg", "success");
		return map;
	}
}
