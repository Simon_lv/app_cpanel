package com.baplay.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springside.modules.orm.Page;

import com.baplay.constant.Config;
import com.baplay.constant.URL;
import com.baplay.entity.Banner;
import com.baplay.entity.TEfunuser;
import com.baplay.form.BannerForm;
import com.baplay.service.impl.BannerManager;
import com.baplay.util.HandleFileUpload;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/banner")
public class BannerController {
	private static final Logger LOG = LoggerFactory.getLogger(BannerController.class);
	private static final int PAGE_SIZE = 25;

	@Autowired
	private BannerManager bannerManager;

	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
//		request.setAttribute("games", Games.getMap3());
		return "/banner/init";
	}

	@RequestMapping("/list")
	public String list(HttpServletRequest request, BannerForm bannerForm) {
		bannerForm.setPageSize(PAGE_SIZE);
		bannerForm.setExport(false);
		Page<Banner> page = bannerManager.list(bannerForm);
		
		request.setAttribute("page", page);
		return "/banner/list";
	}

	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		if (id != null) {
			Banner banner = bannerManager.findOneById(id);
			request.setAttribute("cp", banner);
		}
		
		return "/banner/edit";
	}

	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(HttpServletRequest request, Banner banner) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		if (banner != null) {
			if (banner.getId() == null || banner.getId() <= 0) {
				flag = (bannerManager.add(banner) != null);
			} else {
				flag = (bannerManager.update(banner) > 0);
			}
		}
		
		if (flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		return result;
	}

	@RequestMapping("/delete")
	public @ResponseBody Map<String, Object> deleteCP(HttpServletRequest request, Long id) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		if (id != null && id > 0) {
			TEfunuser user = (TEfunuser) SecurityUtils.getSubject().getSession().getAttribute("loginUser");
			flag = (bannerManager.delete(id, user.getUsername()) > 0);
		}
		if (flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		return result;
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody HashMap<String, String> handleFileUpload(@RequestParam("file") MultipartFile file,
			@RequestParam(value = "json", required = false, defaultValue = "") String json) throws IOException, Exception {
		HashMap<String, String> result = new HashMap<String, String>();
		LOG.info("[Bannerntroller.handleFileUpload][filename=" + file.getOriginalFilename() + "]");
		String url = HandleFileUpload.upload(file, URL.URL_IMG_BANNER.toString(), URL.FTP_IMG_BANNER.toString(), Config.UPLOAD_VIDEO_FILE_LIMIT);
		if (StringUtils.isNotEmpty(json)) {
			LOG.info("json:" + json);
			JSONObject jsonObj = new JSONObject(json);
			LOG.info("jsonObj:" + jsonObj.toString());
		}
		result.put("url", url);
		return result;
	}

}
