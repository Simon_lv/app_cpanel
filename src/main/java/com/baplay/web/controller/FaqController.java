package com.baplay.web.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import com.baplay.constant.Config;
import com.baplay.entity.Faq;
import com.baplay.entity.TEfunuser;
import com.baplay.form.FaqForm;
import com.baplay.service.IFaqManager;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/faq")
public class FaqController {

	@Autowired
	private IFaqManager faqManager;
	
	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		return "/faq/init";
	}
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request, FaqForm faqForm) throws Exception {
		faqForm.setPageSize(Config.PAGE_SIZE);
		faqForm.setExport(false);
		Page<Faq> page = faqManager.list(faqForm);
		
		request.setAttribute("page", page);
		return "/faq/list";
	}
	
	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		if (id != null) {
			Faq faq = faqManager.findOneById(id);
			request.setAttribute("cp", faq);
		}
		
		return "/faq/edit";
	}
	
	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(HttpServletRequest request, Faq faq) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		if (faq != null) {
			TEfunuser user = (TEfunuser) SecurityUtils.getSubject().getSession().getAttribute("loginUser");
						
			if (faq.getId() == null || faq.getId() <= 0) {
				faq.setCreator(user.getUserid().toString());
				
				flag = (faqManager.save(faq) != null);
			} else {
				faq.setModifier(user.getUserid().toString());
				
				flag = (faqManager.update(faq) > 0);
			}			
		}
		
		if (flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		return result;
	}
	
	@RequestMapping("/delete")
	public @ResponseBody Map<String, Object> deleteCP(HttpServletRequest request, Long id) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		if (id != null && id > 0) {			
			flag = (faqManager.delete(id) > 0);
		}
		if (flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		return result;
	}
	
}
