/**
 * 
 */
package com.baplay.web.controller;

import com.baplay.infra.utils.DateUtil;
import com.baplay.util.FtpUtil;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Date;
import java.util.Map;

/**
 * @author Haozi
 *
 */
@Controller
@RequestMapping("/fileUpload")
public class FileUploadController {
	private static final Logger LOG = LoggerFactory.getLogger(FileUploadController.class);

	@RequestMapping("/save")
	@ResponseBody
	public Map<String, Object> upload(MultipartFile file, String module) {
		Map<String, Object> result = Maps.newHashMap();
		String realPath = System.getProperty("java.io.tmpdir");
		if (Strings.isNullOrEmpty(module)) {
			result.put("code", "1004");
			result.put("error", 1);//kindeditor要求
			result.put("message", "模块名必填");
			return result;
		}
		if (file != null && !file.isEmpty()) {
			String type = file.getContentType();
			String filename = String.valueOf(new Date().getTime());
			if (type.equals("image/jpeg")) {
				filename = filename.concat(".jpg");
			} else if (type.equals("image/png")) {
				filename = filename.concat(".png");
			} else if (type.equals("image/bmp")) {
				filename = filename.concat(".bmp");
			} else if (type.equals("image/gif")) {
				filename = filename.concat(".gif");
			} else {
				LOG.error("文件類型不正確");
				result.put("code", "1001");
				result.put("error", 1);//kindeditor要求
				result.put("message", "文件類型不正確！");
				return result;
			}
			File f = new File(realPath, filename);
			System.out.println("上傳目錄======" + f.getAbsolutePath());
			String dateStr = DateUtil.format(new Date(), "yyyy-MM-dd");
			try {
				FileUtils.copyInputStreamToFile(file.getInputStream(), f);
				FtpUtil.upload8888(module + "/" + dateStr, f);
			} catch (Exception e) {
				LOG.error("文件上傳失敗", e);
				result.put("code", "1002");
				result.put("error", 1);//kindeditor要求
				result.put("message", "文件上傳失敗!");
				return result;
			} finally {
				f.delete();
			}
			String url = "http://pic.8888play.com/8888play/" + module + "/" + dateStr + "/" + filename;

			result.put("code", "0000");
			result.put("message", "上传成功！");

			result.put("error",0);
			result.put("url", url);

			return result;
		}
		result.put("code", "1003");
		result.put("error", 1);//kindeditor要求
		result.put("message", "文件不存在！");
		return result;
	}

	@RequestMapping("/saveTemp")
	@ResponseBody
	public Map<String, Object> saveTemp(MultipartFile file) {
		Map<String, Object> result = Maps.newHashMap();
		String realPath = System.getProperty("java.io.tmpdir");
		if (file != null && !file.isEmpty()) {
			String filename = file.getOriginalFilename();
			File f = new File(realPath, filename);
			System.out.println("上傳目錄======" + f.getAbsolutePath());
			try {
				FileUtils.copyInputStreamToFile(file.getInputStream(), f);
			} catch (Exception e) {
				LOG.error("文件上傳失敗", e);
				result.put("code", "1002");
				result.put("message", "文件上傳失敗!");
				return result;
			}
			result.put("code", "1000");
			result.put("message", "上传成功！");
			result.put("url", f.getAbsolutePath());
			return result;
		}
		result.put("code", "1003");
		result.put("message", "文件不存在！");
		return result;
	}

}
