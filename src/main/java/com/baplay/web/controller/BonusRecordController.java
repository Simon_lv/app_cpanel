package com.baplay.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springside.modules.orm.Page;

import com.baplay.constant.Config;
import com.baplay.entity.BonusRecord;
import com.baplay.form.BonusRecordForm;
import com.baplay.service.IBonusRecordManager;

@Controller
@RequestMapping("/bonusRecord")
public class BonusRecordController {
	
	@Autowired
	private IBonusRecordManager bonusRecordManager;
	
	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		return "/bonusRecord/init";
	}
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request, BonusRecordForm bonusRecordForm) throws Exception {
		bonusRecordForm.setPageSize(Config.PAGE_SIZE);
		bonusRecordForm.setExport(false);
		Page<BonusRecord> page = bonusRecordManager.list(bonusRecordForm);
		
		request.setAttribute("page", page);
		return "/bonusRecord/list";
	}
	
}
