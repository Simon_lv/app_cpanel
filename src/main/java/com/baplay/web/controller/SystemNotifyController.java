package com.baplay.web.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springside.modules.orm.Page;

import com.baplay.entity.PrizeSn;
import com.baplay.entity.SystemNotify;
import com.baplay.entity.TEfunuser;
import com.baplay.form.SystemNotifyForm;
import com.baplay.service.impl.SystemNotifyManager;
import com.google.common.collect.Maps;

import jxl.Sheet;
import jxl.Workbook;

@Controller
@RequestMapping("/systemNotify")
public class SystemNotifyController {
	private static final Logger LOG = LoggerFactory.getLogger(SystemNotifyController.class);
	private static final int PAGE_SIZE = 25;

	@Autowired
	private SystemNotifyManager systemNotifyManager;

	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		return "/systemNotify/init";
	}

	@RequestMapping("/list")
	public String list(HttpServletRequest request, SystemNotifyForm systemNotifyForm) {
		systemNotifyForm.setPageSize(PAGE_SIZE);
		systemNotifyForm.setExport(false);
		
		Page<SystemNotify> page = systemNotifyManager.list(systemNotifyForm);
		request.setAttribute("page", page);
		return "/systemNotify/list";
	}

	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		if (id != null) {
			SystemNotify systemNotify = systemNotifyManager.findOneById(id);
			request.setAttribute("cp", systemNotify);
			request.setAttribute("edit", "edit");
		} else {
		}
		return "/systemNotify/edit";
	}

	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(HttpServletRequest request, SystemNotify systemNotify) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		if (systemNotify != null) {
			// TEfunuser user = (TEfunuser) SecurityUtils.getSubject().getSession().getAttribute("loginUser");
			if (systemNotify.getId() == null || systemNotify.getId() <= 0) {
				// gameRecord.setCreator(user.getUsername());
				try {
					flag = (systemNotifyManager.add(systemNotify) != null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				// gameRecord.setModifier(user.getUsername());
				flag = (systemNotifyManager.update(systemNotify) > 0);
			}
		}
		if (flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		return result;
	}

	@RequestMapping("/delete")
	public @ResponseBody Map<String, Object> deleteCP(HttpServletRequest request, Long id) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		if (id != null && id > 0) {
			TEfunuser user = (TEfunuser) SecurityUtils.getSubject().getSession().getAttribute("loginUser");
			flag = (systemNotifyManager.delete(id, user.getUsername()) > 0);
		}
		if (flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}

		return result;
	}
	
	@RequestMapping("/importExcel")
	public @ResponseBody Map<String, Object> importExcel(HttpServletRequest request, MultipartFile excelFile) {
		Map<String, Object> result = Maps.newHashMap();
		String realPath = System.getProperty("java.io.tmpdir");
		if (excelFile != null && !excelFile.isEmpty()) {
			String type = excelFile.getContentType();
			LOG.info("File ContentType =" + excelFile.getContentType());			
			
			String filename = String.valueOf(new Date().getTime());
			if (type.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
				filename = filename.concat(".xlsx");
			} else if (type.equals("application/vnd.ms-excel")) {
				filename = filename.concat(".xls");
				// application/octet-stream
			} else if (type.equals("application/octet-stream")) {
				filename = filename.concat(".xls");
			} else {
				LOG.error("文件類型不正確");
				result.put("code", "1001");
				result.put("message", "文件類型不正確！");
				return result;
			}
			File f = new File(realPath, filename);
			LOG.info("上傳目錄=" + f.getAbsolutePath());			
			try {
				Workbook rwb;
				rwb = Workbook.getWorkbook(excelFile.getInputStream());
				Sheet rs = rwb.getSheet(0);
				int rows = rs.getRows(); // 行
				
				List<SystemNotify> systemNotifyList = new ArrayList<SystemNotify>();
				for (int i = 1; i < rows; i++) {
					if(StringUtils.isBlank(rs.getCell(0, i).getContents()) || StringUtils.isBlank(rs.getCell(1, i).getContents())){
						continue;
					}
					
					SystemNotify systemNotify = new SystemNotify();
					systemNotify.setUserId(rs.getCell(0, i).getContents());
					systemNotify.setTitle(rs.getCell(1, i).getContents());
					systemNotify.setMessage(rs.getCell(2, i).getContents());
					
					systemNotifyList.add(systemNotify);
				}
				
				systemNotifyManager.add(systemNotifyList);
			} catch (Exception e) {
				e.printStackTrace();
				LOG.error("文件上傳失敗", e);
				result.put("code", "1002");
				result.put("message", "文件上傳失敗!");
				return result;
			} finally {
				f.delete();
			}
			result.put("code", "1000");
			result.put("message", "上传成功！");
			return result;
		}
		result.put("code", "1003");
		result.put("message", "文件不存在！");
		return result;
	}
	
}
