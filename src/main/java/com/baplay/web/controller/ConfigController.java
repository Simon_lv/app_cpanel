package com.baplay.web.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import com.baplay.constant.Config;
import com.baplay.form.ConfigForm;
import com.baplay.service.IConfigManager;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/config")
public class ConfigController {

	@Autowired
	private IConfigManager configManager;
	
	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		return "/config/init";
	}
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request, ConfigForm configForm) throws Exception {
		configForm.setPageSize(Config.PAGE_SIZE);
		configForm.setExport(false);
		Page<com.baplay.entity.Config> page = configManager.list(configForm);
		
		request.setAttribute("page", page);
		return "/config/list";
	}
	
	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		if (id != null) {
			com.baplay.entity.Config videoConfig = configManager.findOneById(id);
			request.setAttribute("cp", videoConfig);
		}
		
		return "/config/edit";
	}
	
	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(HttpServletRequest request, com.baplay.entity.Config config) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
				
		if (config != null) {
			if (config.getId() == null || config.getId() <= 0) {
				flag = (configManager.save(config) != null);
			} else {
				flag = (configManager.update(config) > 0);
			}
		}		
		
		if (flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		return result;
	}
	
	@RequestMapping("/delete")
	public @ResponseBody Map<String, Object> deleteCP(HttpServletRequest request, Long id) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		if (id != null && id > 0) {			
			flag = (configManager.delete(id) > 0);
		}
		if (flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		return result;
	}
	
}
