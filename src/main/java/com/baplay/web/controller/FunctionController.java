package com.baplay.web.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baplay.entity.Function;
import com.baplay.service.IFunctionManager;

/**
 * 菜單管理
 * 
 * @author Haozi
 *
 */
@Controller
@RequestMapping("/function")
public class FunctionController {
	@Autowired
	private IFunctionManager manager;

	@RequestMapping("/init")
	public String init(Model model, Long parentId) {
		model.addAttribute("select", manager.queryFunctionList(0L));
		model.addAttribute("list", manager.queryFunctionList(parentId));
		model.addAttribute("parentId", parentId);
		return "/function/init";
	}

	@RequestMapping("/get")
	@ResponseBody
	public Function getFunction(Model model, Long id) {
		Function fun = manager.queryFunction(id);
		return fun;
	}

	@RequestMapping("/save")
	@ResponseBody
	public Map<String, Object> update(Function fun) {
		if (fun.getId() != null) {
			manager.updateFunction(fun);
		} else {
			manager.saveFunction(fun);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("status", 1000);
		result.put("message", "保存成功！");
		return result;
	}

	@RequestMapping("/delete")
	@ResponseBody
	public Map<String, Object> delete(Function fun) {
		Map<String, Object> result = new HashMap<String, Object>();
		if (fun.getId() != null) {
			manager.deleteFunction(fun);
			result.put("status", 1000);
			result.put("message", "保存成功！");
		} else {
			result.put("status", -1);
			result.put("message", "ID必填！");
		}
		return result;
	}
}
