package com.baplay.web.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springside.modules.orm.Page;

import com.baplay.constant.Config;
import com.baplay.constant.Games;
import com.baplay.constant.URL;
import com.baplay.entity.Prize;
import com.baplay.entity.PrizeSn;
import com.baplay.entity.TEfunuser;
import com.baplay.form.PrizeForm;
import com.baplay.service.impl.PrizeManager;
import com.baplay.util.HandleFileUpload;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/prize")
public class PrizeController {
	private static final Logger LOG = LoggerFactory.getLogger(PrizeController.class);
	private static final int PAGE_SIZE = 25;

	@Autowired
	private PrizeManager prizeManager;

	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		request.setAttribute("games", Games.getMap3());
		return "/prize/init";
	}

	@RequestMapping("/list")
	public String list(HttpServletRequest request, PrizeForm prizeForm) {
		prizeForm.setPageSize(PAGE_SIZE);
		prizeForm.setExport(false);
		Page<Prize> page = prizeManager.list(prizeForm);
		
		request.setAttribute("page", page);
		return "/prize/list";
	}

	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		if (id != null) {
			Prize prize = prizeManager.findOneById(id);
			request.setAttribute("cp", prize);
			Map<String, String> map = new HashMap<String, String>();
			map.put(prize.getGameCode(), Games.getMap3().get(prize.getGameCode()));
			request.setAttribute("games", map);
		} else {
			request.setAttribute("games", Games.getMap3());
		}
		return "/prize/edit";
	}

	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(HttpServletRequest request, Prize prize) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		try{
		if (prize != null) {
			// TEfunuser user = (TEfunuser) SecurityUtils.getSubject().getSession().getAttribute("loginUser");
			if (prize.getId() == null || prize.getId() <= 0) {
				// gameRecord.setCreator(user.getUsername());
				flag = (prizeManager.add(prize) != null);
			} else {
				// gameRecord.setModifier(user.getUsername());
				flag = (prizeManager.update(prize) > 0);
			}
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		if (flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		return result;
	}

	@RequestMapping("/delete")
	public @ResponseBody Map<String, Object> deleteCP(HttpServletRequest request, Long id) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		if (id != null && id > 0) {
			TEfunuser user = (TEfunuser) SecurityUtils.getSubject().getSession().getAttribute("loginUser");
			flag = (prizeManager.delete(id, user.getUsername()) > 0);
		}
		if (flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}

		return result;
	}

	@RequestMapping("/importExcel")
	public @ResponseBody Map<String, Object> importExcel(HttpServletRequest request, MultipartFile excelFile) {
		Map<String, Object> result = Maps.newHashMap();
		String realPath = System.getProperty("java.io.tmpdir");
		if (excelFile != null && !excelFile.isEmpty()) {
			String type = excelFile.getContentType();
			System.out.println("File ContentType =" + excelFile.getContentType());
			
			String filename = String.valueOf(new Date().getTime());
			if (type.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
				filename = filename.concat(".xlsx");
			} else if (type.equals("application/vnd.ms-excel")) {
				filename = filename.concat(".xls");
				// application/octet-stream
			} else if (type.equals("application/octet-stream")) {
				filename = filename.concat(".xls");
			} else {
				LOG.error("文件類型不正確");
				result.put("code", "1001");
				result.put("message", "文件類型不正確！");
				return result;
			}
			File f = new File(realPath, filename);
			System.out.println("上傳目錄=" + f.getAbsolutePath());
			try {
				Workbook rwb;
				rwb = Workbook.getWorkbook(excelFile.getInputStream());
				Sheet rs = rwb.getSheet(0);
				int rows = rs.getRows(); // 行
				
				List<PrizeSn> prizeSnList = new ArrayList<PrizeSn>();
				for (int i = 1; i < rows; i++) {
					if(StringUtils.isBlank(rs.getCell(0, i).getContents()) || StringUtils.isBlank(rs.getCell(1, i).getContents())){
						continue;
					}
					
					PrizeSn prizeSn = new PrizeSn();
					prizeSn.setPrizeId(Long.valueOf(rs.getCell(0, i).getContents()));
					prizeSn.setSn(rs.getCell(1, i).getContents());
					prizeSn.setUserId(null);
					prizeSn.setReceiveTime(null);
					prizeSn.setSourceIp(null);
					prizeSn.setDeviceId(null);
					
//					prizeSn.setUserId(StringUtils.isBlank(rs.getCell(2, i).getContents()) ? null : rs.getCell(2, i).getContents());
//					LOG.info("Parse receiveTime: " + rs.getCell(3, i).getContents());
//					prizeSn.setReceiveTime(StringUtils.isBlank(rs.getCell(3, i).getContents()) ? null : new SimpleDateFormat("yyyy/MM/dd HH:mm").parse(rs.getCell(3, i).getContents()));
//					prizeSn.setSourceIp(StringUtils.isBlank(rs.getCell(4, i).getContents()) ? null : rs.getCell(4, i).getContents());
//					prizeSn.setDeviceId(StringUtils.isBlank(rs.getCell(5, i).getContents()) ? null : rs.getCell(5, i).getContents());
					prizeSnList.add(prizeSn);
				}
				
				prizeManager.add(prizeSnList);
			} catch (Exception e) {
				e.printStackTrace();
				LOG.error("文件上傳失敗", e);
				result.put("code", "1002");
				result.put("message", "文件上傳失敗!");
				return result;
			} finally {
				f.delete();
			}
			result.put("code", "1000");
			result.put("message", "上传成功！");
			return result;
		}
		result.put("code", "1003");
		result.put("message", "文件不存在！");
		return result;
	}
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody HashMap<String, String> handleFileUpload(@RequestParam("file") MultipartFile file,
			@RequestParam(value = "json", required = false, defaultValue = "") String json) throws IOException, Exception {
		HashMap<String, String> result = new HashMap<String, String>();
		LOG.info("[PrizeController.handleFileUpload][filename=" + file.getOriginalFilename() + "]");
		String url = HandleFileUpload.upload(file, URL.URL_IMG_GAME_RECORD.toString(), URL.FTP_IMG_GAME_RECORD.toString(), Config.UPLOAD_VIDEO_FILE_LIMIT);

		// Parse Json
		if (StringUtils.isNotEmpty(json)) {
			LOG.info("json:" + json);
			JSONObject jsonObj = new JSONObject(json);
			LOG.info("jsonObj:" + jsonObj.toString());
		}

		result.put("url", url);
		return result;
	}

}
