package com.baplay.web.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import com.baplay.constant.Config;
import com.baplay.entity.AppVersionControl;
import com.baplay.form.AppVersionControlForm;
import com.baplay.service.IAppVersionControlManager;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/appVersionControl")
public class AppVersionControlController {

	private static final Logger LOG = LoggerFactory.getLogger(AppVersionControlController.class);
	
	@Autowired
	private IAppVersionControlManager appVersionControlManager;
	
	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		return "/appVersionControl/init";
	}
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request, AppVersionControlForm appVersionControlForm) throws Exception {
		appVersionControlForm.setPageSize(Config.PAGE_SIZE);
		appVersionControlForm.setExport(false);
		Page<AppVersionControl> page = appVersionControlManager.list(appVersionControlForm);
		
		request.setAttribute("page", page);
		return "/appVersionControl/list";
	}
	
	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		if (id != null) {
			AppVersionControl appVersionControl = appVersionControlManager.findOneById(id);
			request.setAttribute("cp", appVersionControl);
		}
		
		return "/appVersionControl/edit";
	}
	
	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(HttpServletRequest request, AppVersionControl appVersionControl) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		if (appVersionControl != null) {
			if (appVersionControl.getId() == null || appVersionControl.getId() <= 0) {
				flag = (appVersionControlManager.save(appVersionControl) != null);
			} else {
				flag = (appVersionControlManager.update(appVersionControl) > 0);
			}
		}
		
		if (flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		return result;
	}
	
	@RequestMapping("/delete")
	public @ResponseBody Map<String, Object> deleteCP(HttpServletRequest request, Long id) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		if (id != null && id > 0) {			
			flag = (appVersionControlManager.delete(id) > 0);
		}
		if (flag) {
			result.put("code", "0000");
			result.put("message", "停權成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		return result;
	}
	
}
