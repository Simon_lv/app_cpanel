package com.baplay.web.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baplay.dto.UserDto;
import com.baplay.entity.Function;
import com.baplay.entity.Role;
import com.baplay.entity.TEfunuser;
import com.baplay.service.IPermissionManager;
import com.baplay.shiro.UserRealm;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

@Controller
public class LoginController {

	private static final Logger log = LoggerFactory
			.getLogger(LoginController.class);

	@Autowired
	private IPermissionManager permissionManager;

	@RequestMapping(value = "login", method = RequestMethod.POST)
	public String login(@RequestParam(value = "username") String username,
			@RequestParam(value = "password") String password, Model m,
			HttpSession session) throws Exception {
		String url = "login";
		UsernamePasswordToken token = new UsernamePasswordToken(username,
				password);
		// 获取当前的Subject
		Subject currentUser = SecurityUtils.getSubject();
		try {
			// 在调用了login方法后,SecurityManager会收到AuthenticationToken,并将其发送给已配置的Realm执行必须的认证检查
			// 每个Realm都能在必要时对提交的AuthenticationTokens作出反应
			// 所以这一步在调用login(token)方法时,它会走到MyRealm.doGetAuthenticationInfo()方法中,具体验证方式详见此方法
			log.debug("对用户[" + username + "]进行登录验证..验证开始");
			currentUser.login(token);
			log.debug("对用户[" + username + "]进行登录验证..验证通过");
			url = "redirect:index";
		} catch (UnknownAccountException uae) {
			log.debug("对用户[" + username + "]进行登录验证..验证未通过,未知账户");
			m.addAttribute("message_login", "用戶名或密碼錯誤");
			log.error(uae.getMessage(), uae);
		} catch (IncorrectCredentialsException ice) {
			log.debug("对用户[" + username + "]进行登录验证..验证未通过,错误的凭证");
			m.addAttribute("message_login", "用戶名或密碼錯誤");
			log.error(ice.getMessage(), ice);
		} catch (LockedAccountException lae) {
			log.debug("对用户[" + username + "]进行登录验证..验证未通过,账户已锁定");
			m.addAttribute("message_login", "用戶名或密碼錯誤");
			log.error(lae.getMessage(), lae);
		} catch (ExcessiveAttemptsException eae) {
			log.debug("对用户[" + username + "]进行登录验证..验证未通过,错误次数过多");
			m.addAttribute("message_login", "用戶名或密碼錯誤");
			log.error(eae.getMessage(), eae);
		} catch (AuthenticationException ae) {
			// 通过处理Shiro的运行时AuthenticationException就可以控制用户登录失败或密码错误时的情景
			log.debug("对用户[" + username + "]进行登录验证..验证未通过,堆栈轨迹如下");
			ae.printStackTrace();
			m.addAttribute("message_login", "用戶名或密碼錯誤");
			log.error(ae.getMessage(), ae);
		}
		// 验证是否登录成功
		if (currentUser.isAuthenticated()) {
			log.debug("用户[" + username + "]登录认证通过(这里可以进行一些认证通过后的一些系统参数初始化操作)");
			TEfunuser user = (TEfunuser) SecurityUtils.getSubject()
					.getSession().getAttribute("loginUser");
			List<Role> roles = permissionManager.getRole(user.getUserid());
			SecurityUtils.getSubject().getSession().setAttribute("role", roles);
			Map<String, Function> funs = Maps.newHashMap();
			for (Role r : roles) {
				List<Function> fs = permissionManager
						.getFunctions(r.getId(), 0);
				for (Function f : fs) {
					funs.put(f.getName(), f);
				}
			}
			List<Function> f = Lists.newArrayList(funs.values());
			SecurityUtils.getSubject().getSession(true)
					.setAttribute("functions", Lists.reverse(f));
		} else {
			token.clear();
		}
		return url;
	}

	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String toLogin() {
		return "login";
	}

	@Autowired
	private UserRealm realm;

	@RequestMapping("logout")
	public String logout(HttpSession session) {
		if (session != null) {
			session.invalidate();
		}
		if (SecurityUtils.getSubject().getPrincipals() != null) {
			realm.clearCache(SecurityUtils.getSubject().getPrincipals());
		}

		SecurityUtils.getSubject().logout();
		return "redirect:login";
	}

	@RequestMapping("index")
	public String index() {
		return "index";
	}

}
