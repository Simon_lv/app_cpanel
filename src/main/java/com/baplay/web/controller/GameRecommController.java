package com.baplay.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springside.modules.orm.Page;

import com.baplay.constant.Config;
import com.baplay.constant.URL;
import com.baplay.entity.GameRecomm;
import com.baplay.form.GameRecommForm;
import com.baplay.infra.dao.exceptions.DoubleInsertException;
import com.baplay.service.IGameRecommManager;
import com.baplay.util.HandleFileUpload;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/gameRecomm")
public class GameRecommController {

	private static final Logger LOG = LoggerFactory.getLogger(GameRecommController.class);
	
	@Autowired
	private IGameRecommManager gameRecommManager;
	
	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		return "/gameRecomm/init";
	}
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request, GameRecommForm gameRecommForm) {
		gameRecommForm.setPageSize(Config.PAGE_SIZE);
		gameRecommForm.setExport(false);
		Page<GameRecomm> page = gameRecommManager.list(gameRecommForm);
		
		request.setAttribute("page", page);
		return "/gameRecomm/list";
	}
	
	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		if(id != null){
			GameRecomm gameRecomm = gameRecommManager.findById(id);
			
			request.setAttribute("gameRecomm", gameRecomm);
		}
		
		return "/gameRecomm/edit";
	}
	
	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(HttpServletRequest request, GameRecomm gameRecomm) throws Exception {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		if (gameRecomm != null) {
			
			try{
				if (gameRecomm.getId() == null) {
					flag = (gameRecommManager.save(request, gameRecomm) != null);
				} else {
					flag = (gameRecommManager.update(request, gameRecomm) > 0);
				}
				
				if (flag) {
					result.put("code", "0000");
					result.put("message", "保存成功！");
				} else {
					result.put("code", "0001");
					result.put("message", "保存失敗！");
				}
				
			}catch(DoubleInsertException e){
				result.put("code", e.getResultCode());
				result.put("message", e.getResultMsg());
			}
		}
		
		return result;
	}
	
	@RequestMapping("/delete")
	public @ResponseBody Map<String, Object> deleteCP(HttpServletRequest request, Long id) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
					
			flag = (gameRecommManager.delete(id) > 0);
		
		if (flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "刪除失敗！");
		}
		return result;
	}
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody HashMap<String, String> handleFileUpload(@RequestParam("file") MultipartFile file,
			@RequestParam(value = "json", required = false, defaultValue = "") String json) throws IOException, Exception {
		HashMap<String, String> result = new HashMap<String, String>();
		LOG.info("[GameRecommController.handleFileUpload][filename=" + file.getOriginalFilename() + "]");
		String url = HandleFileUpload.upload(file, URL.URL_IMG_GAMERECOMM.toString(), URL.FTP_IMG_GAMERECOMM.toString(), Config.UPLOAD_VIDEO_FILE_LIMIT);
		if (StringUtils.isNotEmpty(json)) {
			LOG.info("json:" + json);
			JSONObject jsonObj = new JSONObject(json);
			LOG.info("jsonObj:" + jsonObj.toString());
		}
		result.put("url", url);
		return result;
	}
	
}
