package com.baplay.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springside.modules.orm.Page;

import com.baplay.constant.Config;
import com.baplay.constant.Games;
import com.baplay.constant.URL;
import com.baplay.entity.GameRecord;
import com.baplay.entity.TEfunuser;
import com.baplay.form.GameRecordForm;
import com.baplay.infra.dao.exceptions.DoubleInsertException;
import com.baplay.service.impl.GameRecordManager;
import com.baplay.util.HandleFileUpload;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/gameRecord")
public class GameRecordController {
	private static final Logger LOG = LoggerFactory.getLogger(GameRecordController.class);
	private static final int PAGE_SIZE = 25;

	@Autowired
	private GameRecordManager gameRecordManager;

	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		request.setAttribute("games", Games.getMap3());
		return "/gameRecord/init";
	}

	@RequestMapping("/list")
	public String list(HttpServletRequest request, GameRecordForm gameRecordForm) {
		gameRecordForm.setPageSize(PAGE_SIZE);
		gameRecordForm.setExport(false);
		Page<GameRecord> page = gameRecordManager.list(gameRecordForm);
		
		request.setAttribute("page", page);
		return "/gameRecord/list";
	}

	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		if (id != null) {
			GameRecord gameRecord = gameRecordManager.findOneById(id);
			request.setAttribute("cp", gameRecord);
			Map<String, String> map = new HashMap<String, String>();
			map.put(gameRecord.getGameCode(), Games.getMap3().get(gameRecord.getGameCode()));
			request.setAttribute("games", map);
		} else {
			request.setAttribute("games", Games.getMap3());
		}
		return "/gameRecord/edit";
	}

	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(HttpServletRequest request, GameRecord gameRecord) throws Exception {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		
		try{
			if (gameRecord != null) {
				if (gameRecord.getId() == null || gameRecord.getId() <= 0) {
					flag = (gameRecordManager.add(gameRecord) != null);
				} else {
					flag = (gameRecordManager.update(gameRecord) > 0);
				}
			}
			if (flag) {
				result.put("code", "0000");
				result.put("message", "保存成功！");
			} else {
				result.put("code", "0001");
				result.put("message", "保存失敗！");
			}
		}catch(DoubleInsertException e){
			result.put("code", e.getResultCode());
			result.put("message", e.getResultMsg());
		}
		
		return result;
	}

	@RequestMapping("/delete")
	public @ResponseBody Map<String, Object> deleteCP(HttpServletRequest request, Long id) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		if (id != null && id > 0) {
			TEfunuser user = (TEfunuser) SecurityUtils.getSubject().getSession().getAttribute("loginUser");
			flag = (gameRecordManager.delete(id, user.getUsername()) > 0);
		}
		if (flag) {
			result.put("code", "0000");
			result.put("message", "刪除成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		return result;
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody HashMap<String, String> handleFileUpload(@RequestParam("file") MultipartFile file,
			@RequestParam(value = "json", required = false, defaultValue = "") String json) throws IOException, Exception {
		HashMap<String, String> result = new HashMap<String, String>();
		LOG.info("[GameRecordController.handleFileUpload][filename=" + file.getOriginalFilename() + "]");
		String url = HandleFileUpload.upload(file, URL.URL_IMG_GAME_RECORD.toString(), URL.FTP_IMG_GAME_RECORD.toString(), Config.UPLOAD_VIDEO_FILE_LIMIT);
		if (StringUtils.isNotEmpty(json)) {
			LOG.info("json:" + json);
			JSONObject jsonObj = new JSONObject(json);
			LOG.info("jsonObj:" + jsonObj.toString());
		}
		result.put("url", url);
		return result;
	}

}
