package com.baplay.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springside.modules.orm.Page;

import com.baplay.constant.Config;
import com.baplay.entity.PrizeSn;
import com.baplay.form.PrizeSnForm;
import com.baplay.service.IPrizeSnManager;

@Controller
@RequestMapping("/prizeSn")
public class PrizeSnController {

	private static final Logger LOG = LoggerFactory.getLogger(PrizeSnController.class);
	
	@Autowired
	private IPrizeSnManager prizeSnManager;
	
	@RequestMapping("/init")
	public String init(HttpServletRequest request) {
		return "/prizeSn/init";
	}
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request, PrizeSnForm prizeSnForm) throws Exception {
		prizeSnForm.setPageSize(Config.PAGE_SIZE);
		prizeSnForm.setExport(false);
		Page<PrizeSn> page = prizeSnManager.list(prizeSnForm);
		
		request.setAttribute("page", page);
		return "/prizeSn/list";
	}
	
}