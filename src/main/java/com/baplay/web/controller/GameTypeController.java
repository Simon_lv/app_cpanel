package com.baplay.web.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springside.modules.orm.Page;

import com.baplay.constant.Config;
import com.baplay.entity.GameType;
import com.baplay.form.GameTypeForm;
import com.baplay.service.IGameTypeManager;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/gameType")
public class GameTypeController {
	
	@Autowired
	private IGameTypeManager gameTypeManager;
	
	@RequestMapping("/init")
	public String init() {
		return "/gameType/init";
	}
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request, GameTypeForm gameTypeForm) {
		gameTypeForm.setPageSize(Config.PAGE_SIZE);
		gameTypeForm.setExport(false);
		Page<GameType> page = gameTypeManager.list(gameTypeForm);
		request.setAttribute("page", page);
		return "/gameType/list";
	}
	
	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		if (id != null) {
			GameType gameType = gameTypeManager.findOneById(id);
			request.setAttribute("cp", gameType);
		}
		
		return "/gameType/edit";
	}
	
	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(HttpServletRequest request, GameType gameType) {
		Map<String, Object> result = Maps.newHashMap();
		boolean flag = false;
		if (gameType != null) {
			if (gameType.getId() == null || gameType.getId() <= 0) {
				flag = (gameTypeManager.save(gameType) != null);
			} else {
				flag = (gameTypeManager.update(gameType) > 0);
			}
		}
		if (flag) {
			result.put("code", "0000");
			result.put("message", "保存成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		return result;
	}
	
	@RequestMapping("/updateStatus")
	public @ResponseBody Map<String, Object> updateStatus(HttpServletRequest request, Long id, int status) {
		Map<String, Object> result = Maps.newHashMap();

		boolean flag = false;
		if (id != null && id > 0) {			
			flag = (gameTypeManager.updateStatus(id, status) > 0);
		}
		if (flag) {
			result.put("code", "0000");
			
			if(status == 1){
				result.put("message", "啟用成功！");
			}else{
				result.put("message", "停權成功！");
			}
			
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		
		return result;
	}
	
}
