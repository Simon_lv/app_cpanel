package com.baplay.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springside.modules.orm.Page;

import com.baplay.constant.Config;
import com.baplay.constant.URL;
import com.baplay.entity.Mission;
import com.baplay.form.MissionForm;
import com.baplay.service.IMissionManager;
import com.baplay.service.impl.GameRecordManager;
import com.baplay.util.HandleFileUpload;
import com.google.common.collect.Maps;

@Controller
@RequestMapping("/mission")
public class MissionController {
	private static final Logger LOG = LoggerFactory
			.getLogger(MissionController.class);
	private static final String TAG = "MissionController";
	private static final int PAGE_SIZE = 20;
	
	@Autowired
	private IMissionManager missionManager;
	
	@Autowired
	private GameRecordManager gameRecordManager;
	
	@RequestMapping("/init")
	public String init(HttpSession session, Model model) {
		List<Map<String, Object>> games = gameRecordManager.getGames();
		model.addAttribute("games", games);
		return "mission/init";
	}
	
	@RequestMapping("/list")
	public String list(HttpServletRequest request, MissionForm missionForm) {
		missionForm.setPageSize(PAGE_SIZE);
		missionForm.setExport(false);
		Page<Mission> page = missionManager.list(missionForm);
		request.setAttribute("page", page);
		return "/mission/list";
	}
	
	@RequestMapping("/edit")
	public String edit(HttpServletRequest request, Long id) {
		List<Map<String, Object>> games = gameRecordManager.getGames();
		request.setAttribute("games", games);
		if(id != null) {
			request.setAttribute("mission", missionManager.findOneById(id));
		}
		return "/mission/edit";
	}
	
	@RequestMapping("/save")
	public @ResponseBody Map<String, Object> save(HttpServletRequest request, Mission mission) {
		Map<String, Object> result = Maps.newHashMap();

		boolean flag = false;
		if(mission != null) {
			if(mission.getId() == null || mission.getId() <= 0) {
				mission = missionManager.add(mission);
				flag = (mission!=null);
			} else {
				flag = (missionManager.update(mission)>0);
			}
		}
		if (flag) {
			result.put("code", "0000");
			result.put("id", mission.getId());
			result.put("message", "保存成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");	
		}
		return result;
	}
	
	@RequestMapping(value="/upload", method=RequestMethod.POST)
	public @ResponseBody HashMap<String, String> handleFileUpload( @RequestParam("file") MultipartFile file,
			@RequestParam(value="json", required = false, defaultValue = "") String json) throws IOException, Exception{
		HashMap<String, String> result = new HashMap<String, String>();
		LOG.info("[MissionController.handleFileUpload][filename="+file.getOriginalFilename()+"]");
		String url = HandleFileUpload.upload(file, URL.URL_IMG_MISSION.toString(), URL.FTP_IMG_MISSION.toString(), Config.UPLOAD_VIDEO_FILE_LIMIT);
		
		// Parse Json
		if(StringUtils.isNotEmpty(json)) {
			LOG.info("json:" + json);
			JSONObject jsonObj = new JSONObject(json);
			LOG.info("jsonObj:" + jsonObj.toString());
		}
		
		result.put("url", url);
		return result;
    }
	
	@RequestMapping("/updateStatus")
	public @ResponseBody Map<String, Object> updateStatus(HttpServletRequest request, Long id, int status) {
		Map<String, Object> result = Maps.newHashMap();

		boolean flag = false;
		if (id != null && id > 0) {
			flag = (missionManager.updateStatus(id, status) > 0);
		}
		if (flag) {
			result.put("code", "0000");
			result.put("message", "修改成功！");
		} else {
			result.put("code", "0001");
			result.put("message", "保存失敗！");
		}
		
		return result;
	}
}
