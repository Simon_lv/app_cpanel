package com.baplay.constant;

public class Config {

	public static final int UPLOAD_FILE_LIMIT = 1000000;
	public static final int UPLOAD_VIDEO_FILE_LIMIT = 10485760;
	public static final int PAGE_SIZE = 20;
}
