package com.baplay.constant;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.sun.tools.jdi.LinkedHashMap;

@Component("Games")
public class Games {
	@SuppressWarnings("unchecked")
	private Map<String, String> map = new LinkedHashMap();
	{
		map.put("ysmz|'ysmz','ysmzios'", "英雄妹子就要你");
		map.put("dys|'dys','dysios'", "大英雄-劍仙再起");
		map.put("ddt|'ddt','ddtios'", "彈彈堂");
		map.put("cb|'cb','cbios'", "赤壁-三分天下");
		map.put("ft|'ft','ftios'", "幻想編年史");
		map.put("alft|'alft'", "幻想編年史(ASUS)");
		map.put("jxy|'jxy','jxyios'", "大英雄");
		map.put("jxy|'jxyasus'", "大英雄(ASUS)");
		map.put("xajh|'xajh','xajhios'", "笑傲江湖");
		map.put("xajhhk|'xajhhk'", "笑傲江湖(HK)");
		map.put("alxajh|'alxajh'", "笑傲江湖(ASUS)");
		map.put("shcu|'shcu','shcuios'", "Q爆江湖");
		map.put("xajh|'xajh','xajhios'", "笑傲江湖");
		map.put("yfno1|'yfno1'", "天下第一城");
		map.put("yfsc|'yfsc','yfscios'", "岳戰天下(星馬版)");
		map.put("yf|'yf','yfios'", "岳戰天下");
		map.put("yfjodo|'yfjodo','yfjodoios'", "岳戰天下(英卓)");
		map.put("qj|'qj','qjios'", "傾城計");
		map.put("lzqj|'lzqj','lzqjios'", "傾城計隆中");
		map.put("shgz|'shgz','shgzios'", "仙國志");
		map.put("lzxg|'lzxg','lzxgios'", "仙國志隆中");
		map.put("yjz|'yjz','yjzios','yjz2','yjz3','yjz4'", "魅子");
		map.put("lzmz|'lzmz','lzmzios'", "魅子隆中");
		map.put("yjz|yjz-2000fun", "魅子2000fun");
		map.put("yjz|yjz-aqgy", "魅子愛情公寓");
		map.put("ah|'ah','ah_ios','ahev','ahevios'", "上古戰魂");
		map.put("lzah|'lzah','lzahios'", "上古戰魂隆中");
		map.put("bhxy|'bhxy','bhxyios'", "崩壞學園");
		map.put("zg2|'zg2','zg2ios','zg2web'", "仙境之戀2");
		map.put("tk|'tk','tkios'", "紅警坦克");
		map.put("zlsg|'zlsg','zlsgios'", "名將爭霸");
		map.put("lzmj|'lzmj'", "名將爭霸隆中");
		map.put("jlts|'jlts','aljlts','fdjlts','jltsios'", "君臨天下");
	}
	@SuppressWarnings("unchecked")
	private Map<String, String> map2 = new LinkedHashMap();
	{
		map2.put("jlts,jltsios", "君臨天下");
		map2.put("ysmz,ysmzios", "英雄妹子就要你");
		map2.put("dys,dysios", "大英雄-劍仙再起");
		map2.put("ddt,ddtios", "彈彈堂");
		map2.put("cb,cbios", "赤壁-三分天下");
		map2.put("ft,ftios", "幻想編年史");
		map2.put("alft", "幻想編年史(ASUS)");
		map2.put("jxy,jxyios", "大英雄");
		map2.put("jxyasus", "大英雄(ASUS)");
		map2.put("xajh,xajhios", "笑傲江湖");
		map2.put("xajhhk", "笑傲江湖(HK)");
		map2.put("alxajh", "笑傲江湖(ASUS)");
		map2.put("qj,qjios,lzqj,lzqjios", "傾城計");
		map2.put("shgz,shgzios,lzxg,lzxgios", "仙國志");
		map2.put("yjz,yjzios,yjz2,yjz3,yjz4,lzmz,lzmzios", "魅子Online");
		map2.put("ah,ah_ios,ahev,ahevios,lzah,lzahios", "上古戰魂");
		map2.put("bhxy,bhxyios", "崩壞學園");
		map2.put("zg2,zg2ios,zg2web", "仙境之戀2");
		map2.put("tk,tkios", "紅警坦克");
		map2.put("yf,yfios", "岳戰天下");
		map2.put("yfno1", "天下第一城");
		map2.put("yfjodo,yfjodoios", "岳戰天下(英卓)");
		map2.put("yfsc,yfscios", "岳戰天下(星馬版)");
		map2.put("zlsg,zlsgios,lzmj", "名將爭霸");
		map2.put("xajh,xajhios", "笑傲江湖");
		map2.put("shcu,shcuios", "Q爆江湖");
	}
	@SuppressWarnings("unchecked")
	private static Map<String, String> map3 = new LinkedHashMap();
	{
		map3.put("jlts", "君臨天下");
		map3.put("ysmz", "英雄妹子就要你");
		map3.put("dys", "大英雄-劍仙再起");
		map3.put("ddt", "彈彈堂");
		map3.put("cb", "赤壁-三分天下");
		map3.put("ft", "幻想編年史");
		map3.put("alft", "幻想編年史(ASUS)");
		map3.put("jxy", "大英雄");
		map3.put("jxyasus", "大英雄(ASUS)");
		map3.put("shcu", "Q爆江湖");
		map3.put("xajh", "笑傲江湖");
		map3.put("xajhhk", "笑傲江湖(HK)");
		map3.put("alxajh", "笑傲江湖(ASUS)");
		map3.put("yfno1", "天下第一城");
		map3.put("yf", "岳戰天下");
		map3.put("yfjodo", "岳戰天下(英卓)");
		map3.put("yfsc", "岳戰天下(星馬版)");
		map3.put("qj", "傾城計");
		map3.put("lzqj", "傾城計隆中");
		map3.put("shgz", "仙國志");
		map3.put("lzxg", "仙國志隆中");
		map3.put("yjz", "魅子");
		map3.put("lzmz", "魅子隆中");
		map3.put("yjz-2000fun", "魅子2000fun");
		map3.put("yjz-aqgy", "魅子愛情公寓");
		map3.put("ah", "上古戰魂");
		map3.put("lzah", "上古戰魂隆中");
		map3.put("bhxy", "崩壞學園");
		map3.put("zg2", "仙境之戀2");
		map3.put("tk", "紅警坦克");
		map3.put("zlsg", "名將爭霸");
		map3.put("lzmj", "名將爭霸隆中");

	}

	@SuppressWarnings("unchecked")
	private static Map<String, String> activityGameMap = new LinkedHashMap();
	static {
		activityGameMap.put("shcu", "Q爆江湖");
		activityGameMap.put("xajh", "笑傲江湖");
		activityGameMap.put("jxy", "大英雄");
		activityGameMap.put("ft", "幻想編年史");
		activityGameMap.put("cb", "赤壁-三分天下");
	}

	@SuppressWarnings("unchecked")
	private static Map<String, String> announcementMap = new LinkedHashMap();
	{
		announcementMap.put("jlts", "君臨天下");
		announcementMap.put("dys", "大英雄-劍仙再起");
		announcementMap.put("ysmz", "英雄妹子就要你");
		announcementMap.put("ddt", "彈彈堂");
		announcementMap.put("cb", "赤壁-三分天下");
		announcementMap.put("ft", "幻想編年史");
		announcementMap.put("jxy", "大英雄");
		announcementMap.put("shcu", "Q爆江湖");
		announcementMap.put("xajh", "笑傲江湖");
		announcementMap.put("yf", "岳戰天下");
		announcementMap.put("qj", "傾城計");
		announcementMap.put("lzqj", "傾城計隆中");
		announcementMap.put("shgz", "仙國志");
		announcementMap.put("yjz", "魅子");
		announcementMap.put("ah", "上古戰魂");
		announcementMap.put("lzah", "上古戰魂隆中");
		announcementMap.put("bhxy", "崩壞學園");
		announcementMap.put("zg2", "仙境之戀2");
		announcementMap.put("tk", "紅警坦克");
		announcementMap.put("zlsg", "名將爭霸");

	}

	private Map<String, String> vipDepositMap = new LinkedHashMap();
	{
		vipDepositMap.put("ddt|'ddt':'ddtios'", "彈彈堂");
		vipDepositMap.put("ysmz|'ysmz':'ysmzios'", "英雄妹子就要你");
		vipDepositMap.put("dys|'dys':'dysios'", "大英雄-劍仙再起");
		vipDepositMap.put("cb|'cb':'cbios'", "赤壁-三分天下");
		vipDepositMap.put("ft|'ft':'ftios':'alft'", "幻想編年史");
		vipDepositMap.put("jxy|'jxy':'jxyios':'jxyasus'", "大英雄");
		vipDepositMap.put("xajh|'xajh':'xajhios':'xajhhk':'alxajh':'xajhios'", "笑傲江湖");
		vipDepositMap.put("shcu|'shcu':'shcuios'", "Q爆江湖");
		vipDepositMap.put("yfno1|'yfno1'", "天下第一城");
		vipDepositMap.put("yf|'yf':'yfios':'yfsc':'yfscios':'yfjodo':'yfjodoios'", "岳戰天下");
		vipDepositMap.put("qj|'qj':'qjios':'lzqj':'lzqjios'", "傾城計");
		vipDepositMap.put("shgz|'shgz':'shgzios':'lzxg':'lzxgios'", "仙國志");
		vipDepositMap.put("yjz|'yjz':'yjzios':'yjz2':'yjz3':'yjz4':'lzmz':'lzmzios':'yjz-2000fun':'yjz-aqgy'", "魅子");
		vipDepositMap.put("ah|'ah':'ah_ios':'ahev':'ahevios':'lzah':'lzahios'", "上古戰魂");
		vipDepositMap.put("bhxy|'bhxy':'bhxyios'", "崩壞學園");
		vipDepositMap.put("zg2|'zg2':'zg2ios':'zg2web'", "仙境之戀2");
		vipDepositMap.put("tk|'tk':'tkios'", "紅警坦克");
		vipDepositMap.put("zlsg|'zlsg':'zlsgios':'lzmj'", "名將爭霸");
		vipDepositMap.put("jlts|'jlts':'aljlts':'fdjlts':'jltsios'", "君臨天下");
	}

	public Map<String, String> getVipDepositMap() {
		return vipDepositMap;
	}

	public void setVipDepositMap(Map<String, String> vipDepositMap) {
		this.vipDepositMap = vipDepositMap;
	}

	public static Map<String, String> getActivityGameMap() {
		return activityGameMap;
	}

	public static void setActivityGameMap(Map<String, String> activityGameMap) {
		Games.activityGameMap = activityGameMap;
	}

	public String get2(String gameCode) {
		return map2.get(gameCode);
	}

	public String get3(String gameCode) {
		return map3.get(gameCode);
	}

	public String get(String gameCode) {
		return map.get(gameCode);
	}

	public Map<String, String> getMap() {
		return map;
	}

	public Map<String, String> getMap2() {
		return map2;
	}

	public static Map<String, String> getMap3() {
		return map3;
	}

	public static String getMap3Str(String gameCode) {
		return map3.get(gameCode);
	}

	public static Map<String, String> getAnnouncementMap() {
		return announcementMap;
	}

	public static void setAnnouncementMap(Map<String, String> announcementMap) {
		Games.announcementMap = announcementMap;
	}

}
