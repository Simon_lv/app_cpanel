package com.baplay.constant;

import com.baplay.util.Configuration;

public enum URL {
	URL_IMG_MISSION("url.images.mission"),
	
	FTP_IMG_MISSION("ftp.images.mission"),
	
	URL_IMG_PUSH("url.images.push"),
	
	FTP_IMG_PUSH("ftp.images.push"),
	
	URL_IMG_GAME_RECORD("url.images.gameRecord"),
	
	FTP_IMG_GAME_RECORD("ftp.images.gameRecord"),
	
	URL_IMG_BANNER("url.images.banner"),
	
	FTP_IMG_BANNER("ftp.images.banner"),
	
	URL_PUSH("url.push"),
	
	URL_IMG_GAMERECOMM("url.images.gameRecomm"),
	
	FTP_IMG_GAMERECOMM("ftp.images.gameRecomm"),
	
	URL_IMG_ANNOUNCEMENT("url.images.announcement"),
	
	FTP_IMG_ANNOUNCEMENT("ftp.images.announcement")
	
	;
	
	private String key;
	
	private URL(String key){
		//auto add "test_" prefix if running a test server
		if(isTest())
		    this.key = "test." + key;
		else
		    this.key=key;
	}

	@Override
	public String toString() {
		return Configuration.getInstance().getProperty(this.key);
	}
	
	private boolean isTest() {
		return "true".equalsIgnoreCase(Configuration.getInstance().getProperty("isTestServer"));
	}
	
}
