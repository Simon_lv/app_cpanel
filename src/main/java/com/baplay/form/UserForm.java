package com.baplay.form;

public class UserForm {
	private Long userId;
	private Long roleId1;
	private Long roleId2;
	private String funname;
	private String username;
	private String password;
	private String mobile;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFunname() {
		return funname;
	}

	public void setFunname(String funname) {
		this.funname = funname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Long getRoleId1() {
		return roleId1;
	}

	public void setRoleId1(Long roleId1) {
		this.roleId1 = roleId1;
	}

	public Long getRoleId2() {
		return roleId2;
	}

	public void setRoleId2(Long roleId2) {
		this.roleId2 = roleId2;
	}
}
