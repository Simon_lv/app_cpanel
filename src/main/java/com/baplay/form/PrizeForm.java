package com.baplay.form;

public class PrizeForm {
	private static final long serialVersionUID = 1L;

	private String gameCode;
	private String timeStart;
	private String timeEnd;
	private String prizeTimeStart;
	private String prizeTimeEnd;

	private Integer pageNo;
	private int pageSize;
	private boolean export;

	public String getGameCode() {
		return gameCode;
	}

	public void setGameCode(String gameCode) {
		this.gameCode = gameCode;
	}

	public String getTimeStart() {
		return timeStart;
	}

	public void setTimeStart(String timeStart) {
		this.timeStart = timeStart;
	}

	public String getTimeEnd() {
		return timeEnd;
	}

	public void setTimeEnd(String timeEnd) {
		this.timeEnd = timeEnd;
	}

	public String getPrizeTimeStart() {
		return prizeTimeStart;
	}

	public void setPrizeTimeStart(String prizeTimeStart) {
		this.prizeTimeStart = prizeTimeStart;
	}

	public String getPrizeTimeEnd() {
		return prizeTimeEnd;
	}

	public void setPrizeTimeEnd(String prizeTimeEnd) {
		this.prizeTimeEnd = prizeTimeEnd;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public boolean isExport() {
		return export;
	}

	public void setExport(boolean export) {
		this.export = export;
	}

}
