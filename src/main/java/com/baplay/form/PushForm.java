package com.baplay.form;

import com.baplay.entity.PushRecord;

public class PushForm extends PushRecord {
	private static final long serialVersionUID = 1L;

	private Integer pageNo;
	private int pageSize;
	private boolean export;

	private String pushTimeStart;
	private String pushTimeEnd;

	private String runTimeStart;
	private String runTimeEnd;
	
	private String iosRunTimeStart;
	private String iosRunTimeEnd;

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public boolean isExport() {
		return export;
	}

	public void setExport(boolean export) {
		this.export = export;
	}

	public String getPushTimeStart() {
		return pushTimeStart;
	}

	public void setPushTimeStart(String pushTimeStart) {
		this.pushTimeStart = pushTimeStart;
	}

	public String getPushTimeEnd() {
		return pushTimeEnd;
	}

	public void setPushTimeEnd(String pushTimeEnd) {
		this.pushTimeEnd = pushTimeEnd;
	}

	public String getRunTimeStart() {
		return runTimeStart;
	}

	public void setRunTimeStart(String runTimeStart) {
		this.runTimeStart = runTimeStart;
	}

	public String getRunTimeEnd() {
		return runTimeEnd;
	}

	public void setRunTimeEnd(String runTimeEnd) {
		this.runTimeEnd = runTimeEnd;
	}

	public String getIosRunTimeStart() {
		return iosRunTimeStart;
	}

	public void setIosRunTimeStart(String iosRunTimeStart) {
		this.iosRunTimeStart = iosRunTimeStart;
	}

	public String getIosRunTimeEnd() {
		return iosRunTimeEnd;
	}

	public void setIosRunTimeEnd(String iosRunTimeEnd) {
		this.iosRunTimeEnd = iosRunTimeEnd;
	}

}
