package com.baplay.form;

import com.baplay.entity.AppVersionControl;

public class AppVersionControlForm extends AppVersionControl {
	private static final long serialVersionUID = 1L;
	
	private Integer pageNo;
	private int pageSize;
	private boolean export;
	
	public Integer getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public boolean isExport() {
		return export;
	}
	public void setExport(boolean export) {
		this.export = export;
	}
	
}
