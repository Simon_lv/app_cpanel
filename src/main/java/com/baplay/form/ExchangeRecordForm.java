package com.baplay.form;

import com.baplay.entity.ExchangeRecord;

public class ExchangeRecordForm extends ExchangeRecord {
	private static final long serialVersionUID = 1L;
	
	private Integer pageNo;
	private int pageSize;
	private boolean export;
	
	private String gameCode;
	
	public String getGameCode() {
		return gameCode;
	}
	public void setGameCode(String gameCode) {
		this.gameCode = gameCode;
	}
	public Integer getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public boolean isExport() {
		return export;
	}
	public void setExport(boolean export) {
		this.export = export;
	}
	
}
