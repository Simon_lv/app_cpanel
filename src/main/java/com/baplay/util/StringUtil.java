package com.baplay.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringUtil {

	private static final Logger logger = LoggerFactory
			.getLogger(StringUtil.class);

	/**
	 * 金额汇总专用格式化tag
	 * 
	 * 只取整数部分并且分组，小数不进位
	 * 
	 * @param bd
	 * @return
	 */
	public static String bigDecimalToFormattedString(BigDecimal bd) {
		String formattedString = null;
		try {
			DecimalFormatSymbols symbols = new DecimalFormatSymbols();
			symbols.setDecimalSeparator('.');
			symbols.setGroupingSeparator(',');

			DecimalFormat chCurrencyFormat = new DecimalFormat("#.##", symbols);
			chCurrencyFormat.setMaximumFractionDigits(2);

			formattedString = chCurrencyFormat.format(bd);
			BigDecimal bd2 = new BigDecimal(formattedString);
			DecimalFormat chCurrencyFormat2 = new DecimalFormat("###,###",
					symbols);
			chCurrencyFormat2.setGroupingSize(3);
			chCurrencyFormat2.setMaximumFractionDigits(0);
			chCurrencyFormat2.setRoundingMode(RoundingMode.DOWN);
			formattedString = chCurrencyFormat2.format(bd2);
		} catch (Exception e) {
			logger.error(String.format(
					"Unable to convert BigDecimal : %s to String. error : %s",
					new Object[] { bd, e.getMessage() }));
		}
		return formattedString;
	}

}
