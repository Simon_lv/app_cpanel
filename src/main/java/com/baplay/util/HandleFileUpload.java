package com.baplay.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.MessageFormat;

import org.springframework.web.multipart.MultipartFile;

public class HandleFileUpload { 
	//tmpUrl example:  URL.IMG_MISSION_GIRL_MISSION.toString()
	//tmpFilePath example:  URL.FTP_MISSION_GIRL_MISSION.toString()
	public static String upload(MultipartFile file, String tmpUrl, String tmpFilePath, int bigSize) throws Exception {
		String url = "";
        if (!file.isEmpty()) {
        	if(file.getBytes().length > bigSize) {
        		System.out.println("file is overSize filename="+file.getOriginalFilename());
        		return "overSize";
        	}
        	String filename = FileNameGenerator.byTime(file.getOriginalFilename());
        	url = fetchUrl(tmpUrl, filename);
        	upload(file.getInputStream(), tmpFilePath, filename);
        } else {
        	System.out.println("file is empty filename="+file.getOriginalFilename());
        }
         return url;
	}
	
	private static String fetchUrl(String imgUrl, String filename){
		String url = MessageFormat.format(imgUrl,filename);
        return url;
	}
	
	private static void upload(InputStream inputStream, String ftpPath, String filename) throws Exception {
		
		 try {
	           OutputStream os = new FileOutputStream(filename);
	             
	            byte[] buffer = new byte[1024];
	            int bytesRead;
	            //read from is to buffer
	            while((bytesRead = inputStream.read(buffer)) !=-1){
	                os.write(buffer, 0, bytesRead);
	            }
	            inputStream.close();
	            //flush OutputStream to write any buffered data to file
	            os.flush();
	            os.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		File f = new File(filename);
		FileUploadService.toFtp(f, ftpPath, filename);
    }
}
