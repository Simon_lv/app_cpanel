package com.baplay.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;

/**
 * 清除遠程的ecache本地緩存
 * @author Times
 *
 */
public class EcacheUtil {
	private static final Logger LOG = LoggerFactory.getLogger(EcacheUtil.class);
	/**
	 * 清除笑傲官網公告緩存
	 */
	public static void cleanXAJHAnnouncement(){
		String[] urls = {"http://203.69.240.206:8035/announce/evictCache",
				"http://203.69.240.207:8035/announce/evictCache",
				"http://203.69.240.208:8035/announce/evictCache",
				"http://203.69.240.209:8035/announce/evictCache"};
		for(String url : urls ){
			LOG.info("sendGet url={}",url);
			JSONObject result = HttpUtil.sendGet(url);
			LOG.info("final result={}",result.toJSONString());
		}
	}
	
	public static void main(String[] args) {
		cleanXAJHAnnouncement();
	}
}
