package com.baplay.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author jenco
 * time：2015-2-6 下午5:49:45
 * 通用屬性配置，可以把key-value寫在configuration.properties裏
 * 通过Configuration.getInstance().getProperty(key)使用
 */
public class Configuration {
	String config = "configuration.properties";
	private Properties properties = new Properties();
	private static Configuration instance = new Configuration();
	private static Map<String, String> area = new LinkedHashMap<String, String>();
	
	private  Configuration() {
		try {
			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(config);
			properties.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static Configuration getInstance() {
		return instance;
	}
	
	public String getProperty(String key) {
		return properties.getProperty(key);
	}
	
	public void setProperty(String key, String value) {
		properties.setProperty(key, value);
	}
	
	/**
	 * 台灣地區縣市
	 * 
	 * @return
	 */
	public Map<String, String> getAreaList() {
		if(area.isEmpty()) {		
			area.put("", "不限");
			area.put("基隆市", "基隆市");
			area.put("臺北市", "臺北市");
			area.put("新北市", "新北市");
			area.put("桃園市", "桃園市");
			area.put("新竹市", "新竹市");
			area.put("新竹縣", "新竹縣");
			area.put("苗栗縣", "苗栗縣");
			area.put("臺中市", "臺中市");
			area.put("彰化縣", "彰化縣");
			area.put("南投縣", "南投縣");
			area.put("雲林縣", "雲林縣");
			area.put("嘉義市", "嘉義市");
			area.put("嘉義縣", "嘉義縣");
			area.put("臺南市", "臺南市");
			area.put("高雄市", "高雄市");
			area.put("屏東縣", "屏東縣");
			area.put("臺東縣", "臺東縣");
			area.put("花蓮縣", "花蓮縣");
			area.put("宜蘭縣", "宜蘭縣");
			area.put("澎湖縣", "澎湖縣");
			area.put("金門縣", "金門縣");
			area.put("連江縣", "連江縣");
		}
		
		return area;
	}
}