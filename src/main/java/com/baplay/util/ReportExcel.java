package com.baplay.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Workbook;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFeatures;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public final class ReportExcel {
	/**
	 * 生成文件
	 * @param filename
	 * @param tmptitle
	 * @param dataTitles
	 * @param list
	 * @throws IOException
	 * @throws WriteException
	 */
	public final static void reportExcel(String filename, String tmptitle,
			String[] dataTitles, List<Object[]> list) throws IOException, WriteException {
		File file = new File(filename);
		BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(file));
		WritableWorkbook wbook = null;
		WritableSheet wsheet = null;
		wbook = Workbook.createWorkbook(os);

		int k = 0;
		int row = 0;
		WritableFont wfont = new WritableFont(WritableFont.createFont("宋体"),
				20, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,
				Colour.BLACK);

		WritableCellFormat wcfFC = new WritableCellFormat(wfont);

		wfont = new jxl.write.WritableFont(WritableFont.createFont("宋体"), 10,
				WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				Colour.BLACK);
		wcfFC = new WritableCellFormat(wfont);
		try {
			wcfFC.setBorder(Border.NONE, BorderLineStyle.NONE);
		} catch (WriteException e) {

			e.printStackTrace();
		}
		Label label;

		if (list != null && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				if (i % 50000 == 0) {
					wbook.createSheet(tmptitle + k, k);
					wsheet = wbook.getSheet(k);
					k++;
					mergeCellsAndInsertData(wsheet, 3, 0, 8, 0, tmptitle, wcfFC);
					for (int m = 0; m < dataTitles.length; m++) {

						// label = new Label(m, 1, dataTitles[m], wcfFC);//
						// 生成工作表标题
						label = new Label(m, 0, dataTitles[m], wcfFC);// 生成工作表标题

						try {
							wsheet.addCell(label);
						} catch (RowsExceededException e) {

							e.printStackTrace();
						} catch (WriteException e) {

							e.printStackTrace();
						}
					}
				}

				row = i % 50000;
				Object[] ob = list.get(i);
				for (int l = 0; l < ob.length; l++) {
					insertOne(wsheet, l, row + 1,
							ob[l] == null ? "" : ob[l].toString(), wcfFC);
				}
			}
			wbook.write();
			wbook.close();
			os.close();
			return;
		}

		return;
	}

	/**
	 * 生成 exsel 工具類
	 * 
	 * @param request
	 * @param response
	 * @param filename
	 * @param tmptitle
	 * @param dataTitles
	 * @param list
	 * @throws IOException
	 * @throws WriteException
	 * @author xiaopan 2012-12-18
	 */

	public final static void reportExcel(HttpServletRequest request,
			HttpServletResponse response, String filename, String tmptitle,
			String[] dataTitles, List list) throws IOException, WriteException {

		response.setContentType("application/msexcel");
		response.setHeader("Content-Disposition", "attachment; filename="
				+ URLEncoder.encode(filename + ".xls", "utf-8"));
		OutputStream os = response.getOutputStream();
		WritableWorkbook wbook = null;
		WritableSheet wsheet = null;
		wbook = Workbook.createWorkbook(os);

		int k = 0;
		int row = 0;
		/* wsheet = wbook.createSheet(tmptitle+k, k); */
		WritableFont wfont = new WritableFont(WritableFont.createFont("宋体"),
				20, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,
				Colour.BLACK);

		WritableCellFormat wcfFC = new WritableCellFormat(wfont);
		// wsheet.mergeCells(4, 0, 10, 0);//合并单元格 代表第一行 4列至10列 合并
		/* mergeCellsAndInsertData(wsheet, 3, 0, 8, 0, tmptitle, wcfFC); */

		wfont = new jxl.write.WritableFont(WritableFont.createFont("宋体"), 10,
				WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				Colour.BLACK);
		WritableFont wfontSed = new WritableFont(WritableFont.createFont("宋体"),
				10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				Colour.RED);
		wcfFC = new WritableCellFormat(wfont);
		WritableCellFormat wcfFCRed = new WritableCellFormat(wfontSed);
		try {
			wcfFC.setBorder(Border.NONE, BorderLineStyle.NONE);
		} catch (WriteException e) {

			e.printStackTrace();
		}
		Label label;
		int satr = 0;
		/*
		 * for (int i = 0; i < dataTitles.length; i++) {
		 * 
		 * label = new Label(i, 1, dataTitles[i], wcfFC);// 生成工作表标题 try {
		 * wsheet.addCell(label); } catch (RowsExceededException e) {
		 * 
		 * e.printStackTrace(); } catch (WriteException e) {
		 * 
		 * e.printStackTrace(); } satr++; }
		 */

		if (list != null && list.size() > 0) {

			for (int i = 0; i < list.size(); i++) {

				if (i % 50000 == 0) {

					wbook.createSheet(tmptitle + k, k);
					wsheet = wbook.getSheet(k);
					k++;
					mergeCellsAndInsertData(wsheet, 3, 0, 8, 0, tmptitle, wcfFC);
					for (int m = 0; m < dataTitles.length; m++) {

//						label = new Label(m, 1, dataTitles[m], wcfFC);// 生成工作表标题
						label = new Label(m, 0, dataTitles[m], wcfFC);// 生成工作表标题

						try {
							wsheet.addCell(label);
						} catch (RowsExceededException e) {

							e.printStackTrace();
						} catch (WriteException e) {

							e.printStackTrace();
						}
						satr++;
					}

				}

				// wsheet = wbook.getSheet(k);

				row = i % 50000;

				Object[] ob = (Object[]) list.get(i);

				for (int l = 0; l < ob.length; l++) {

//					insertOne(wsheet, l, row + 2,
//							ob[l] == null ? "" : ob[l].toString(), wcfFC);
					insertOne(wsheet, l, row+1,
							ob[l] == null ? "" : ob[l].toString(), wcfFC);

				}

			}

			wbook.write();
			wbook.close();
			os.close();
			return;
		}

		return;
	}

	/**
	 * 合并单元格，并插入数据
	 * 
	 * @param sheet
	 *            工作表
	 * @param col_start
	 *            开始列
	 * @param row_start
	 *            开始行
	 * @param col_end
	 *            停止列
	 * @param row_end
	 *            停止行
	 * @param data
	 *            插入数据
	 * @param format
	 *            风格
	 */
	private final static void mergeCellsAndInsertData(WritableSheet sheet,
			Integer col_start, Integer row_start, Integer col_end,
			Integer row_end, Object data, WritableCellFormat format) {
		try {
			//sheet.mergeCells(col_start, row_start, col_end, row_end);// 左上角到右下角
			insertOneCellData(sheet, col_start, row_start, data, format);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 简单插入单元格
	 * 
	 * @param sheet
	 *            工作表
	 * @param col
	 *            列
	 * @param row
	 *            行
	 * @param date
	 *            数据
	 * @param form
	 *            风格
	 */

	private final static void insertOne(WritableSheet sheet, Integer col,
			Integer row, String date, WritableCellFormat form) {
		try {
			Label label = new Label(col, row, date, form);
			sheet.addCell(label);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 插入单元格数据
	 * 
	 * @param sheet
	 *            工作表
	 * @param col
	 *            列号
	 * @param row
	 *            行号
	 * @param data
	 *            数据
	 */
	private final static void insertOneCellData(WritableSheet sheet,
			Integer col, Integer row, Object data, WritableCellFormat format) {
		try {
			if (data instanceof Double) {

				jxl.write.Number labelNF = new jxl.write.Number(col, row,
						(Double) data, format);
				sheet.addCell(labelNF);
			} else if (data instanceof Boolean) {
				jxl.write.Boolean labelB = new jxl.write.Boolean(col, row,
						(Boolean) data, format);
				sheet.addCell(labelB);
			} else if (data instanceof Date) {
				jxl.write.DateTime labelDT = new jxl.write.DateTime(col, row,
						(Date) data, format);
				sheet.addCell(labelDT);
				setCellComments(labelDT, "这是个创建表的日期说明！");
			} else {
				Label label = new Label(col, row, data.toString(), format);
				sheet.addCell(label);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 给单元格加注释
	 * 
	 * @param label
	 *            单元格
	 * @param comments
	 *            注释内容
	 */
	private final static void setCellComments(Object label, String comments) {
		WritableCellFeatures cellFeatures = new WritableCellFeatures();
		cellFeatures.setComment(comments); // 设置注释说明
		if (label instanceof jxl.write.Number) {
			jxl.write.Number num = (jxl.write.Number) label;
			num.setCellFeatures(cellFeatures);
		} else if (label instanceof jxl.write.Boolean) {
			jxl.write.Boolean bool = (jxl.write.Boolean) label;
			bool.setCellFeatures(cellFeatures);
		} else if (label instanceof jxl.write.DateTime) {
			jxl.write.DateTime dt = (jxl.write.DateTime) label;
			dt.setCellFeatures(cellFeatures);
		} else {
			Label _label = (Label) label;
			_label.setCellFeatures(cellFeatures);
		}
	}

	public final static void reportExcel(HttpServletRequest request,
			HttpServletResponse response, String filename, String tmptitle,
			String[] gashTitles, String[] iecardTitles, String[] molPayTitles,
			String[] payPalTitles, String[] googleTitles, String[] zhdxTitles,
			String[] cashUTitles, String[] tstoreTitles, String[] mycardTitles,
			List gashList, List iecardList, List molPayList, List payPalList,
			List googleList, List zhdxList, List cashUList, List tstoreList,
			List mycardList) throws Exception {
		response.setContentType("application/msexcel");
		response.setHeader("Content-Disposition", "attachment; filename="
				+ URLEncoder.encode(filename + ".xls", "utf-8"));

		OutputStream os = response.getOutputStream();
		WritableWorkbook wbook = null;
		WritableSheet wsheet = null;
		wbook = Workbook.createWorkbook(os);

		int k = 0;
		int row = 0;
		WritableFont wfont = new WritableFont(WritableFont.createFont("宋体"),
				20, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE,
				Colour.BLACK);

		WritableCellFormat wcfFC = new WritableCellFormat(wfont);

		wfont = new jxl.write.WritableFont(WritableFont.createFont("宋体"), 10,
				WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				Colour.BLACK);
		WritableFont wfontSed = new WritableFont(WritableFont.createFont("宋体"),
				10, WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				Colour.RED);
		wcfFC = new WritableCellFormat(wfont);
		WritableCellFormat wcfFCRed = new WritableCellFormat(wfontSed);
		try {
			wcfFC.setBorder(Border.NONE, BorderLineStyle.NONE);
		} catch (WriteException e) {

			e.printStackTrace();
		}
		Label label;
		int satr = 0;

		if (gashList != null && gashList.size() > 0) {
			for (int i = 0; i < gashList.size(); i++) {
				if (i % 50000 == 0) {
					wbook.createSheet(tmptitle + k, k);
					wsheet = wbook.getSheet(k);
					k++;
					mergeCellsAndInsertData(wsheet, 3, 0, 8, 0, tmptitle
							+ "gash", wcfFC);
					for (int m = 0; m < gashTitles.length; m++) {
						label = new Label(m, 1, gashTitles[m], wcfFC);// 生成工作表标题
						try {
							wsheet.addCell(label);
						} catch (RowsExceededException e) {
							e.printStackTrace();
						} catch (WriteException e) {
							e.printStackTrace();
						}
						satr++;
					}
				}
				row = i % 50000;
				Object[] ob = (Object[]) gashList.get(i);
				for (int l = 0; l < ob.length; l++) {
					insertOne(wsheet, l, row + 2,
							ob[l] == null ? "" : ob[l].toString(), wcfFC);
				}
			}
		}

		if (iecardList != null && iecardList.size() > 0) {
			for (int i = 0; i < iecardList.size(); i++) {
				if (i % 50000 == 0) {
					wbook.createSheet(tmptitle + k, k);
					wsheet = wbook.getSheet(k);
					k++;
					mergeCellsAndInsertData(wsheet, 3, 0, 8, 0, tmptitle
							+ "iecard", wcfFC);
					for (int m = 0; m < iecardTitles.length; m++) {
						label = new Label(m, 1, iecardTitles[m], wcfFC);// 生成工作表标题
						try {
							wsheet.addCell(label);
						} catch (RowsExceededException e) {
							e.printStackTrace();
						} catch (WriteException e) {
							e.printStackTrace();
						}
						satr++;
					}
				}
				row = i % 50000;
				Object[] ob = (Object[]) iecardList.get(i);
				for (int l = 0; l < ob.length; l++) {
					insertOne(wsheet, l, row + 2,
							ob[l] == null ? "" : ob[l].toString(), wcfFC);
				}
			}
		}

		if (molPayList != null && molPayList.size() > 0) {
			for (int i = 0; i < molPayList.size(); i++) {
				if (i % 50000 == 0) {
					wbook.createSheet(tmptitle + k, k);
					wsheet = wbook.getSheet(k);
					k++;
					mergeCellsAndInsertData(wsheet, 3, 0, 8, 0, tmptitle
							+ "molPay", wcfFC);
					for (int m = 0; m < molPayTitles.length; m++) {
						label = new Label(m, 1, molPayTitles[m], wcfFC);// 生成工作表标题
						try {
							wsheet.addCell(label);
						} catch (RowsExceededException e) {
							e.printStackTrace();
						} catch (WriteException e) {
							e.printStackTrace();
						}
						satr++;
					}
				}
				row = i % 50000;
				Object[] ob = (Object[]) molPayList.get(i);
				for (int l = 0; l < ob.length; l++) {
					insertOne(wsheet, l, row + 2,
							ob[l] == null ? "" : ob[l].toString(), wcfFC);
				}
			}
		}

		if (payPalList != null && payPalList.size() > 0) {
			for (int i = 0; i < payPalList.size(); i++) {
				if (i % 50000 == 0) {
					wbook.createSheet(tmptitle + k, k);
					wsheet = wbook.getSheet(k);
					k++;
					mergeCellsAndInsertData(wsheet, 3, 0, 8, 0, tmptitle
							+ "payPal", wcfFC);
					for (int m = 0; m < payPalTitles.length; m++) {
						label = new Label(m, 1, payPalTitles[m], wcfFC);// 生成工作表标题
						try {
							wsheet.addCell(label);
						} catch (RowsExceededException e) {
							e.printStackTrace();
						} catch (WriteException e) {
							e.printStackTrace();
						}
						satr++;
					}
				}
				row = i % 50000;
				Object[] ob = (Object[]) payPalList.get(i);
				for (int l = 0; l < ob.length; l++) {
					insertOne(wsheet, l, row + 2,
							ob[l] == null ? "" : ob[l].toString(), wcfFC);
				}
			}
		}

		if (googleList != null && googleList.size() > 0) {
			for (int i = 0; i < googleList.size(); i++) {
				if (i % 50000 == 0) {
					wbook.createSheet(tmptitle + k, k);
					wsheet = wbook.getSheet(k);
					k++;
					mergeCellsAndInsertData(wsheet, 3, 0, 8, 0, tmptitle
							+ "google", wcfFC);
					for (int m = 0; m < googleTitles.length; m++) {
						label = new Label(m, 1, googleTitles[m], wcfFC);// 生成工作表标题
						try {
							wsheet.addCell(label);
						} catch (RowsExceededException e) {
							e.printStackTrace();
						} catch (WriteException e) {
							e.printStackTrace();
						}
						satr++;
					}
				}
				row = i % 50000;
				Object[] ob = (Object[]) googleList.get(i);
				for (int l = 0; l < ob.length; l++) {
					insertOne(wsheet, l, row + 2,
							ob[l] == null ? "" : ob[l].toString(), wcfFC);
				}
			}
		}

		if (zhdxList != null && zhdxList.size() > 0) {
			for (int i = 0; i < zhdxList.size(); i++) {
				if (i % 50000 == 0) {
					wbook.createSheet(tmptitle + k, k);
					wsheet = wbook.getSheet(k);
					k++;
					mergeCellsAndInsertData(wsheet, 3, 0, 8, 0, tmptitle
							+ "中华电信", wcfFC);
					for (int m = 0; m < zhdxTitles.length; m++) {
						label = new Label(m, 1, zhdxTitles[m], wcfFC);// 生成工作表标题
						try {
							wsheet.addCell(label);
						} catch (RowsExceededException e) {
							e.printStackTrace();
						} catch (WriteException e) {
							e.printStackTrace();
						}
						satr++;
					}
				}
				row = i % 50000;
				Object[] ob = (Object[]) zhdxList.get(i);
				for (int l = 0; l < ob.length; l++) {
					insertOne(wsheet, l, row + 2,
							ob[l] == null ? "" : ob[l].toString(), wcfFC);
				}
			}
		}

		if (cashUList != null && cashUList.size() > 0) {
			for (int i = 0; i < cashUList.size(); i++) {
				if (i % 50000 == 0) {
					wbook.createSheet(tmptitle + k, k);
					wsheet = wbook.getSheet(k);
					k++;
					mergeCellsAndInsertData(wsheet, 3, 0, 8, 0, tmptitle
							+ "CashU", wcfFC);
					for (int m = 0; m < cashUTitles.length; m++) {
						label = new Label(m, 1, cashUTitles[m], wcfFC);// 生成工作表标题
						try {
							wsheet.addCell(label);
						} catch (RowsExceededException e) {
							e.printStackTrace();
						} catch (WriteException e) {
							e.printStackTrace();
						}
						satr++;
					}
				}
				row = i % 50000;
				Object[] ob = (Object[]) cashUList.get(i);
				for (int l = 0; l < ob.length; l++) {
					insertOne(wsheet, l, row + 2,
							ob[l] == null ? "" : ob[l].toString(), wcfFC);
				}
			}
		}

		if (tstoreList != null && tstoreList.size() > 0) {
			for (int i = 0; i < tstoreList.size(); i++) {
				if (i % 50000 == 0) {
					wbook.createSheet(tmptitle + k, k);
					wsheet = wbook.getSheet(k);
					k++;
					mergeCellsAndInsertData(wsheet, 3, 0, 8, 0, tmptitle
							+ "tstore", wcfFC);
					for (int m = 0; m < tstoreTitles.length; m++) {
						label = new Label(m, 1, tstoreTitles[m], wcfFC);// 生成工作表标题
						try {
							wsheet.addCell(label);
						} catch (RowsExceededException e) {
							e.printStackTrace();
						} catch (WriteException e) {
							e.printStackTrace();
						}
						satr++;
					}
				}
				row = i % 50000;
				Object[] ob = (Object[]) tstoreList.get(i);
				for (int l = 0; l < ob.length; l++) {
					insertOne(wsheet, l, row + 2,
							ob[l] == null ? "" : ob[l].toString(), wcfFC);
				}
			}
		}

		if (mycardList != null && mycardList.size() > 0) {
			for (int i = 0; i < mycardList.size(); i++) {
				if (i % 50000 == 0) {
					wbook.createSheet(tmptitle + k, k);
					wsheet = wbook.getSheet(k);
					k++;
					mergeCellsAndInsertData(wsheet, 3, 0, 8, 0, tmptitle
							+ "tstore", wcfFC);
					for (int m = 0; m < mycardTitles.length; m++) {
						label = new Label(m, 1, mycardTitles[m], wcfFC);// 生成工作表标题
						try {
							wsheet.addCell(label);
						} catch (RowsExceededException e) {
							e.printStackTrace();
						} catch (WriteException e) {
							e.printStackTrace();
						}
						satr++;
					}
				}
				row = i % 50000;
				Object[] ob = (Object[]) mycardList.get(i);
				for (int l = 0; l < ob.length; l++) {
					insertOne(wsheet, l, row + 2,
							ob[l] == null ? "" : ob[l].toString(), wcfFC);
				}
			}
		}
		wbook.write();
		wbook.close();
		os.close();
		return;
	}

}
