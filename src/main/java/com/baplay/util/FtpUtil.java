package com.baplay.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.SocketException;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import com.google.common.base.Strings;

public class FtpUtil {
	private static final String HOST = "cdn-ftp.idc.hinet.net";
	private static final int PORT = 21;
	private static final String username = "8888picupload";
	private static final String password = "PDoM9Wj7Tuh^";

	/**
	 * 
	 * @param file
	 *            上传的文件或文件夹
	 * @throws IOException
	 * @throws SocketException
	 * @throws Exception
	 */
	public static void upload8888(String path, File file)
			throws SocketException, IOException {
		FTPClient ftp = connect(path, HOST, PORT, "8888picupload",
				"PDoM9Wj7Tuh^");
		FileInputStream input = new FileInputStream(file);
		ftp.enterLocalPassiveMode();
		ftp.storeFile(file.getName(), input);
		ftp.logout();
		ftp.disconnect();
	}

	/**
	 * 
	 * @param file
	 *            上传的文件或文件夹
	 * @throws IOException
	 * @throws SocketException
	 * @throws Exception
	 */
	public static void upload(String path, File file) throws SocketException,
			IOException {
		FTPClient ftp = connect(path, HOST, PORT, username, password);
		FileInputStream input = new FileInputStream(file);
		ftp.enterLocalPassiveMode();
		ftp.storeFile(file.getName(), input);
		ftp.logout();
		ftp.disconnect();
		input.close();
	}

	/**
	 * 
	 * @param path
	 *            上传到ftp服务器哪个路径下
	 * @param addr
	 *            地址
	 * @param port
	 *            端口号
	 * @param username
	 *            用户名
	 * @param password
	 *            密码
	 * @return
	 * @throws IOException
	 * @throws SocketException
	 * @throws Exception
	 */
	private static FTPClient connect(String path, String addr, int port,
			String username, String password) throws SocketException,
			IOException {
		FTPClient ftp = new FTPClient();
		ftp.setControlEncoding("utf8");
		int reply;
		ftp.connect(addr, port);
		ftp.login(username, password);
		ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
		reply = ftp.getReplyCode();
		if (!FTPReply.isPositiveCompletion(reply)) {
			ftp.disconnect();
			return ftp;
		}
		ftp.changeWorkingDirectory("/");
		String[] paths = path.split("/");
		for (int i = 0; i < paths.length; i++) {
			if (!Strings.isNullOrEmpty(paths[i])) {
				ftp.makeDirectory(paths[i]);
				ftp.changeWorkingDirectory(paths[i]);
			}
		}
		System.out.println("ftp directory: " + path);
		ftp.changeWorkingDirectory(path);

		System.out.println("ftp current directory: "
				+ ftp.printWorkingDirectory());
		return ftp;
	}
}
