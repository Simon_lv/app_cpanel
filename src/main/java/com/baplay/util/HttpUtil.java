package com.baplay.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONObject;


public class HttpUtil{
	
	private static Logger logger=Logger.getLogger(HttpUtil.class);
	
	private static String ENCODEING_CHARSET="UTF-8";
	
	
	/**
	 * 判断是否为防盗链
	 * @param request
	 * 		请求对象
	 * @param prefix
	 * 		请求页面是否来自于该前缀
	 * @return
	 * 		如果返回为true，则表示为正常操作，否则为防盗链用户
	 */
	public static boolean preventSteal(HttpServletRequest request,String prefix){
		
		if(request==null||prefix==null||prefix.trim().length()==0){
			return false;
		}
		String referer=request.getHeader("referer");
		if(referer!=null&&referer.startsWith(prefix)){
			return true;
		}else{
			return false;
		}
		
	}
	
	/**
	 * Post方式调用远程链接
	 * @param uri 地址
	 * @param params 参数
	 * @return 是否成功 JSONObject
	 */
	public static String sendPost(String uri,Map<String, String> params){
		if(StringUtils.isEmpty(uri))return null;
		StringBuffer resBuffer = new StringBuffer(); 
		try {
			PostMethod method=new PostMethod(uri);
			if(params!=null){
				Object[] keys = params.keySet().toArray();
				for(Object objKey:keys){
					String key=String.valueOf(objKey);
					method.addParameter(key,params.get(key));
				}
			}
			HttpClient httpClient=new HttpClient();
			httpClient.getParams().setParameter(HttpMethodParams.HTTP_ELEMENT_CHARSET, ENCODEING_CHARSET);
			httpClient.executeMethod(method);
			//取出返回值
			InputStream resStream = method.getResponseBodyAsStream();  
			BufferedReader br = new BufferedReader(new InputStreamReader(resStream, ENCODEING_CHARSET));  
			String resTemp = "";  
			while ((resTemp = br.readLine()) != null) {  
	          resBuffer.append(resTemp);  
			}  
			return resBuffer.toString();
		}catch (ClassCastException e) {
			JSONObject object = new JSONObject();
			object.put("value", resBuffer.toString());
			return null;
		}catch (HttpException e) {
			logger.error("advertising registered activation failure ,message:"+e.getMessage());
		} catch (IOException e) {
			logger.error("advertising registered activation failure ,message:"+e.getMessage());
		}
		return null;
	}
	
	/**
	 * Get方式调用远程链接
	 * @param uri 地址 包含参数
	 * @return 是否成功 JSONObject
	 */
	public static JSONObject sendGet(String uri){
		if(StringUtils.isEmpty(uri))return null;
		StringBuffer resBuffer = new StringBuffer();
		try {
			GetMethod method=new GetMethod(uri);
			HttpClient httpClient=new HttpClient();
			httpClient.getParams().setParameter(HttpMethodParams.HTTP_ELEMENT_CHARSET, ENCODEING_CHARSET);
			httpClient.executeMethod(method);
			//取出返回值
			InputStream resStream = method.getResponseBodyAsStream();  
			BufferedReader br = new BufferedReader(new InputStreamReader(resStream, ENCODEING_CHARSET));  
			String resTemp = "";
			while ((resTemp = br.readLine()) != null) {  
	          resBuffer.append(resTemp);  
			}
			return JSONObject.parseObject(resBuffer.toString());
		}catch (ClassCastException e) {
			JSONObject object = new JSONObject();
			object.put("value", resBuffer.toString());
			logger.error("advertising registered activation failure ,message:"+resBuffer.toString());
			return object;
		}catch (HttpException e) {
			logger.error("advertising registered activation failure ,message:"+e.getMessage());
		} catch (IOException e) {
			logger.error("advertising registered activation failure ,message:"+e.getMessage());
		}
		return null;
	}
	
	/**
	 * 發送請求(url,map自動轉換為get方式發送請求)
	 * todo<br>
	 * 作者:冷诚贞 Email:lengchengzhen@efun.com
	 * 時間:2014-2-22 下午02:30:27<br>
	 * @param uri
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public static JSONObject sendGetByMapForm(String uri,Map<String,String> map) throws UnsupportedEncodingException{
		if(null==uri||null==map||map.size()<=0){
			return null;
		}
		StringBuffer sb=new StringBuffer(uri+"?");
		Iterator<Entry<String,String>> it=map.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String,String> e=it.next();
			sb.append(e.getKey()+"=").append(URLEncoder.encode(e.getValue(),"utf-8")+"&");
		}
		
		String url=sb.toString();
		url=url.substring(0, url.length()-1);
		
		logger.info("[push url]: "+url);
		
		return sendGet(url);
	}
	/**
	 * Get方式调用远程链接
	 * @param uri 地址 包含参数
	 * @return 是否成功 String
	 */
    @Deprecated
	public static String toSendGet(String url){
		if(StringUtils.isEmpty(url))return null;
		StringBuffer resBuffer = new StringBuffer();  
		try {
			GetMethod method=new GetMethod();
			HttpClient httpClient=new HttpClient();
			HttpClientParams params = httpClient.getParams();
			
			params.setHttpElementCharset("GBK");
			params.setContentCharset("GBK");
			
			URI uri = new URI(url,true,"GBK");
			
			method.setURI(uri);
			httpClient.executeMethod(method);
			//取出返回值
			InputStream resStream = method.getResponseBodyAsStream();  
			BufferedReader br = new BufferedReader(new InputStreamReader(resStream, ENCODEING_CHARSET));  
			String resTemp = "";  
			while ((resTemp = br.readLine()) != null) {  
	          resBuffer.append(resTemp);  
			}  
			return resBuffer.toString();
		}catch (ClassCastException e) {
			return resBuffer.toString();
		}catch (HttpException e) {
			logger.error("advertising registered activation failure ,message:"+e.getMessage());
		} catch (IOException e) {
			logger.error("advertising registered activation failure ,message:"+e.getMessage());
		}
		return null;
	}
	
	/**
	 * Get方式调用远程链接
	 * @param uri 地址 包含参数
	 * @return 是否成功 String
	 */
	public static List<String> toSendGetReadLine(String uri){
		if(StringUtils.isEmpty(uri))return null;
		List<String> lists = new ArrayList<String>();
		try {
			GetMethod method=new GetMethod(uri);
			HttpClient httpClient=new HttpClient();
			httpClient.getParams().setParameter(HttpMethodParams.HTTP_ELEMENT_CHARSET, ENCODEING_CHARSET);
			httpClient.executeMethod(method);
			//取出返回值
			InputStream resStream = method.getResponseBodyAsStream();  
			BufferedReader br = new BufferedReader(new InputStreamReader(resStream, ENCODEING_CHARSET));  
			String resTemp = "";  
			while ((resTemp = br.readLine()) != null) {  
				lists.add(resTemp);
			}  
			return lists;
		}catch (ClassCastException e) {
			return lists;
		}catch (HttpException e) {
			logger.error("advertising registered activation failure ,message:"+e.getMessage());
		} catch (IOException e) {
			logger.error("advertising registered activation failure ,message:"+e.getMessage());
		}
		return lists;
	}
	
	/**
	 * 获取访问地址的返回值
	 * @param urlAddress URL地址
	 * @param charset 编码格式 默认GBK
	 * @return URL的返回内容
	 */
    public static String getUrlReturnValue(String urlAddress,String charset) {
        try {
        	if (StringUtils.isEmpty(charset))charset="GBK";
            URL url = new URL(urlAddress);
            URLConnection conn = url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    conn.getInputStream(), charset));
            String line = null;
            StringBuffer result = new StringBuffer();
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
            reader.close();
           return String.valueOf(result);
        } catch (Exception e) {
            logger.error(e);
            return "";
        }
    }
    
	/**
	 * Get方式调用远程链接
	 * @param uri 地址 包含参数
	 * @return 是否成功 JSONObject
	 */
	public static void asynSendGet(final String uri){
		if(StringUtils.isEmpty(uri))return;
		Runnable t=new Runnable() {
			public void run() {
				try {
					GetMethod method=new GetMethod(uri);
					HttpClient httpClient=new HttpClient();
					httpClient.getParams().setParameter(HttpMethodParams.HTTP_ELEMENT_CHARSET, ENCODEING_CHARSET);
					httpClient.executeMethod(method);
				} catch (HttpException e) {
					logger.error("advertising registered activation failure ,message:"+e.getMessage());
				} catch (IOException e) {
					logger.error("advertising registered activation failure ,message:"+e.getMessage());
				}
			}
		};
		Thread th = new Thread(t);
		th.setPriority(Thread.MIN_PRIORITY);
		th.start();
	}
	
	/**
	 *  《BM》加密处理 调用原厂接口
	 * @param url
	 * @param data
	 * @return
	 * @throws IOException
	 */
	public static JSONObject  PostDataJson(String url, byte[] data) throws IOException {
        
        return JSONObject.parseObject(postSend(url,data));
    }
	
	public static String  PostDataStr(String url, byte[] data) throws IOException {
        return postSend(url,data);
    }
	
	private static  String postSend(String url, byte[] data) throws IOException {
		HttpClient httpClient = new HttpClient();
        Protocol myhttps = new Protocol("https", new MySSLProtocolSocketFactory(), 443);
        Protocol.registerProtocol("https", myhttps);
        PostMethod postMethod = null;
        InputStream inputStream = null;
        InputStream retData = null;
        BufferedReader reader = null;
        String ret = "";
        try {
            postMethod = new PostMethod(url);
            postMethod.getParams().setParameter(HttpMethodParams.SO_TIMEOUT, 20000);
            if (null != data && data.length > 0) {
                byte[] dataByte = data;
                inputStream = new ByteArrayInputStream(dataByte);
                RequestEntity re = new InputStreamRequestEntity(inputStream, dataByte.length);
                postMethod.setRequestEntity(re);
            }
            httpClient.executeMethod(postMethod);
            retData = postMethod.getResponseBodyAsStream();
            StringBuffer sb = new StringBuffer();
            if (null != retData) {
                reader = new BufferedReader(new InputStreamReader(retData, "utf-8"));
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            }
            ret = sb.toString();
            
        } catch (Exception ex) {
        	ex.printStackTrace();
        } finally {
            if (null != reader) reader.close();
            if (null != retData) retData.close();
            if (null != inputStream) inputStream.close();
            if (null != postMethod) {
                postMethod.abort();
                postMethod.releaseConnection();
            }
        }
        
        return ret;
	}
	
}
