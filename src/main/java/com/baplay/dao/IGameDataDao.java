package com.baplay.dao;

import java.util.List;
import java.util.Map;

import org.springside.modules.orm.Page;

import com.baplay.entity.GameData;
import com.baplay.form.GameDataForm;

public interface IGameDataDao {
	public List<Map<String, Object>> getGameCodeName();
	
	public List<Map<String, Object>> getGameIdName1();

	public List<?> checkExist(GameData gameData);
	
	public Page<GameData> list(GameDataForm gameDataForm);

	public GameData findOneById(Long id);

	public Object add(GameData gameData);

	public int update(GameData gameData);

	public int delete(Long id, String modifier);

}
