package com.baplay.dao;

import java.util.List;

import org.springside.modules.orm.Page;

import com.baplay.entity.SystemNotify;
import com.baplay.form.SystemNotifyForm;

public interface ISystemNotifyDao {

	public Page<SystemNotify> list(SystemNotifyForm systemNotifyForm);

	public SystemNotify findOneById(Long id);

	public Object add(SystemNotify systemNotify);

	public int update(SystemNotify systemNotify);

	public int delete(Long id, String modifier);
	
	public void add(List<SystemNotify> systemNotifyList);
	
}
