package com.baplay.dao;

import org.springside.modules.orm.Page;

import com.baplay.entity.ExchangeRecord;
import com.baplay.form.ExchangeRecordForm;

public interface IExchangeRecordDao {

	public Page<ExchangeRecord> list(ExchangeRecordForm exchangeRecordForm);
	
}
