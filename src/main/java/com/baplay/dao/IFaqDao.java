package com.baplay.dao;

import org.springside.modules.orm.Page;

import com.baplay.entity.Faq;
import com.baplay.form.FaqForm;

public interface IFaqDao {

	public Page<Faq> list(FaqForm faqForm);
	
	public Faq findOneById(Long id);
	
	public Faq save(Faq faq);
	
	public int update(Faq faq);
	
	public int delete(Long id);	
	
}
