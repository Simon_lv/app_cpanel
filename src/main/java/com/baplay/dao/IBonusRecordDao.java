package com.baplay.dao;

import org.springside.modules.orm.Page;

import com.baplay.entity.BonusRecord;
import com.baplay.form.BonusRecordForm;

public interface IBonusRecordDao {

	public Page<BonusRecord> list(BonusRecordForm bonusRecordForm);
	
}
