package com.baplay.dao;

import java.util.List;

import com.baplay.entity.TEfunuser;
import com.baplay.form.UserForm;

public interface IUserDao {

	/**
	 * 添加用户
	 * 
	 * @param form
	 * @return
	 */
	public Long addUser(UserForm form);

	/**
	 * 为用户添加角色
	 * 
	 * @param userId
	 * @param roleId
	 */
	public void addUserRole(Long userId, List<Long> roleId);

	/**
	 * 更新用户
	 * 
	 * @param form
	 */
	public void updateUser(UserForm form);

	/**
	 * 更新密码
	 * 
	 * @param userId
	 * @param password
	 */
	public void updatePwd(Long userId, String password);

	/**
	 * 更新状态
	 * 
	 * @param userId
	 * @param status
	 */
	public void updateUserStatus(Long userId, Integer status);

	/**
	 * 按用户名查询
	 * 
	 * @param username
	 * @return
	 */
	public TEfunuser findByUsername(String username);

	public List<TEfunuser> getAllUser(String userName);

	public UserForm findById(Long id);
}
