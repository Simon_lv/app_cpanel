package com.baplay.dao;

import java.util.List;

import org.springside.modules.orm.Page;

import com.baplay.entity.GameType;
import com.baplay.form.GameTypeForm;

public interface IGameTypeDao {

	public Page<GameType> list(GameTypeForm gameTypeForm);
	
	public GameType findOneById(Long id);
	
	public GameType save(GameType gameType);
	
	public int update(GameType gameType);
	
	public List<GameType> findAll();
	
	public int updateStatus(Long id, int status);
	
}
