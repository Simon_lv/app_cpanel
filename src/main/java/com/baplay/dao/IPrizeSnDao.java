package com.baplay.dao;

import org.springside.modules.orm.Page;

import com.baplay.entity.PrizeSn;
import com.baplay.form.PrizeSnForm;

public interface IPrizeSnDao {

	public Page<PrizeSn> list(PrizeSnForm prizeSnForm);
	
	public PrizeSn edit(Long id);	
	
}
