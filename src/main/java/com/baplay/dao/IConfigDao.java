package com.baplay.dao;

import org.springside.modules.orm.Page;

import com.baplay.entity.Config;
import com.baplay.form.ConfigForm;

public interface IConfigDao {

public Page<Config> list(ConfigForm videoConfigForm);
	
	public Config findOneById(Long id);
	
	public Config save(Config videoConfig);
	
	public int update(Config videoConfig);
	
	public int delete(Long id);
	
}
