package com.baplay.dao;

import org.springside.modules.orm.Page;

import com.baplay.entity.GameRecomm;
import com.baplay.form.GameRecommForm;

public interface IGameRecommDao {

	public Page<GameRecomm> list(GameRecommForm gameRecommForm);
	
	public GameRecomm findById(Long id);
	
	public GameRecomm save(GameRecomm gameRecomm);
	
	public int update(GameRecomm gameRecomm);
	
	public int delete(Long id);
	
}
