package com.baplay.dao;

import java.util.List;

import org.springside.modules.orm.Page;

import com.baplay.entity.Prize;
import com.baplay.entity.PrizeSn;
import com.baplay.form.PrizeForm;

public interface IPrizeDao {

	public Page<Prize> list(PrizeForm prizeForm);

	public Prize findOneById(Long id);

	public Object add(Prize prize);

	public int update(Prize prize);

	public int delete(Long id, String modifier);

	public void add(List<PrizeSn> prizeSnList);
}
