package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IGameRecordDao;
import com.baplay.entity.GameRecord;
import com.baplay.form.GameRecordForm;
import com.baplay.infra.dao.exceptions.DoubleInsertException;

@Repository
@Transactional
public class GameRecordDaoImpl extends BaseDao<GameRecord> implements IGameRecordDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<GameRecord> list(GameRecordForm gameRecordForm) {
		StringBuilder sql = new StringBuilder("SELECT gr.*,(select gameName from t_games where gameCode = gr.gameCode) gameName, (select name from game_type where gr.gameTypeId=id) gameType FROM game_record gr WHERE gr.status=1");
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM game_record gr WHERE gr.status=1");
		ArrayList<Object> param = new ArrayList<Object>();
		if (StringUtils.isNotBlank(gameRecordForm.getGameCode())) {
			sql.append(" AND gr.gameCode = ? ");
			countSql.append(" AND gr.gameCode = ? ");
			param.add(gameRecordForm.getGameCode());
		}
		if (StringUtils.isNotBlank(gameRecordForm.getTimeStart())) {
			sql.append(" AND gr.`createTime` >= ? ");
			countSql.append(" AND gr.`createTime` >= ? ");
			param.add(gameRecordForm.getTimeStart());
		}
		if (StringUtils.isNotBlank(gameRecordForm.getTimeEnd())) {
			sql.append(" AND gr.`createTime` <= ? ");
			countSql.append(" AND gr.`createTime` <= ? ");
			param.add(gameRecordForm.getTimeEnd());
		}
		
		sql.append(" ORDER BY COALESCE(gr.order_index,999), gr.createTime DESC");
		
		if (!gameRecordForm.isExport()) {
			sql.append(" LIMIT ").append((gameRecordForm.getPageNo() - 1) * gameRecordForm.getPageSize()).append(",").append(gameRecordForm.getPageSize());
		}
		Page<GameRecord> page = new Page<GameRecord>(gameRecordForm.getPageSize());
		page = (Page<GameRecord>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(), param.toArray(),
				new BeanPropertyRowMapper<GameRecord>(GameRecord.class));
		page.setPageNo(gameRecordForm.getPageNo());
		return page;
	}

	@Override
	public GameRecord findOneById(Long id) {
		String sql = "SELECT * FROM game_record WHERE id=?";
		Object[] args = new Object[] { id };
		return (GameRecord) super.queryForObject(dsBmsQry, sql, args, GameRecord.class);
	}

	@Override
	public Object add(GameRecord gameRecord) {
		String sqlQ = "SELECT * FROM game_record WHERE status=1 and gameCode=?";
		GameRecord gr = (GameRecord) super.queryForObject(dsBmsQry, sqlQ, new Object[]{gameRecord.getGameCode()}, GameRecord.class);
		
		if(gr == null){
			String sql = "INSERT INTO game_record ( gameTypeId, gameCode, packageName, type, iconUrl, apkUrl, iosUrl, content, version, size, videoUrl, videoImgUrl, imgUrl1, imgUrl2, imgUrl3, imgUrl4, imgUrl5, createTime, status, title, subTitle, order_index ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(),1, ?, ?, ?)";
			return (GameRecord) super.addForObject(
					dsBmsUpd,
					sql,
					gameRecord,
					new Object[] { gameRecord.getGameTypeId(), gameRecord.getGameCode(), gameRecord.getPackageName(), gameRecord.getType(), gameRecord.getIconUrl(), gameRecord.getApkUrl(),
							gameRecord.getIosUrl(), gameRecord.getContent(), gameRecord.getVersion(), gameRecord.getSize(), gameRecord.getVideoUrl(), gameRecord.getVideoImgUrl(),
							gameRecord.getImgUrl1(), gameRecord.getImgUrl2(), gameRecord.getImgUrl3(), gameRecord.getImgUrl4(), gameRecord.getImgUrl5(),
							gameRecord.getTitle(), gameRecord.getSubTitle(), gameRecord.getOrderIndex() });
		}else{
			throw new DoubleInsertException("AAAA", "遊戲code資料已存在");
		}
	}

	@Override
	public int update(GameRecord gameRecord) {		
		String sql = "UPDATE game_record SET gameTypeId=?, gameCode=?, packageName=?, type=?, iconUrl=?, apkUrl=?, iosUrl=?, content=?, version=?, size=?, videoUrl=?, videoImgUrl=?, imgUrl1=?, imgUrl2=?, imgUrl3=?, imgUrl4=?, imgUrl5=?, createTime=NOW(), title=?, subTitle=?, order_index=? WHERE id=?";
		return super.updateForObject(
				dsBmsUpd,
				sql,
				new Object[] { gameRecord.getGameTypeId(), gameRecord.getGameCode(), gameRecord.getPackageName(), gameRecord.getType(), gameRecord.getIconUrl(), gameRecord.getApkUrl(),
						gameRecord.getIosUrl(), gameRecord.getContent(), gameRecord.getVersion(), gameRecord.getSize(), gameRecord.getVideoUrl(), gameRecord.getVideoImgUrl(),
						gameRecord.getImgUrl1(), gameRecord.getImgUrl2(), gameRecord.getImgUrl3(), gameRecord.getImgUrl4(), gameRecord.getImgUrl5(),
						gameRecord.getTitle(), gameRecord.getSubTitle(), gameRecord.getOrderIndex(), gameRecord.getId() });
	}

	@Override
	public int delete(Long id, String modifier) {
		String sql = "UPDATE game_record SET status=2 WHERE id=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[] { id });
	}

	@Override
	public List<Map<String, Object>> getGames() {
		return super
				.queryForListMap(
						dsBmsQry,
						"SELECT g.* FROM (SELECT gameCode FROM game_record GROUP BY gameCode) gr JOIN (SELECT gameCode, gameName FROM t_games) g ON g.gameCode = gr.gameCode",
						new Object[] {});
	}

}
