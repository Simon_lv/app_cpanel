package com.baplay.dao.impl;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IMissionDao;
import com.baplay.entity.Mission;
import com.baplay.form.MissionForm;

@Repository
@Transactional
public class MissionDaoImpl extends BaseDao<Mission> implements IMissionDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}

	@Override
	public Mission add(Mission mission) {
		String sql = "INSERT INTO `mission` (`gameCode`, `type`, `iconUrl`, `content`, `bonus`, `prize_id`, `order_index`, `createTime`, `status`) VALUES (?, ?, ?, ?, ?, ?, ?, NOW(), '1')";
		return (Mission) super.addForObject(dsBmsUpd, sql, mission,
				new Object[] { mission.getGameCode(), mission.getType(), mission.getIconUrl(), mission.getContent(), mission.getBonus(), mission.getPrizeId(),
						mission.getOrderIndex() });
	}

	@Override
	public int update(Mission mission) {
		String sql = "UPDATE `mission` SET `gameCode`=?, `type`=?, `iconUrl`=?, `content`=?, `bonus`=?, `prize_id`=?, `order_index`=? WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[] { mission.getGameCode(), mission.getType(), mission.getIconUrl(), mission.getContent(),
				mission.getBonus(), mission.getPrizeId(), mission.getOrderIndex(), mission.getId() });
	}

	@Override
	public Mission findOneById(Long id) {
		String sql = "SELECT * FROM `mission` WHERE id=?";
		Object[] args = new Object[] { id };
		return (Mission) super.queryForObject(dsBmsQry, sql, args, Mission.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<Mission> list(MissionForm missionForm) {
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();

		if (StringUtils.isNotBlank(missionForm.getType())) {
			sb.append(" AND m.type = ? ");
			param.add(missionForm.getType());
		}

		if (StringUtils.isNotBlank(missionForm.getGameCode())) {
			sb.append(" AND m.gameCode = ? ");
			param.add(missionForm.getGameCode());
		}

		// sb.append(" AND `status` = 1 ");

		StringBuilder sql = new StringBuilder("SELECT m.*,(SELECT gameName FROM t_games WHERE gameCode=m.gameCode) gameName FROM mission m WHERE 1=1 ").append(sb.toString());
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM mission m WHERE 1=1").append(sb.toString());
		sql.append(" ORDER BY coalesce(m.order_index,999), m.createTime DESC ");
		if (!missionForm.isExport()) {
			sql.append(" LIMIT ").append((missionForm.getPageNo() - 1) * missionForm.getPageSize()).append(",").append(missionForm.getPageSize());
		}
		Page<Mission> page = new Page<Mission>(missionForm.getPageSize());

		page = (Page<Mission>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(), param.toArray(), new BeanPropertyRowMapper<Mission>(
				Mission.class));
		page.setPageNo(missionForm.getPageNo());
		return page;
	}

	@Override
	public int updateStatus(Long id, int status) {
		String sql = "UPDATE `mission` SET `status`=? WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[] { status, id });
	}

}
