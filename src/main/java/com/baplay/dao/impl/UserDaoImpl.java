/**
 * 
 */
package com.baplay.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baplay.dao.IUserDao;
import com.baplay.entity.TEfunuser;
import com.baplay.entity.UserRole;
import com.baplay.form.UserForm;
import com.google.common.base.Strings;

/**
 * @author Haozi
 *
 */
@Repository
@Transactional
public class UserDaoImpl extends BaseDao<TEfunuser> implements IUserDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IUserDao#addUser(com.baplay.form.UserForm)
	 */
	@Override
	public Long addUser(UserForm form) {
		TEfunuser old = (TEfunuser) this.queryForObject(dsBmsQry,
				"select * from t_app_user where username = ?",
				new Object[] { form.getUsername() }, TEfunuser.class);
		if (old != null) {
			return old.getUserid();
		}
		TEfunuser user = new TEfunuser();
		user.setUsername(form.getUsername());
		user.setFunname(form.getFunname());
		user.setPassword(form.getPassword());
		user.setMobile(form.getMobile());
		user = (TEfunuser) this
				.addForParam(
						dsBmsUpd,
						"INSERT INTO `t_app_user`(funname,username,password,mobile,delesign,status) VALUES(?,?,?,?,0,0)",
						user, new String[] { "funname", "username", "password",
								"mobile" });
		return user.getId();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IUserDao#addUserRole(Long, java.util.List)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void addUserRole(Long userId, List<Long> roleId) {
		List<UserRole> old = this
				.queryForList(
						dsBmsQry,
						"select * from t_app_user_role where userId = ? AND status = 0",
						new Object[] { userId },
						new BeanPropertyRowMapper<UserRole>(UserRole.class));
		for (int i = 0; i < old.size(); i++) {
			this.updateForObject(dsBmsUpd,
					"update t_app_user_role set status = ? where id=?",
					new Object[] { 1, old.get(i).getId() });
		}
		UserRole ur = new UserRole();
		for (int i = 0; i < roleId.size(); i++) {
			ur.setRoleId(roleId.get(i));
			ur.setUserId(userId);
			this.addForParam(
					dsBmsUpd,
					"insert into t_app_user_role (roleId,userId,status)values(?,?,0)",
					ur, new String[] { "roleId", "userId" });
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IUserDao#updateUser(com.baplay.form.UserForm)
	 */
	@Override
	public void updateUser(UserForm form) {
		String sql = "UPDATE `t_app_user` u SET u.`funname`=?,u.`username`=?,u.`password`=?,u.`mobile`=? WHERE u.`userid`=?";
		if (form.getPassword() == null) {
			sql = "UPDATE `t_app_user` u SET u.`funname`=?,u.`username`=?,u.`mobile`=? WHERE u.`userid`=?";
			this.updateForObject(
					dsBmsUpd,
					sql,
					new Object[] { form.getFunname(), form.getUsername(),
							form.getMobile(), form.getUserId() });
		} else {
			this.updateForObject(
					dsBmsUpd,
					sql,
					new Object[] { form.getFunname(), form.getUsername(),
							form.getPassword(), form.getMobile(),
							form.getUserId() });
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IUserDao#updatePwd(Long, String)
	 */
	@Override
	public void updatePwd(Long userId, String password) {
		String sql = "UPDATE `t_app_user` u SET u.`password`=? WHERE u.`userid`=?";
		this.updateForObject(dsBmsUpd, sql, new Object[] { password, userId });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IUserDao#updateUserStatus(Long, Integer)
	 */
	@Override
	public void updateUserStatus(Long userId, Integer status) {
		String sql = "UPDATE `t_app_user` u SET u.`status`=? WHERE u.`userid`=?";
		this.updateForObject(dsBmsUpd, sql, new Object[] { status, userId });
	}

	@Override
	public TEfunuser findByUsername(String username) {
		String sql = "select * from t_app_user where username=? and status = 0 LIMIT 1";
		return (TEfunuser) this.queryForObject(dsBmsQry, sql,
				new Object[] { username },
				new BeanPropertyRowMapper<TEfunuser>(TEfunuser.class));
	}

	@Override
	public UserForm findById(Long id) {
		String sql = "select u.* from t_app_user u where u.userid = ? and u.`status` = 0 LIMIT 1";
		UserForm user = (UserForm) this.queryForObject(dsBmsQry, sql,
				new Object[] { id }, new BeanPropertyRowMapper<UserForm>(
						UserForm.class));
		List<Long> roleIds = this
				.queryForLongList(
						dsBmsQry,
						"select roleId from t_app_user_role where userId = ? and status = 0",
						new Object[] { id });
		if (roleIds != null) {
			if (roleIds.size() > 0) {
				user.setRoleId1(roleIds.get(0));
			}
			if (roleIds.size() > 1) {
				user.setRoleId2(roleIds.get(1));
			}
		}
		return user;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TEfunuser> getAllUser(String userName) {
		if (Strings.isNullOrEmpty(userName)) {
			return this
					.queryForList(
							dsBmsQry,
							"select u.userid, u.mobile,GROUP_CONCAT(r.`name`) 'name' ,u.username,u.funname,u.`status` from t_app_user u LEFT JOIN t_app_user_role ur ON u.userid = ur.userId AND ur.`status` = 0 LEFT JOIN t_app_role r ON ur.roleId = r.id GROUP BY u.userid",
							null, TEfunuser.class);
		} else {
			return this
					.queryForList(
							dsBmsQry,
							"select u.userid, u.mobile,GROUP_CONCAT(r.`name`) 'name' ,u.username,u.funname,u.`status` from t_app_user u LEFT JOIN t_app_user_role ur ON u.userid = ur.userId AND ur.`status` = 0 LEFT JOIN t_app_role r ON ur.roleId = r.id WHERE u.username = ? GROUP BY u.userid",
							new Object[] { userName }, TEfunuser.class);
		}
	}
	

}
