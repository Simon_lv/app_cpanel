package com.baplay.dao.impl;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IFaqDao;
import com.baplay.entity.Faq;
import com.baplay.form.FaqForm;

@Repository
@Transactional
public class FaqDaoImpl extends BaseDao<Faq> implements IFaqDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Page<Faq> list(FaqForm faqForm) {
		StringBuilder sql = new StringBuilder("SELECT f.* FROM faq f WHERE f.status=1");
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM faq f WHERE f.status=1");
		ArrayList<Object> param = new ArrayList<Object>();
		if (faqForm.getType() > 0) {
			sql.append(" AND f.type = ? ");
			countSql.append(" AND f.type = ? ");
			param.add(faqForm.getType());
		}
		
		sql.append(" ORDER BY COALESCE(f.orderIndex,999), f.createTime DESC");		
		
		if (!faqForm.isExport()) {
			sql.append(" LIMIT ").append((faqForm.getPageNo() - 1) * faqForm.getPageSize()).append(",").append(faqForm.getPageSize());
		}
		Page<Faq> page = new Page<Faq>(faqForm.getPageSize());
		page = (Page<Faq>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(), param.toArray(), new BeanPropertyRowMapper<Faq>(
				Faq.class));
		page.setPageNo(faqForm.getPageNo());
		return page;
	}

	@Override
	public Faq findOneById(Long id) {
		String sql = "SELECT * FROM faq WHERE id=?";
		return (Faq) this.queryForObject(dsBmsQry, sql, new Object[]{id}, Faq.class);
	}

	@Override
	public Faq save(Faq faq) {
		String sql = "INSERT INTO faq (type, title, content, orderIndex, position, creator, createTime, status) VALUES (?,?,?,?,?,?,?,?,NOW(),1)";
		return (Faq) this.addForObject(dsBmsUpd, sql, faq, new Object[]{
				faq.getType(), faq.getTitle(), faq.getContent(), faq.getOrderIndex(), faq.getPosition(),
				faq.getCreator()
		});
	}

	@Override
	public int update(Faq faq) {
		String sql = "UPDATE faq SET type=?, title=?, content=?, orderIndex=?, position=?, modifier=?, updateTime=NOW() WHERE id=?";
		return this.updateForObject(dsBmsUpd, sql, new Object[]{
				faq.getType(), faq.getTitle(), faq.getContent(), faq.getOrderIndex(), faq.getPosition(), faq.getModifier(),
				faq.getId()
		});
	}

	@Override
	public int delete(Long id) {
		String sql = "UPDATE faq SET status=2 WHERE id=?";
		return this.updateForObject(dsBmsUpd, sql, new Object[]{id});
	}	

}
