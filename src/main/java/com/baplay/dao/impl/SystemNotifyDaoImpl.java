package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.ISystemNotifyDao;
import com.baplay.entity.SystemNotify;
import com.baplay.form.SystemNotifyForm;

@Repository
@Transactional
public class SystemNotifyDaoImpl extends BaseDao<SystemNotify> implements ISystemNotifyDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<SystemNotify> list(SystemNotifyForm systemNotifyForm) {
		StringBuilder sql = new StringBuilder("SELECT * FROM system_notify WHERE 1=1 ");
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM system_notify WHERE 1=1 ");
		ArrayList<Object> param = new ArrayList<Object>();
		if (StringUtils.isNotBlank(systemNotifyForm.getUserId())) {
			sql.append(" AND userId = ? ");
			countSql.append(" AND userId = ? ");
			param.add(systemNotifyForm.getUserId());
		}
		if (StringUtils.isNotBlank(systemNotifyForm.getTimeStart())) {
			sql.append(" AND `createTime` >= ? ");
			countSql.append(" AND `createTime` >= ? ");
			param.add(systemNotifyForm.getTimeStart());
		}
		if (StringUtils.isNotBlank(systemNotifyForm.getTimeEnd())) {
			sql.append(" AND `createTime` <= ? ");
			countSql.append(" AND `createTime` <= ? ");
			param.add(systemNotifyForm.getTimeEnd());
		}
		if (StringUtils.isNotBlank(systemNotifyForm.getTitle())) {
			sql.append(" AND `title` like '%" + systemNotifyForm.getTitle() + "%' ");
			countSql.append(" AND `title` like '%" + systemNotifyForm.getTitle() + "%' ");
		}
		
		sql.append(" ORDER BY createTime DESC");
		
		if (!systemNotifyForm.isExport()) {
			sql.append(" LIMIT ").append((systemNotifyForm.getPageNo() - 1) * systemNotifyForm.getPageSize()).append(",")
					.append(systemNotifyForm.getPageSize());
		}
		Page<SystemNotify> page = new Page<SystemNotify>(systemNotifyForm.getPageSize());
		page = (Page<SystemNotify>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(), param.toArray(),
				new BeanPropertyRowMapper<SystemNotify>(SystemNotify.class));
		page.setPageNo(systemNotifyForm.getPageNo());
		return page;
	}

	@Override
	public SystemNotify findOneById(Long id) {
		String sql = "SELECT * FROM system_notify WHERE id=?";
		Object[] args = new Object[] { id };
		return (SystemNotify) super.queryForObject(dsBmsQry, sql, args, SystemNotify.class);
	}

	@Override
	public Object add(SystemNotify systemNotify) {
		String sql = "INSERT INTO system_notify ( userId, title, message, createTime, STATUS ) VALUES (?, ?, ?, NOW(), 1)";
		return (SystemNotify) super.addForObject(dsBmsUpd, sql, systemNotify,
				new Object[] { systemNotify.getUserId(), systemNotify.getTitle(), systemNotify.getMessage() });
	}
	
	@Override
	public int update(SystemNotify systemNotify) {
		String sql = "UPDATE system_notify SET userId=?, title=?, message=?, createTime=NOW(), status=? WHERE id=?";
		return super.updateForObject(dsBmsUpd, sql,
				new Object[] { systemNotify.getUserId(), systemNotify.getTitle(), systemNotify.getMessage(), systemNotify.getStatus(), systemNotify.getId() });

	}

	@Override
	public int delete(Long id, String modifier) {
		String sql = "UPDATE system_notify SET status=2 WHERE id=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[] { id });
	}

	@Override
	public void add(List<SystemNotify> systemNotifyList) {		
		String sql = "INSERT INTO system_notify ( userId, title, message, createTime, STATUS ) VALUES (?, ?, ?, NOW(), 1)";
		for (SystemNotify systemNotify : systemNotifyList){
			super.addForObject(dsBmsUpd, sql, systemNotify, new Object[]{
					systemNotify.getUserId(), systemNotify.getTitle(), systemNotify.getMessage()
			});
		}
		
	}

}
