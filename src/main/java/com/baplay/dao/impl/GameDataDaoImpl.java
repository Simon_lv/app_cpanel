package com.baplay.dao.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IGameDataDao;
import com.baplay.entity.GameData;
import com.baplay.form.GameDataForm;

@Repository
@Transactional
public class GameDataDaoImpl extends BaseDao<GameData> implements IGameDataDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}

	@Override
	public List<Map<String, Object>> getGameCodeName() {
		String sql = "SELECT game_code gameCode, game_name gameName FROM game_data WHERE status = 1 ORDER BY id";
		return super.queryForListMap(dsBmsQry, sql, new Object[] {});
	}
	
	@Override
	public List<Map<String, Object>> getGameIdName1() {
		String sql = "SELECT id, game_name gameName FROM game_data WHERE status = 1 and id>1 ORDER BY id";
		return super.queryForListMap(dsBmsQry, sql, new Object[] {});
	}

	@Override
	public List<?> checkExist(GameData gameData) {
		String sql = "SELECT * FROM game_data WHERE id=? AND game_code=?";
		Object[] args = new Object[] { gameData.getId(), gameData.getGameCode() };
		return super.queryForList(dsBmsQry, sql, args, GameData.class);
	}

	@Override
	public Page<GameData> list(GameDataForm gameDataForm) {
		StringBuilder sql = new StringBuilder("SELECT * FROM game_data WHERE 1=1 ");
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM partner_record WHERE 1=1 ");
		ArrayList<Object> param = new ArrayList<Object>();

		if (null != gameDataForm.getId()) {
			sql.append(" AND id = ? ");
			countSql.append(" AND id = ? ");
			param.add(gameDataForm.getId());
		}

		sql.append(" AND status = 1 ORDER BY create_time DESC ");

		if (!gameDataForm.isExport()) {
			sql.append(" LIMIT ").append((gameDataForm.getPageNo() - 1) * gameDataForm.getPageSize()).append(",").append(gameDataForm.getPageSize());
		}
		Page<GameData> page = new Page<GameData>(gameDataForm.getPageSize());

		page = (Page<GameData>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(), param.toArray(), new BeanPropertyRowMapper<GameData>(
				GameData.class));
		return page;
	}

	@Override
	public GameData findOneById(Long id) {
		String sql = "SELECT * FROM game_data WHERE id=?";
		Object[] args = new Object[] { id };
		return (GameData) super.queryForObject(dsBmsQry, sql, args, GameData.class);
	}

	@Override
	public Object add(GameData gameData) {
		String sql = "INSERT INTO game_data (game_code, game_name, company_id, licensing_fee, lf_currency, lf_rate, lf_date, lf_end_month, advance_rate, split_mode, step_currency, step1_money, step1_scale, step2_money, step2_scale, creator, create_time, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), 1)";

		Calendar cal = Calendar.getInstance();
		cal.setTime(gameData.getLfDate());
		cal.add(Calendar.MONTH, +11);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");

		String lfEndMonth = sdf.format(cal.getTime());

		return (GameData) super.addForObject(dsBmsUpd, sql, gameData,
				new Object[] { gameData.getGameCode(), gameData.getGameName(), gameData.getCompanyId(), gameData.getLicensingFee(), gameData.getLfCurrency(),
						gameData.getLfRate(), gameData.getLfDate(), lfEndMonth, gameData.getAdvanceRate(), gameData.getSplitMode(), gameData.getStepCurrency(),
						gameData.getStep1Money(), gameData.getStep1Scale(), gameData.getStep2Money(), gameData.getStep2Scale(), gameData.getCreator() });
	}

	@Override
	public int update(GameData gameData) {
		String sql = "UPDATE game_data SET game_code=?, game_name=?, company_id=?, licensing_fee=?, lf_currency=?, lf_rate=?, lf_date=?, lf_end_month=?, advance_rate=?, split_mode=?, step_currency=?, step1_money=?, step1_scale=?, step2_money=?, step2_scale=?, update_time=NOW(), modifier=? WHERE id=?";

		Calendar cal = Calendar.getInstance();
		cal.setTime(gameData.getLfDate());
		cal.add(Calendar.MONTH, +11);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
		String lfEndMonth = sdf.format(cal.getTime());

		return super.updateForObject(dsBmsUpd, sql,
				new Object[] { gameData.getGameCode(), gameData.getGameName(), gameData.getCompanyId(), gameData.getLicensingFee(), gameData.getLfCurrency(),
						gameData.getLfRate(), gameData.getLfDate(), lfEndMonth, gameData.getAdvanceRate(), gameData.getSplitMode(), gameData.getStepCurrency(),
						gameData.getStep1Money(), gameData.getStep1Scale(), gameData.getStep2Money(), gameData.getStep2Scale(), gameData.getModifier(),
						gameData.getId() });
	}

	@Override
	public int delete(Long id, String modifier) {
		String sql = "UPDATE game_data SET modifier=?, update_time=NOW(), status=2 WHERE id=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[] { modifier, id });
	}

}
