package com.baplay.dao.impl;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IAppVersionControlDao;
import com.baplay.entity.AppVersionControl;
import com.baplay.form.AppVersionControlForm;

@Repository
@Transactional
public class AppVersionControlDaoImpl extends BaseDao<AppVersionControl> implements IAppVersionControlDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Page<AppVersionControl> list(AppVersionControlForm appVersionControlForm) {
		StringBuilder sql = new StringBuilder("SELECT avc.*, (SELECT gameName FROM t_games WHERE gameCode=avc.gameCode) gameName FROM app_version_control avc WHERE 1=1");
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM app_version_control WHERE 1=1");
		ArrayList<Object> param = new ArrayList<Object>();
		
		if(StringUtils.isNotBlank(appVersionControlForm.getGameCode())){
			sql.append(" AND gameCode=? ");
			countSql.append(" AND gameCode=? ");
			param.add(appVersionControlForm.getGameCode());
		}
		if(appVersionControlForm.getIsUpgrade() > 0){
			sql.append(" AND isUpgrade=? ");
			countSql.append(" AND isUpgrade=? ");
			param.add(appVersionControlForm.getIsUpgrade());
		}
		sql.append(" ORDER BY version DESC");		
		
		if (!appVersionControlForm.isExport()) {
			sql.append(" LIMIT ").append((appVersionControlForm.getPageNo() - 1) * appVersionControlForm.getPageSize()).append(",").append(appVersionControlForm.getPageSize());
		}
		Page<AppVersionControl> page = new Page<AppVersionControl>(appVersionControlForm.getPageSize());
		page = (Page<AppVersionControl>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(), param.toArray(), new BeanPropertyRowMapper<AppVersionControl>(
				AppVersionControl.class));
		page.setPageNo(appVersionControlForm.getPageNo());
		return page;
	}

	@Override
	public AppVersionControl findOneById(Long id) {
		String sql = "SELECT * FROM app_version_control WHERE id=?";
		return (AppVersionControl) this.queryForObject(dsBmsQry, sql, new Object[]{id}, AppVersionControl.class);
	}

	@Override
	public AppVersionControl save(AppVersionControl appVersionControl) {
		String sql = "INSERT INTO app_version_control (gameCode, version, storeUrl, isUpgrade, createTime, status) values (?,?,?,?,NOW(),1)";
		return (AppVersionControl) this.addForObject(dsBmsUpd, sql, appVersionControl, new Object[]{
				appVersionControl.getGameCode(), appVersionControl.getVersion(), appVersionControl.getStoreUrl(),
				appVersionControl.getIsUpgrade()
		});
	}

	@Override
	public int update(AppVersionControl appVersionControl) {
		String sql = "UPDATE app_version_control SET gameCode=?, version=?, storeUrl=?, isUpgrade=? WHERE id=?";
		return this.updateForObject(dsBmsUpd, sql, new Object[]{
				appVersionControl.getGameCode(), appVersionControl.getVersion(), appVersionControl.getStoreUrl(),
				appVersionControl.getIsUpgrade(), appVersionControl.getId()
		});
	}

	@Override
	public int delete(Long id) {
		String sql = "UPDATE app_version_control SET status=2 WHERE id=?";
		return this.updateForObject(dsBmsUpd, sql, new Object[]{ id });
	}

}
