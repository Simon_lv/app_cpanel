/**
 * 
 */
package com.baplay.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baplay.dao.IFunctionDao;
import com.baplay.entity.Function;

/**
 * @author Haozi
 *
 */
@Repository
@Transactional
public class FunctionDao extends BaseDao<Function> implements IFunctionDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IFunctionDao#save(com.baplay.dto.Function)
	 */
	@Override
	public void save(Function function) {
		Function old = (Function) this.queryForObject(dsBmsQry,
				"select * from t_app_function where name = ?",
				new Object[] { function.getName() }, Function.class);
		if (old != null) {
			return;
		}
		this.addForParam(
				dsBmsUpd,
				"insert into t_app_function (name,parentId,url,type,status) values (?,?,?,?,0)",
				function, new String[] { "name", "parentId", "url", "type" });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IFunctionDao#update(com.baplay.dto.Function)
	 */
	@Override
	public void update(Function function) {
		Function old = (Function) this.queryForObject(dsBmsQry,
				"select * from t_app_function where name = ?",
				new Object[] { function.getName() }, Function.class);
		if (old != null && !old.getId().equals(function.getId())) {
			return;
		}
		this.updateForParam(
				dsBmsUpd,
				"update t_app_function set name = ? ,parentId = ?,url = ?,type=? where id = ?",
				function, new String[] { "name", "parentId", "url", "type",
						"id" });

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IFunctionDao#delete(com.baplay.dto.Function)
	 */
	@Override
	public void delete(Function function) {
		this.updateForParam(dsBmsUpd,
				"update t_app_function set status=? where id = ?", function,
				new String[] { "status", "id" });
		this.updateForParam(
				dsBmsUpd,
				"update t_app_role_function set status=? where functionId = ?",
				function, new String[] { "status", "id" });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.baplay.dao.IFunctionDao#query(Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Function> query(Long id) {
		return this.queryForList(dsBmsQry,
				"select * from t_app_function where parentId = ?",
				new Object[] { id }, new BeanPropertyRowMapper<Function>(
						Function.class));
	}

	@Override
	public Function queryOne(Long id) {
		return (Function) this.queryForObject(dsBmsQry,
				"select * from t_app_function where id = ?",
				new Object[] { id }, new BeanPropertyRowMapper<Function>(
						Function.class));
	}
}
