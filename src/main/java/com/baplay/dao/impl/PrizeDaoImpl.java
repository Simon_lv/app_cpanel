package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IPrizeDao;
import com.baplay.entity.Prize;
import com.baplay.entity.PrizeSn;
import com.baplay.form.PrizeForm;

@Repository
@Transactional
public class PrizeDaoImpl extends BaseDao<Prize> implements IPrizeDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<Prize> list(PrizeForm prizeForm) {
		StringBuilder sql = new StringBuilder("SELECT p.*, (select gameName from t_games where gameCode=p.gameCode limit 1) gameName FROM prize p WHERE 1=1 ");
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM prize p WHERE 1=1 ");
		ArrayList<Object> param = new ArrayList<Object>();
		if (StringUtils.isNotBlank(prizeForm.getGameCode())) {
			sql.append(" AND p.gameCode = ? ");
			countSql.append(" AND p.gameCode = ? ");
			param.add(prizeForm.getGameCode());
		}
		if (StringUtils.isNotBlank(prizeForm.getTimeStart())) {
			sql.append(" AND p.`createTime` >= ? ");
			countSql.append(" AND p.`createTime` >= ? ");
			param.add(prizeForm.getTimeStart());
		}
		if (StringUtils.isNotBlank(prizeForm.getTimeEnd())) {
			sql.append(" AND p.`createTime` <= ? ");
			countSql.append(" AND p.`createTime` <= ? ");
			param.add(prizeForm.getTimeEnd());
		}
		if (StringUtils.isNotBlank(prizeForm.getPrizeTimeStart())) {
			sql.append(" AND p.`startTime` >= ? ");
			countSql.append(" AND p.`startTime` >= ? ");
			param.add(prizeForm.getPrizeTimeStart());
		}
		if (StringUtils.isNotBlank(prizeForm.getPrizeTimeEnd())) {
			sql.append(" AND p.`endTime` <= ? ");
			countSql.append(" AND p.`endTime` <= ? ");
			param.add(prizeForm.getPrizeTimeEnd());
		}
		sql.append(" AND p.status = 1 ");
		countSql.append(" AND p.status = 1 ");
		if (!prizeForm.isExport()) {
			sql.append(" LIMIT ").append((prizeForm.getPageNo() - 1) * prizeForm.getPageSize()).append(",").append(prizeForm.getPageSize());
		}
		Page<Prize> page = new Page<Prize>(prizeForm.getPageSize());
		page = (Page<Prize>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(), param.toArray(), new BeanPropertyRowMapper<Prize>(
				Prize.class));
		page.setPageNo(prizeForm.getPageNo());
		return page;
	}

	@Override
	public Prize findOneById(Long id) {
		String sql = "SELECT * FROM prize WHERE id=?";
		Object[] args = new Object[] { id };
		return (Prize) super.queryForObject(dsBmsQry, sql, args, Prize.class);
	}

	@Override
	public Object add(Prize prize) {
		String sql = "INSERT INTO prize ( gameCode, type, iconUrl, `desc`, content, `usage`, cost, startTime, endTime, order_index, createTime, STATUS, limitTime ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), 1, ?)";
		return (Prize) super.addForObject(dsBmsUpd, sql, prize,
				new Object[] { prize.getGameCode(), prize.getType(), prize.getIconUrl(), prize.getDesc(), prize.getContent(), prize.getUsage(),
						prize.getCost(), prize.getStartTime(), prize.getEndTime(), prize.getOrder_index(), prize.getLimitTime() });
	}

	@Override
	public int update(Prize prize) {
		String sql = "UPDATE prize SET gameCode=?, type=?, iconUrl=?, `desc`=?, content=?, `usage`=?, cost=?, startTime=?, endTime=?, order_index=?, createTime=NOW(), status=?, limitTime=? WHERE id=?";
		return super.updateForObject(dsBmsUpd, sql,
				new Object[] { prize.getGameCode(), prize.getType(), prize.getIconUrl(), prize.getDesc(), prize.getContent(), prize.getUsage(),
						prize.getCost(), prize.getStartTime(), prize.getEndTime(), prize.getOrder_index(), prize.getStatus(), prize.getLimitTime(), prize.getId() });

	}

	@Override
	public int delete(Long id, String modifier) {
		String sql = "UPDATE prize SET status=2 WHERE id=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[] { id });
	}

	@Override
	public void add(List<PrizeSn> prizeSnList) {
		String sql = "INSERT INTO prize_sn ( prizeId, sn, userId, receiveTime, sourceIp, deviceId, createTime ) VALUES (?, ?, ?, ?, ?, ?, NOW())";
		for (PrizeSn prizeSn : prizeSnList) {
			super.addForObject(dsBmsUpd, sql, prizeSn, new Object[] { prizeSn.getPrizeId(), prizeSn.getSn(), prizeSn.getUserId(), prizeSn.getReceiveTime(),
					prizeSn.getSourceIp(), prizeSn.getDeviceId() });
		}
	}
}
