package com.baplay.dao.impl;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IAnnouncementDao;
import com.baplay.entity.Announcement;
import com.baplay.form.AnnouncementForm;

@Repository
@Transactional
public class AnnouncementDaoImpl extends BaseDao<Announcement> implements IAnnouncementDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Page<Announcement> list(AnnouncementForm announcementForm) {
		StringBuilder sql = new StringBuilder("SELECT a.*, (SELECT gameName from t_games WHERE gameCode=a.gameCode) gameName FROM announcement a WHERE a.status = 1 ");
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM announcement a WHERE a.status = 1 ");
		ArrayList<Object> param = new ArrayList<Object>();
		if (announcementForm.getType() > 0) {
			sql.append(" AND a.type = ? ");
			countSql.append(" AND a.type = ? ");
			param.add(announcementForm.getType());
		}
		if(StringUtils.isNotBlank(announcementForm.getGameCode())){
			sql.append(" AND a.gameCode = ? ");
			countSql.append(" AND a.gameCode = ? ");
			param.add(announcementForm.getGameCode());
		}
		sql.append(" ORDER BY COALESCE(a.orderIndex,999), a.createTime DESC");
		countSql.append(" ORDER BY COALESCE(a.orderIndex,999), a.createTime DESC");
		
		if (!announcementForm.isExport()) {
			sql.append(" LIMIT ").append((announcementForm.getPageNo() - 1) * announcementForm.getPageSize()).append(",").append(announcementForm.getPageSize());
		}
		Page<Announcement> page = new Page<Announcement>(announcementForm.getPageSize());
		page = (Page<Announcement>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(), param.toArray(), new BeanPropertyRowMapper<Announcement>(
				Announcement.class));
		page.setPageNo(announcementForm.getPageNo());
		return page;		
	}

	@Override
	public Announcement findOneById(Long id) {
		String sql = "SELECT * FROM announcement WHERE id=?";
		return (Announcement) this.queryForObject(dsBmsQry, sql, new Object[]{id}, Announcement.class);
	}

	@Override
	public Announcement save(Announcement announcement) {
		String sql = "INSERT INTO announcement (type, isVip, gameCode, serverCode, title, content, orderIndex, startTime, endTime,"
				+ "createTime, status, position) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), 1, ?)";
		return (Announcement) this.addForObject(dsBmsUpd, sql, announcement, new Object[]{
				announcement.getType(), announcement.getIsVip(), announcement.getGameCode(), announcement.getServerCode(),
				announcement.getTitle(), announcement.getContent(), announcement.getOrderIndex(), announcement.getStartTime(),
				announcement.getEndTime(), announcement.getPosition()
		});
	}

	@Override
	public int update(Announcement announcement) {
		String sql = "UPDATE announcement SET type=?, isVip=?, gameCode=?, serverCode=?, title=?, content=?, orderIndex=?,"
				+ "startTime=?, endTime=?, position=? WHERE id=?";
		return this.updateForObject(dsBmsUpd, sql, new Object[]{
				announcement.getType(), announcement.getIsVip(), announcement.getGameCode(), announcement.getServerCode(),
				announcement.getTitle(), announcement.getContent(), announcement.getOrderIndex(), announcement.getStartTime(),
				announcement.getEndTime(), announcement.getPosition(), announcement.getId()
		});
	}

	@Override
	public int delete(Long id) {
		String sql = "UPDATE announcement SET status=? WHERE id=?";
		return this.updateForObject(dsBmsUpd, sql, new Object[]{2, id});
	}
	
	

}
