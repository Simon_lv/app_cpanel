package com.baplay.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.baplay.dao.IPermissionDao;
import com.baplay.entity.Function;
import com.google.common.base.Joiner;

@Repository("PermissionDao")
@Transactional
public class PermissionDao extends BaseDao<Function> implements IPermissionDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Function> getFunctions(List<Long> roleIds, int all) {
		if (all == 1) {
			return this.queryForList(dsBmsQry,
					"SELECT f.* FROM t_app_function f where status = 0",
					null, new BeanPropertyRowMapper<Function>(Function.class));
		} else {
			return this
					.queryForList(
							dsBmsQry,
							"SELECT f.* FROM t_app_function f LEFT JOIN t_app_role_function rf ON f.id = rf.functionId AND rf.status=0 WHERE f.status = 0 AND rf.roleId in ("
									+ Joiner.on(",").join(roleIds) + ")", null,
							new BeanPropertyRowMapper<Function>(Function.class));
		}
	}

	@Override
	public void updateFunctionStatus(Long funId, Integer status) {
		this.updateForObject(dsBmsUpd,
				"update t_app_function set status=? where id=?",
				new Object[] { status, funId });

	}
}
