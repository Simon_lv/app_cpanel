package com.baplay.dao.impl;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IBonusRecordDao;
import com.baplay.entity.BonusRecord;
import com.baplay.form.BonusRecordForm;

@Repository
@Transactional
public class BonusRecordDaoImpl extends BaseDao<BonusRecord> implements IBonusRecordDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Page<BonusRecord> list(BonusRecordForm bonusRecordForm) {
		StringBuilder sql = new StringBuilder("SELECT br.* FROM bonus_record br WHERE 1=1");
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM bonus_record br WHERE 1=1");
		ArrayList<Object> param = new ArrayList<Object>();
		
		if (StringUtils.isNotBlank(bonusRecordForm.getUserId())) {
			sql.append(" AND br.userId=? ");
			countSql.append(" AND br.userId=? ");
			param.add(bonusRecordForm.getUserId());
		}
		
		sql.append(" ORDER BY br.createTime DESC");
		
		if (!bonusRecordForm.isExport()) {
			sql.append(" LIMIT ").append((bonusRecordForm.getPageNo() - 1) * bonusRecordForm.getPageSize()).append(",").append(bonusRecordForm.getPageSize());
		}
		Page<BonusRecord> page = new Page<BonusRecord>(bonusRecordForm.getPageSize());
		page = (Page<BonusRecord>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(), param.toArray(), new BeanPropertyRowMapper<BonusRecord>(
				BonusRecord.class));
		page.setPageNo(bonusRecordForm.getPageNo());
		return page;
	}

}
