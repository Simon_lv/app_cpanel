package com.baplay.dao.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import com.baplay.dao.IGameDao;
import com.google.common.collect.Maps;

@Repository
public class GameDaoImpl extends BaseDao implements IGameDao {
	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, String> getGameMap() {
		List<Map<String, Object>> list = this.queryForListMap(dsBmsQry,
				"select gameCode, gameName from `t_games` ORDER BY `gameCode`",
				null);
		Map<String, String> gameMap = Maps.newLinkedHashMap();
		for (Map<String, Object> map : list) {
			gameMap.put(map.get("gameCode").toString(), map.get("gameName")
					.toString());
		}
		return gameMap;
	}

	@Override
	public String getGameName(String gameCode) {
		return this.queryForString(dsBmsQry,
				"select gameName from `t_games` where gameCode = ? limit 1",
				new Object[] { gameCode });
	}

	@Override
	public String getGameServerName(String gameCode, String serverCode) {
		return this
				.queryForString(
						dsBmsQry,
						"SELECT serverName FROM t_game_servers where gameCode = ? and serverCode = ? limit 1",
						new Object[] { gameCode, serverCode });
	}

	@Override
	public String getGameServerNameList(String gameCode, String serverCode) {
		
		List<String> names = super.queryForObjectList(dsBmsQry, "SELECT serverName FROM t_game_servers where gameCode = ? and serverCode IN ("+serverCode+")", new Object[] { gameCode },String.class);
		StringBuilder result = new StringBuilder();
		int i = 0;
		for(String n : names) {
			if("".equals(n)) {
				continue;
			}
			result.append(n).append("，");
			i++;
		}
		
		if(i>0) {
			result.deleteCharAt(result.length()-1);
		}
		return result.toString();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, String> getSimpleHKGameMap() {
		List<Map<String, Object>> list = this.queryForListMap(dsBmsQry,
				"select gameCode, gameName from `t_games` where flag='是' and isGame='是' and dataSource = 'hk_vice' ORDER BY `gameCode`", null);
		Map<String, String> gameMap = Maps.newLinkedHashMap();
		for (Map<String, Object> map : list) {
			String gameCode = map.get("gameCode").toString();
			if (StringUtils.isBlank(gameCode)) {
				continue;
			}
			if (gameCode.endsWith("ios")) {
				continue;
			}
			gameMap.put(gameCode, map.get("gameName").toString());
		}
		return gameMap;
	}

	@Override
	public Map<String, String> getHKGameMap() {
		List<Map<String, Object>> list = this.queryForListMap(dsBmsQry,
				"select gameCode, gameName from `t_games` where flag='是' and isGame='是' and dataSource = 'hk_vice' ORDER BY `gameCode`", null);
		Map<String, String> gameMap = Maps.newLinkedHashMap();
		for (Map<String, Object> map : list) {
			gameMap.put(map.get("gameCode").toString(), map.get("gameName").toString());
		}
		return gameMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, String> getSimpleNoHKGameMap() {
		List<Map<String, Object>> list = this.queryForListMap(dsBmsQry,
				"select gameCode, gameName from `t_games` where flag='是' and isGame='是' ORDER BY `level`", null);
		Map<String, String> gameMap = Maps.newLinkedHashMap();
		for (Map<String, Object> map : list) {
			String gameCode = map.get("gameCode").toString().toLowerCase();
			if (StringUtils.isBlank(gameCode)) {
				continue;
			}
			if (gameCode.endsWith("ios")) {
				continue;
			}
			if (gameCode.endsWith("hk")) {
				continue;
			}
			gameMap.put(gameCode, map.get("gameName").toString());
		}
		return gameMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, String> getSimpleGameMap() {
		List<Map<String, Object>> list = this.queryForListMap(dsBmsQry,
				"select gameCode, gameName from `t_games` where flag='是' and isGame='是' ORDER BY `level`", null);
		Map<String, String> gameMap = Maps.newLinkedHashMap();
		for (Map<String, Object> map : list) {
			String gameCode = map.get("gameCode").toString();
			if (StringUtils.isBlank(gameCode)) {
				continue;
			}
			if (gameCode.endsWith("ios")) {
				continue;
			}
			gameMap.put(gameCode, map.get("gameName").toString());
		}
		return gameMap;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, String> getByGameCode(String gameCode){
		List<Map<String, Object>> list = this.queryForListMap(dsBmsQry,
				"select gameCode, gameName from `t_games` WHERE gameCode=? ORDER BY `gameCode`",
				new Object[]{gameCode});
		Map<String, String> gameMap = Maps.newLinkedHashMap();
		for (Map<String, Object> map : list) {
			gameMap.put(map.get("gameCode").toString(), map.get("gameName")
					.toString());
		}
		return gameMap;
	}

}
