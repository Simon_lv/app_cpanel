package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IPushDao;
import com.baplay.entity.PushRecord;
import com.baplay.form.PushForm;

@Repository
@Transactional
public class PushDaoImpl extends BaseDao<PushRecord> implements IPushDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@Override
	public PushRecord add(PushRecord pushRecord) {

		String sql = "INSERT INTO `push_record` (`title`, `content`, `type`, `traceId`, `pushTime`, `imgUrl`, `creator`, `createTime`, `status`) VALUES (?, ?, 0, null, ?, ?, ?, NOW(), 1)";
		return (PushRecord) super.addForObject(dsBmsUpd, sql, pushRecord, new Object[]{pushRecord.getTitle(), pushRecord.getContent(), pushRecord.getPushTime(), 
				pushRecord.getImgUrl(), pushRecord.getCreator()});
	}

	@Override
	public int update(PushRecord pushRecord) {//`purchase_date`=?, `money`=?, 不可編輯20151022
		String sql = "UPDATE `push_record` SET `title`=?, `content`=?, `pushTime`=?, `imgUrl`=?, `modifier`=?, `updateTime`=NOW() WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{pushRecord.getTitle(), pushRecord.getContent(), pushRecord.getPushTime(), pushRecord.getImgUrl(), 
				pushRecord.getModifier(), pushRecord.getId()});
	}

	@Override
	public PushRecord findOneById(Long id) {
		String sql = "SELECT * FROM push_record WHERE id=?";
		Object[] args = new Object[]{id};
		return (PushRecord) super.queryForObject(dsBmsQry, sql, args, PushRecord.class);
	}

	@Override
	public Page<PushRecord> list(PushForm pushForm) {
		StringBuilder sb = new StringBuilder();
		ArrayList<Object> param = new ArrayList<Object>();
		
		
		if(StringUtils.isNotBlank(pushForm.getTitle())) {
			sb.append(" AND title LIKE ? ");
			param.add("%"+pushForm.getTitle()+"%");
		}
		
		if(StringUtils.isNotBlank(pushForm.getPushTimeStart())) {
			sb.append(" AND pushTime >= ? ");
			param.add(pushForm.getPushTimeStart());
		}
		
		if(StringUtils.isNotBlank(pushForm.getPushTimeEnd())) {
			sb.append(" AND pushTime <= ? ");
			param.add(pushForm.getPushTimeEnd());
		}
		
		if(StringUtils.isNotBlank(pushForm.getRunTimeStart())) {
			sb.append(" AND runTime >= ? ");
			param.add(pushForm.getRunTimeStart());
		}
		
		if(StringUtils.isNotBlank(pushForm.getRunTimeEnd())) {
			sb.append(" AND runTime <= ? ");
			param.add(pushForm.getRunTimeEnd());
		}
		
		if(StringUtils.isNotBlank(pushForm.getIosRunTimeStart())) {
			sb.append(" AND iosRunTime >= ? ");
			param.add(pushForm.getIosRunTimeStart());
		}
		
		if(StringUtils.isNotBlank(pushForm.getIosRunTimeEnd())) {
			sb.append(" AND iosRunTime <= ? ");
			param.add(pushForm.getIosRunTimeEnd());
		}
		
		sb.append(" AND `status` = 1 ");
		StringBuilder sql = new StringBuilder("SELECT * FROM push_record WHERE 1=1 ").append(sb.toString());
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM push_record WHERE 1=1 ").append(sb.toString());
		sql.append(" ORDER BY createTime DESC ");
		if(!pushForm.isExport()) {
			sql.append(" LIMIT ").append((pushForm.getPageNo()-1)*pushForm.getPageSize()).append(",").append(pushForm.getPageSize());
		}
		Page<PushRecord> page = new Page<PushRecord>(pushForm.getPageSize());

		page = (Page<PushRecord>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(),
				param.toArray(), 
				new BeanPropertyRowMapper<PushRecord>(PushRecord.class));
		page.setPageNo(pushForm.getPageNo());
		return page;
	}

	@Override
	public int delete(Long id, String modifier) {
		String sql = "UPDATE `push_record` SET `modifier`=?, `updateTime`=NOW(), `status`=2 WHERE (`id`=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{modifier, id});
	}

	@Override
	public List<PushRecord> scheulePush() {
		String sql = "SELECT * FROM `push_record` WHERE `status` = 1 AND (`runTime` IS NULL OR `iosRunTime` IS NULL) AND `pushTime` <= NOW()";
		return super.queryForList(dsBmsQry, sql, new Object[]{}, PushRecord.class);
	}
	
	public String getAppKey(String gameCode) {
		String sql = "select appkey from t_games where `gameCode`=? limit 1";		
		return queryForString(dsBmsQry, sql, new Object[]{gameCode});
	}

}
