package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IBannerDao;
import com.baplay.entity.Banner;
import com.baplay.form.BannerForm;

@Repository
@Transactional
public class BannerDaoImpl extends BaseDao<Banner> implements IBannerDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<Banner> list(BannerForm bannerForm) {
		StringBuilder sql = new StringBuilder("SELECT b.*, (SELECT gameName from t_games WHERE gameCode=b.gameCode) gameName FROM banner b WHERE 1=1 ");
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM banner b WHERE 1=1 ");
		ArrayList<Object> param = new ArrayList<Object>();
		if (StringUtils.isNotBlank(bannerForm.getGameCode())) {
			sql.append(" AND b.gameCode = ? ");
			countSql.append(" AND b.gameCode = ? ");
			param.add(bannerForm.getGameCode());
		}
		sql.append(" AND b.status = 1 ORDER BY COALESCE(b.orderIndex,999), b.createTime DESC");
		countSql.append(" AND b.status = 1 ORDER BY COALESCE(b.orderIndex,999), b.createTime DESC");
		
		if (!bannerForm.isExport()) {
			sql.append(" LIMIT ").append((bannerForm.getPageNo() - 1) * bannerForm.getPageSize()).append(",").append(bannerForm.getPageSize());
		}
		Page<Banner> page = new Page<Banner>(bannerForm.getPageSize());
		page = (Page<Banner>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(), param.toArray(), new BeanPropertyRowMapper<Banner>(
				Banner.class));
		page.setPageNo(bannerForm.getPageNo());
		return page;
	}

	@Override
	public Banner findOneById(Long id) {
		String sql = "SELECT b.*, (SELECT gameName from t_games WHERE gameCode=b.gameCode) gameName FROM banner b WHERE b.id=?";
		Object[] args = new Object[] { id };
		return (Banner) super.queryForObject(dsBmsQry, sql, args, Banner.class);
	}

	@Override
	public Object add(Banner banner) {
		String sql = "INSERT INTO banner ( position, title, bannerUrl, linkUrl, orderIndex, startTime, endTime, createTime,"
				+ " status, isOpen ) VALUES (?, ?, ?, ?, ?, ?, ?, NOW(), 1, ?)";
		return (Banner) super.addForObject(dsBmsUpd, sql, banner, new Object[] { 
				banner.getPosition(), banner.getTitle(), banner.getBannerUrl(), banner.getLinkUrl(), banner.getOrderIndex(),
				banner.getStartTime(), banner.getEndTime(), banner.getIsOpen() });
	}

	@Override
	public int update(Banner banner) {
		String sql = "UPDATE banner SET position=?, title=?, bannerUrl=?, linkUrl=?, orderIndex=?, startTime=?, endTime=?, isOpen=? WHERE id=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[] { 
				banner.getPosition(), banner.getTitle(), banner.getBannerUrl(), banner.getLinkUrl(), banner.getOrderIndex(),
				banner.getStartTime(), banner.getEndTime(), banner.getIsOpen(), banner.getId() });
	}

	@Override
	public int delete(Long id, String modifier) {
		String sql = "UPDATE banner SET status=2 WHERE id=?";
		return super.updateForObject(dsBmsUpd, sql, new Object[] { id });
	}

	@Override
	public List<Map<String, Object>> getGames() {
		return super
				.queryForListMap(
						dsBmsQry,
						"SELECT g.* FROM (SELECT gameCode FROM game_record GROUP BY gameCode) gr JOIN (SELECT gameCode, gameName FROM t_games) g ON g.gameCode = gr.gameCode",
						new Object[] {});
	}

}
