package com.baplay.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IGameTypeDao;
import com.baplay.entity.GameType;
import com.baplay.form.GameTypeForm;

@Repository
@Transactional
public class GameTypeDaoImpl extends BaseDao<GameType> implements IGameTypeDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Page<GameType> list(GameTypeForm gameTypeForm) {		
		StringBuilder sql = new StringBuilder("SELECT * FROM game_type WHERE 1=1 ");
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM game_type WHERE 1=1 ");
		ArrayList<Object> param = new ArrayList<Object>();
		
		if(StringUtils.isNotBlank(gameTypeForm.getName())){
			sql.append(" AND name like ? ");
			countSql.append(" AND name like ? ");
			param.add("%"+gameTypeForm.getName()+"%");
		}
		
		if (!gameTypeForm.isExport()) {
			sql.append(" LIMIT ").append((gameTypeForm.getPageNo() - 1) * gameTypeForm.getPageSize()).append(",").append(gameTypeForm.getPageSize());
		}
		Page<GameType> page = new Page<GameType>(gameTypeForm.getPageSize());
		page = (Page<GameType>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(), param.toArray(), new BeanPropertyRowMapper<GameType>(
				GameType.class));
		page.setPageNo(gameTypeForm.getPageNo());
		return page;
	}

	@Override
	public GameType findOneById(Long id) {
		String sql = "SELECT * FROM game_type WHERE id=?";
		return (GameType) super.queryForObject(dsBmsQry, sql, new Object[]{id}, GameType.class);
	}

	@Override
	public GameType save(GameType gameType) {
		String sql = "INSERT INTO game_type (name, createTime, status) values (?, NOW(), 1)";
		return (GameType) super.addForObject(dsBmsUpd, sql, gameType, new Object[]{
				gameType.getName()
		});
	}

	@Override
	public int update(GameType gameType) {
		String sql = "UPDATE game_type SET name=? WHERE (id=?)";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{
				gameType.getName(), gameType.getId()
		});
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GameType> findAll() {
		String sql = "SELECT * FROM game_type WHERE status=1";
		return super.queryForList(dsBmsQry, sql, null, GameType.class);
	}

	@Override
	public int updateStatus(Long id, int status) {
		String sql = "UPDATE game_type SET status=? WHERE id=?";
		return this.updateForObject(dsBmsUpd, sql, new Object[]{status, id});
	}

}
