package com.baplay.dao.impl;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IGameRecommDao;
import com.baplay.entity.GameRecomm;
import com.baplay.form.GameRecommForm;

@Repository
@Transactional
public class GameRecommDaoImpl extends BaseDao<GameRecomm> implements IGameRecommDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Page<GameRecomm> list(GameRecommForm gameRecommForm) {
		StringBuilder sql = new StringBuilder("SELECT gr.*, (SELECT gameName FROM t_games WHERE gameCode = gr.gameCode LIMIT 1 ) gameName FROM game_recomm gr WHERE status=1");
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM game_recomm gr WHERE status=1");
		ArrayList<Object> param = new ArrayList<Object>();
		
		if (StringUtils.isNotBlank(gameRecommForm.getGameCode())) {
			sql.append(" AND gr.gameCode = ? ");
			countSql.append(" AND gr.gameCode = ? ");
			param.add(gameRecommForm.getGameCode());
		}
		
		sql.append(" ORDER BY gr.createTime DESC");
		
		if (!gameRecommForm.isExport()) {
			sql.append(" LIMIT ").append((gameRecommForm.getPageNo() - 1) * gameRecommForm.getPageSize()).append(",").append(gameRecommForm.getPageSize());
		}
		Page<GameRecomm> page = new Page<GameRecomm>(gameRecommForm.getPageSize());
		page = (Page<GameRecomm>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(), param.toArray(), new BeanPropertyRowMapper<GameRecomm>(
				GameRecomm.class));
		page.setPageNo(gameRecommForm.getPageNo());
		return page;		
	}

	@Override
	public GameRecomm save(GameRecomm gameRecomm) {		
		String sql = "INSERT INTO game_recomm (	gameCode, bannerUrl, createTime, `STATUS` ) VALUES (?, ?, NOW(), 1)";
		return (GameRecomm) super.addForObject(dsBmsUpd, sql, gameRecomm, new Object[]{
				gameRecomm.getGameCode(), gameRecomm.getBannerUrl()
		});		
	}

	@Override
	public int update(GameRecomm gameRecomm) {
		String sql = "UPDATE game_recomm SET gameCode=?, bannerUrl =? WHERE id =?";
		return super.updateForObject(dsBmsUpd, sql, new Object[]{
				gameRecomm.getGameCode(), gameRecomm.getBannerUrl(), gameRecomm.getId()
		});
	}

	@Override
	public int delete(Long id) {
		String sql = "UPDATE game_recomm SET status=2 WHERE id=?";
		return this.updateForObject(dsBmsUpd, sql, new Object[]{id});
	}

	@Override
	public GameRecomm findById(Long id) {
		String sql = "SELECT * FROM game_recomm WHERE id=?";
		return (GameRecomm) this.queryForObject(dsBmsQry, sql, new Object[]{id}, GameRecomm.class);
	}

}
