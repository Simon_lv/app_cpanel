package com.baplay.dao.impl;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IPrizeSnDao;
import com.baplay.entity.PrizeSn;
import com.baplay.form.PrizeSnForm;

@Repository
@Transactional
public class PrizeSnDaoImpl extends BaseDao<PrizeSn> implements IPrizeSnDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Page<PrizeSn> list(PrizeSnForm prizeSnForm) {
		StringBuilder sql = new StringBuilder("SELECT sn.*,(SELECT gameName FROM t_games WHERE gameCode=p.gameCode) gameName FROM prize_sn sn LEFT JOIN prize p ON sn.prizeId=p.id WHERE 1=1");
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM prize_sn sn LEFT JOIN prize p ON sn.prizeId=p.id WHERE 1=1");
		ArrayList<Object> param = new ArrayList<Object>();
		
		if(StringUtils.isNotBlank(prizeSnForm.getGameCode())){
			sql.append(" AND p.gameCode = ? ");
			countSql.append(" AND p.gameCode = ? ");
			param.add(prizeSnForm.getGameCode());
		}
		
		if(StringUtils.isNotBlank(prizeSnForm.getUserId())){
			sql.append(" AND sn.userId = ? ");
			countSql.append(" AND sn.userId = ? ");
			param.add(prizeSnForm.getUserId());
		}
		
		sql.append(" ORDER BY sn.createTime DESC, sn.id DESC");		
		
		if (!prizeSnForm.isExport()) {
			sql.append(" LIMIT ").append((prizeSnForm.getPageNo() - 1) * prizeSnForm.getPageSize()).append(",").append(prizeSnForm.getPageSize());
		}
		Page<PrizeSn> page = new Page<PrizeSn>(prizeSnForm.getPageSize());
		page = (Page<PrizeSn>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(), param.toArray(), new BeanPropertyRowMapper<PrizeSn>(
				PrizeSn.class));
		page.setPageNo(prizeSnForm.getPageNo());
		return page;
	}

	@Override
	public PrizeSn edit(Long id) {
		String sql = "SELECT sn.*, p.gameCode, (SELECT COUNT(id) FROM prize_sn WHERE prizeId=sn.prizeId AND COALESCE(userId,'')='' ) remain FROM prize_sn sn JOIN prize p ON sn.prizeId=p.id WHERE sn.prizeId=1 GROUP BY sn.prizeId";
		return (PrizeSn) this.queryForObject(dsBmsQry, sql, new Object[]{id}, PrizeSn.class);
	}
	
}
