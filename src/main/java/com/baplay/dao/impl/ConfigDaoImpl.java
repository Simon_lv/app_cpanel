package com.baplay.dao.impl;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IConfigDao;
import com.baplay.entity.Config;
import com.baplay.form.ConfigForm;

@Repository
@Transactional
public class ConfigDaoImpl extends BaseDao<Config> implements IConfigDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Page<com.baplay.entity.Config> list(ConfigForm configForm) {
		StringBuilder sql = new StringBuilder("SELECT * FROM 8888play_app_config WHERE status=1");
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM 8888play_app_config WHERE status=1");		
		
		sql.append(" ORDER BY createTime DESC");		
		
		if (!configForm.isExport()) {
			sql.append(" LIMIT ").append((configForm.getPageNo() - 1) * configForm.getPageSize()).append(",").append(configForm.getPageSize());
		}
		Page<com.baplay.entity.Config> page = new Page<com.baplay.entity.Config>(configForm.getPageSize());
		page = (Page<com.baplay.entity.Config>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(), null, new BeanPropertyRowMapper<com.baplay.entity.Config>(
				com.baplay.entity.Config.class));
		page.setPageNo(configForm.getPageNo());
		return page;
	}

	@Override
	public com.baplay.entity.Config findOneById(Long id) {
		String sql = "SELECT * FROM 8888play_app_config WHERE id=?";
		return (com.baplay.entity.Config) this.queryForObject(dsBmsQry, sql, new Object[]{id}, com.baplay.entity.Config.class);
	}

	@Override
	public com.baplay.entity.Config save(com.baplay.entity.Config config) {
		String sql = "INSERT INTO 8888play_app_config (viedoUrl, appVersion, appiosVersion, isPay, isBonus, createTime, status) VALUES (?,?,?,?,?, NOW(), 1)";
		return (com.baplay.entity.Config) this.addForObject(dsBmsUpd, sql, config, new Object[]{
				config.getViedoUrl(), config.getAppVersion(), config.getAppiosVersion(), config.getIsPay(), config.getIsBonus()
		});
	}

	@Override
	public int update(com.baplay.entity.Config config) {
		String upSql = "update 8888play_app_config set viedoUrl=? where status=1";
		this.updateForObject(dsBmsUpd, upSql, new Object[]{config.getViedoUrl()});
		String sql = "UPDATE 8888play_app_config SET viedoUrl=?, appVersion=?, appiosVersion=?, isPay=?, isBonus=? WHERE id=?";		
		return this.updateForObject(dsBmsUpd, sql, new Object[]{
				config.getViedoUrl(), config.getAppVersion(), config.getAppiosVersion(), config.getIsPay(), config.getIsBonus(), config.getId()
		});
	}

	@Override
	public int delete(Long id) {
		String sql = "UPDATE 8888play_app_config SET status=2 WHERE id=?";
		return this.updateForObject(dsBmsUpd, sql, new Object[]{id});
	}

}
