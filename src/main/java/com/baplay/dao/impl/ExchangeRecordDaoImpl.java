package com.baplay.dao.impl;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IExchangeRecordDao;
import com.baplay.entity.ExchangeRecord;
import com.baplay.form.ExchangeRecordForm;

@Repository
@Transactional
public class ExchangeRecordDaoImpl extends BaseDao<ExchangeRecord> implements IExchangeRecordDao {

	@Resource(name = "dsBmsQry")
	private DataSource dsBmsQry;

	@Resource(name = "dsBmsUpd")
	private DataSource dsBmsUpd;

	@PostConstruct
	private void initialize() {
		setDataSource(dsBmsUpd);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Page<ExchangeRecord> list(ExchangeRecordForm exchangeRecordForm) {
		StringBuilder sql = new StringBuilder("SELECT er.*, p.*, (SELECT userName FROM t_users WHERE userid=er.userId) userName, (SELECT gameName FROM t_games WHERE gameCode=p.gameCode ) gameName FROM exchange_record er JOIN prize p on er.prizeId=p.id WHERE 1=1 ");
		StringBuilder countSql = new StringBuilder("SELECT COUNT(*) FROM exchange_record er JOIN prize p on er.prizeId=p.id WHERE 1=1 ");
		ArrayList<Object> param = new ArrayList<Object>();
		
		if (exchangeRecordForm.getType() > 0) {
			sql.append(" AND p.type=? ");
			countSql.append(" AND p.type=? ");
			param.add(exchangeRecordForm.getType());
		}
		
		if(StringUtils.isNotBlank(exchangeRecordForm.getGameCode())){
			sql.append(" AND p.gameCode=?");
			countSql.append(" AMD p.gameCode=?");
			param.add(exchangeRecordForm.getGameCode());
		}
		
		if(StringUtils.isNotBlank(exchangeRecordForm.getUserId())){
			sql.append(" AND er.userId=?");
			countSql.append(" AMD er.userId=?");
			param.add(exchangeRecordForm.getUserId());
		}

		sql.append(" ORDER BY er.createTime DESC");
		countSql.append(" ORDER BY er.createTime DESC");
		
		if (!exchangeRecordForm.isExport()) {
			sql.append(" LIMIT ").append((exchangeRecordForm.getPageNo() - 1) * exchangeRecordForm.getPageSize()).append(",").append(exchangeRecordForm.getPageSize());
		}
		Page<ExchangeRecord> page = new Page<ExchangeRecord>(exchangeRecordForm.getPageSize());
		page = (Page<ExchangeRecord>) super.queryForPage(dsBmsQry, page, sql.toString(), countSql.toString(), param.toArray(), new BeanPropertyRowMapper<ExchangeRecord>(
				ExchangeRecord.class));
		page.setPageNo(exchangeRecordForm.getPageNo());
		return page;
	}

}
