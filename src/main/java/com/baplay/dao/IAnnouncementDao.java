package com.baplay.dao;

import org.springside.modules.orm.Page;

import com.baplay.entity.Announcement;
import com.baplay.form.AnnouncementForm;

public interface IAnnouncementDao {

	public Page<Announcement> list(AnnouncementForm announcementForm);
	
	public Announcement findOneById(Long id);
	
	public Announcement save(Announcement announcement);
	
	public int update(Announcement announcement);
	
	public int delete(Long id);
	
}
