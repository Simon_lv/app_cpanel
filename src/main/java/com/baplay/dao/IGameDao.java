package com.baplay.dao;

import java.util.Map;

public interface IGameDao {

	public Map<String, String> getGameMap();
	
	public Map<String, String> getSimpleHKGameMap();
	
	public Map<String, String> getHKGameMap();
	
	public Map<String, String> getSimpleNoHKGameMap();
	
	public Map<String, String> getSimpleGameMap();
	
	public Map<String, String> getByGameCode(String gameCode);
	
	public String getGameName(String gameCode);

	public String getGameServerName(String gameCode, String serverCode);

	public String getGameServerNameList(String gameCode, String serverCode);
}
