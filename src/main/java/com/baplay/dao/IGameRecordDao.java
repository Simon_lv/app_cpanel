package com.baplay.dao;

import java.util.List;
import java.util.Map;

import org.springside.modules.orm.Page;

import com.baplay.entity.GameRecord;
import com.baplay.form.GameRecordForm;

public interface IGameRecordDao {

	public Page<GameRecord> list(GameRecordForm gameRecordForm);

	public GameRecord findOneById(Long id);

	public Object add(GameRecord gameRecord);

	public int update(GameRecord gameRecord);

	public int delete(Long id, String modifier);

	public List<Map<String, Object>> getGames();
}
