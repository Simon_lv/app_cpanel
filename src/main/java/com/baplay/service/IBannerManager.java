package com.baplay.service;

import java.util.List;
import java.util.Map;

import org.springside.modules.orm.Page;

import com.baplay.entity.Banner;
import com.baplay.form.BannerForm;

public interface IBannerManager {

	public Page<Banner> list(BannerForm bannerForm);
	
	public Banner findOneById(Long id);
	
	public Object add(Banner banner);
	
	public int update(Banner banner);
	
	public int delete(Long id, String modifier);
	
	public List<Map<String, Object>> getGames();
	
}
