package com.baplay.service;

import org.springside.modules.orm.Page;

import com.baplay.entity.Faq;
import com.baplay.form.FaqForm;

public interface IFaqManager {

	public Page<Faq> list(FaqForm faqForm);
	
	public Faq findOneById(Long id);
	
	public Faq save(Faq faq);
	
	public int update(Faq faq);
	
	public int delete(Long id);
	
}
