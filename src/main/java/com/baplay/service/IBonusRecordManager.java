package com.baplay.service;

import org.springside.modules.orm.Page;

import com.baplay.entity.BonusRecord;
import com.baplay.form.BonusRecordForm;

public interface IBonusRecordManager {

	public Page<BonusRecord> list(BonusRecordForm bonusRecordForm);
	
}
