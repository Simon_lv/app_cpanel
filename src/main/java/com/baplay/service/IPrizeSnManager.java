package com.baplay.service;

import org.springside.modules.orm.Page;

import com.baplay.entity.PrizeSn;
import com.baplay.form.PrizeSnForm;

public interface IPrizeSnManager {

	public Page<PrizeSn> list(PrizeSnForm prizeSnForm);
	
	public PrizeSn edit(Long id);	
	
}
