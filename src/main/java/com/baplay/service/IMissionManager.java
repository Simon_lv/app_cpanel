package com.baplay.service;

import org.springside.modules.orm.Page;

import com.baplay.entity.Mission;
import com.baplay.form.MissionForm;


public interface IMissionManager {
	public Mission add(Mission mission);
	public int update(Mission mission);
	public Mission findOneById(Long id);
	public Page<Mission> list(MissionForm missionForm);
	public int updateStatus(Long id, int status);
}
