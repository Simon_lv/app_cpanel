package com.baplay.service;

import org.springside.modules.orm.Page;

import com.baplay.entity.ExchangeRecord;
import com.baplay.form.ExchangeRecordForm;

public interface IExchangeRecordManager {

	public Page<ExchangeRecord> list(ExchangeRecordForm exchangeRecordForm);
	
}
