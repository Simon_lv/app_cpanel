package com.baplay.service;

import org.springside.modules.orm.Page;

import com.baplay.entity.GameType;
import com.baplay.form.GameTypeForm;

public interface IGameTypeManager {

	public Page<GameType> list(GameTypeForm gameTypeForm);
	
	public GameType findOneById(Long id);
	
	public GameType save(GameType gameType);
	
	public int update(GameType gameType);
	
	public int updateStatus(Long id, int status);
	
}
