package com.baplay.service;

import org.springside.modules.orm.Page;

import com.baplay.entity.Config;
import com.baplay.form.ConfigForm;

public interface IConfigManager {

	public Page<Config> list(ConfigForm configForm);
	
	public Config findOneById(Long id);
	
	public Config save(Config config);
	
	public int update(Config config);
	
	public int delete(Long id);
	
}
