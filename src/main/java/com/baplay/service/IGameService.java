package com.baplay.service;

import java.util.Map;

public interface IGameService {
	public Map<String, String> getGameMap();

	public String getGameName(String gameCode);

	public String getGameServerName(String gameCode, String serverCode);
	
	public String getGameServerNameList(String gameCode, String serverCode);
}
