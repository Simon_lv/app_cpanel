package com.baplay.service;

import java.util.List;

import org.springside.modules.orm.Page;

import com.baplay.entity.PushRecord;
import com.baplay.form.PushForm;

public interface IPushManager {
	public PushRecord add(PushRecord pushRecord);
	public int update(PushRecord pushRecord);
	public PushRecord findOneById(Long id);
	public Page<PushRecord> list(PushForm pushForm);
	public int delete(Long id, String modifier);
	public List<PushRecord> scheulePush();
	public String getAppKey(String gameCode);
}
