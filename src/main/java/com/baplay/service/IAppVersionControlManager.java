package com.baplay.service;

import org.springside.modules.orm.Page;

import com.baplay.entity.AppVersionControl;
import com.baplay.form.AppVersionControlForm;

public interface IAppVersionControlManager {

	public Page<AppVersionControl> list(AppVersionControlForm appVersionControlForm);
	
	public AppVersionControl findOneById(Long id);
	
	public AppVersionControl save(AppVersionControl appVersionControl);
	
	public int update(AppVersionControl appVersionControl);
	
	public int delete(Long id);
	
}
