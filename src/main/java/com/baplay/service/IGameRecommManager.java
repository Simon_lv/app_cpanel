package com.baplay.service;

import javax.servlet.http.HttpServletRequest;

import org.springside.modules.orm.Page;

import com.baplay.entity.GameRecomm;
import com.baplay.form.GameRecommForm;

public interface IGameRecommManager {

	public Page<GameRecomm> list(GameRecommForm gameRecommForm);
	
	public GameRecomm findById(Long id);

	public GameRecomm save(HttpServletRequest request, GameRecomm gameRecomm) throws Exception;
	
	public int update(HttpServletRequest request, GameRecomm gameRecomm);
	
	public int delete(Long id);
	
}
