package com.baplay.service;

import java.util.List;

import com.baplay.entity.Function;
import com.baplay.entity.Role;
import com.baplay.entity.TEfunuser;
import com.baplay.form.RoleForm;
import com.baplay.form.UserForm;

public interface IPermissionManager {
	public TEfunuser findByUsername(String username);

	public List<Function> getFunctions(long roleId, int all);

	public List<Role> getRole(long userId);

	public void addRole(RoleForm form);

	public void updateRole(RoleForm form);

	public void addUser(UserForm form);

	public void updateUser(UserForm form);

	public void updatePwd(long userId, String password);

	public void updateRoleStatus(Long roleId, Integer status);

	public void updateUserStatus(Integer status, Long userId);

	public List<Role> getRoles(int all);

	public List<TEfunuser> getUsers(String userName);
	
	public UserForm getUser(Long id);
}
