package com.baplay.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IPrizeDao;
import com.baplay.entity.Prize;
import com.baplay.entity.PrizeSn;
import com.baplay.form.PrizeForm;

@Service
public class PrizeManager {
	@Autowired
	private IPrizeDao prizeDao;

	public Page<Prize> list(PrizeForm prizeForm) {
		return prizeDao.list(prizeForm);
	}

	public Prize findOneById(Long id) {
		return prizeDao.findOneById(id);
	}

	@Transactional
	public Object add(Prize prize) {
		return prizeDao.add(prize);
	}

	@Transactional
	public int update(Prize prize) {
		return prizeDao.update(prize);
	}

	@Transactional
	public int delete(Long id, String modifier) {
		return prizeDao.delete(id, modifier);
	}

	@Transactional
	public void add(List<PrizeSn> prizeSnList) {
		prizeDao.add(prizeSnList);
	}
}
