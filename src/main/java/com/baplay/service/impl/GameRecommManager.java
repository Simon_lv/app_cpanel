package com.baplay.service.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IGameRecommDao;
import com.baplay.entity.GameRecomm;
import com.baplay.form.GameRecommForm;
import com.baplay.service.IGameRecommManager;

@Service
@Transactional
public class GameRecommManager implements IGameRecommManager {

	@Autowired
	private IGameRecommDao gameRecommDao;
	
	@Override
	public Page<GameRecomm> list(GameRecommForm gameRecommForm) {
		return gameRecommDao.list(gameRecommForm);
	}

	@Override
	public GameRecomm save(HttpServletRequest request, GameRecomm gameRecomm) throws Exception {
		String gameCode[] = request.getParameterValues("gameCode");
		String bannerUrl[] = request.getParameterValues("bannerUrl");
		
		for(int i=0;i<bannerUrl.length;i++){
			if(StringUtils.isNotBlank(bannerUrl[i]) && StringUtils.isNotBlank(gameCode[i])){
				gameRecomm.setGameCode(gameCode[i]);
				gameRecomm.setBannerUrl(bannerUrl[i]);
				
				gameRecomm = gameRecommDao.save(gameRecomm);
			}			
		}
		
		return gameRecomm;
	}

	@Override
	public int update(HttpServletRequest request, GameRecomm gameRecomm) {	
		int result = 0;
		
		if(StringUtils.isNotBlank(gameRecomm.getGameCode()) && StringUtils.isNotBlank(gameRecomm.getBannerUrl())){
			result = gameRecommDao.update(gameRecomm);
		}
		
		return result;
	}

	@Override
	public int delete(Long id) {		
		return gameRecommDao.delete(id);
	}

	@Override
	public GameRecomm findById(Long id) {
		return gameRecommDao.findById(id);
	}

}
