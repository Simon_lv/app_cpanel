package com.baplay.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IFaqDao;
import com.baplay.entity.Faq;
import com.baplay.form.FaqForm;
import com.baplay.service.IFaqManager;

@Service
@Transactional
public class FaqManager implements IFaqManager {

	@Autowired
	private IFaqDao faqDao;
	
	@Override
	public Page<Faq> list(FaqForm faqForm) {		
		return faqDao.list(faqForm);
	}

	@Override
	public Faq findOneById(Long id) {
		return faqDao.findOneById(id);
	}

	@Override
	public Faq save(Faq faq) {
		return faqDao.save(faq);
	}

	@Override
	public int update(Faq faq) {
		return faqDao.update(faq);
	}

	@Override
	public int delete(Long id) {
		return faqDao.delete(id);
	}

}
