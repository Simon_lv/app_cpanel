package com.baplay.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IAppVersionControlDao;
import com.baplay.entity.AppVersionControl;
import com.baplay.form.AppVersionControlForm;
import com.baplay.service.IAppVersionControlManager;

@Service
@Transactional
public class AppVersionControlManager implements IAppVersionControlManager {

	@Autowired
	private IAppVersionControlDao appVersionControlDao;
	
	@Override
	public Page<AppVersionControl> list(AppVersionControlForm appVersionControlForm) {		
		return appVersionControlDao.list(appVersionControlForm);
	}

	@Override
	public AppVersionControl findOneById(Long id) {		
		return appVersionControlDao.findOneById(id);
	}

	@Override
	public AppVersionControl save(AppVersionControl appVersionControl) {
		return appVersionControlDao.save(appVersionControl);
	}

	@Override
	public int update(AppVersionControl appVersionControl) {
		return appVersionControlDao.update(appVersionControl);
	}

	@Override
	public int delete(Long id) {		
		return appVersionControlDao.delete(id);
	}

}
