package com.baplay.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IPushDao;
import com.baplay.entity.PushRecord;
import com.baplay.form.PushForm;
import com.baplay.service.IPushManager;

@Service
@Transactional
public class PushManager implements IPushManager {
	@Autowired
	private IPushDao pushDao;
	
	@Override
	public PushRecord add(PushRecord device) {
		return pushDao.add(device);
	}

	@Override
	public int update(PushRecord device) {
		return pushDao.update(device);
	}

	@Override
	public PushRecord findOneById(Long id) {
		return pushDao.findOneById(id);
	}

	@Override
	public Page<PushRecord> list(PushForm pushForm) {
		return pushDao.list(pushForm);
	}

	@Override
	public int delete(Long id, String modifier) {
		return pushDao.delete(id, modifier);
	}

	@Override
	public List<PushRecord> scheulePush() {
		return pushDao.scheulePush();
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public String getAppKey(String gameCode) {
		return pushDao.getAppKey(gameCode);
	}
}
