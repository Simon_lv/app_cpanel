package com.baplay.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IGameDataDao;
import com.baplay.entity.GameData;
import com.baplay.form.GameDataForm;
import com.baplay.service.IGameDataManager;

@Service
@Transactional
public class GameDataManager implements IGameDataManager {
	@Autowired
	private IGameDataDao gameDataDao;

	@Override
	public List<Map<String, Object>> getGameCodeName() {
		return gameDataDao.getGameCodeName();
	}

	@Override
	public List<Map<String, Object>> getGameIdName1() {
		return gameDataDao.getGameIdName1();
	}

	@Override
	public boolean checkExist(GameData gameData) {
		boolean returnValue = false;
		if (gameDataDao.checkExist(gameData).size() == 0) {
			returnValue = true;
		}
		return returnValue;
	}

	@Override
	public Page<GameData> list(GameDataForm gameDataForm) {
		return gameDataDao.list(gameDataForm);
	}

	@Override
	public GameData findOneById(Long id) {
		return gameDataDao.findOneById(id);
	}

	@Override
	public Object add(GameData gameData) {
		return gameDataDao.add(gameData);
	}

	@Override
	public int update(GameData gameData) {
		return gameDataDao.update(gameData);
	}

	@Override
	public int delete(Long id, String modifier) {
		return gameDataDao.delete(id, modifier);
	}

}
