package com.baplay.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IBannerDao;
import com.baplay.service.IBannerManager;
import com.baplay.entity.Banner;
import com.baplay.form.BannerForm;

@Service
@Transactional
public class BannerManager implements IBannerManager {
	@Autowired
	private IBannerDao bannerDao;

	public Page<Banner> list(BannerForm bannerForm) {
		return bannerDao.list(bannerForm);
	}

	public Banner findOneById(Long id) {
		return bannerDao.findOneById(id);
	}

	public Object add(Banner banner) {
		return bannerDao.add(banner);
	}

	public int update(Banner banner) {
		return bannerDao.update(banner);
	}

	public int delete(Long id, String modifier) {
		return bannerDao.delete(id, modifier);
	}

	public List<Map<String, Object>> getGames() {
		return bannerDao.getGames();
	}
}
