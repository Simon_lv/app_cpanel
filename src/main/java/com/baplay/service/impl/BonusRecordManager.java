package com.baplay.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IBonusRecordDao;
import com.baplay.entity.BonusRecord;
import com.baplay.form.BonusRecordForm;
import com.baplay.service.IBonusRecordManager;

@Service
@Transactional
public class BonusRecordManager implements IBonusRecordManager {

	@Autowired
	private IBonusRecordDao bonusRecordDao;
	
	@Override
	public Page<BonusRecord> list(BonusRecordForm bonusRecordForm) {		
		return bonusRecordDao.list(bonusRecordForm);
	}

}
