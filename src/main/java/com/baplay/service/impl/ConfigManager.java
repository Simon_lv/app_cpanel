package com.baplay.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IConfigDao;
import com.baplay.entity.Config;
import com.baplay.form.ConfigForm;
import com.baplay.service.IConfigManager;

@Service
@Transactional
public class ConfigManager implements IConfigManager {

	@Autowired
	private IConfigDao videoConfigDao;
	
	@Override
	public Page<Config> list(ConfigForm videoConfigForm) {		
		return videoConfigDao.list(videoConfigForm);
	}

	@Override
	public Config findOneById(Long id) {
		return videoConfigDao.findOneById(id);
	}

	@Override
	public Config save(Config videoConfig) {		
		return videoConfigDao.save(videoConfig);
	}

	@Override
	public int update(Config videoConfig) {
		return videoConfigDao.update(videoConfig);
	}

	@Override
	public int delete(Long id) {		
		return videoConfigDao.delete(id);
	}

}
