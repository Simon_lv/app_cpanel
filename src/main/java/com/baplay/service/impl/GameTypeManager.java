package com.baplay.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IGameTypeDao;
import com.baplay.entity.GameType;
import com.baplay.form.GameTypeForm;
import com.baplay.service.IGameTypeManager;

@Service
@Transactional
public class GameTypeManager implements IGameTypeManager {

	@Autowired
	private IGameTypeDao gameTypeDao;
	
	@Override
	public Page<GameType> list(GameTypeForm gameTypeForm) {		
		return gameTypeDao.list(gameTypeForm);
	}

	@Override
	public GameType findOneById(Long id) {
		return gameTypeDao.findOneById(id);
	}

	@Override
	public GameType save(GameType gameType) {		
		return gameTypeDao.save(gameType);
	}

	@Override
	public int update(GameType gameType) {
		return gameTypeDao.update(gameType);
	}

	@Override
	public int updateStatus(Long id, int status) {		
		return gameTypeDao.updateStatus(id, status);
	}

}
