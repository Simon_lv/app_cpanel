package com.baplay.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.ISystemNotifyDao;
import com.baplay.entity.SystemNotify;
import com.baplay.form.SystemNotifyForm;

@Service
public class SystemNotifyManager {
	@Autowired
	private ISystemNotifyDao systemNotifyDao;

	public Page<SystemNotify> list(SystemNotifyForm systemNotifyForm) {
		return systemNotifyDao.list(systemNotifyForm);
	}

	public SystemNotify findOneById(Long id) {
		return systemNotifyDao.findOneById(id);
	}

	@Transactional
	public Object add(SystemNotify systemNotify) {
		return systemNotifyDao.add(systemNotify);
	}
	
	@Transactional
	public void add(List<SystemNotify> systemNotifyList){
		systemNotifyDao.add(systemNotifyList);
	}

	@Transactional
	public int update(SystemNotify systemNotify) {
		return systemNotifyDao.update(systemNotify);
	}

	@Transactional
	public int delete(Long id, String modifier) {
		return systemNotifyDao.delete(id, modifier);
	}
}
