package com.baplay.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IPrizeSnDao;
import com.baplay.entity.PrizeSn;
import com.baplay.form.PrizeSnForm;
import com.baplay.service.IPrizeSnManager;

@Service
@Transactional
public class PrizeSnManager implements IPrizeSnManager {

	@Autowired
	private IPrizeSnDao prizeSnDao;
	
	@Override
	public Page<PrizeSn> list(PrizeSnForm prizeSnForm) {		
		return prizeSnDao.list(prizeSnForm);
	}

	@Override
	public PrizeSn edit(Long id) {
		return prizeSnDao.edit(id);
	}	

}
