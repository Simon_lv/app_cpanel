package com.baplay.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IAnnouncementDao;
import com.baplay.entity.Announcement;
import com.baplay.form.AnnouncementForm;
import com.baplay.service.IAnnouncementManager;

@Service
@Transactional
public class AnnouncementManager implements IAnnouncementManager {

	@Autowired
	private IAnnouncementDao announcementDao;
	
	@Override
	public Page<Announcement> list(AnnouncementForm announcementForm) {
		return announcementDao.list(announcementForm);
	}

	@Override
	public Announcement findOneById(Long id) {		
		return announcementDao.findOneById(id);
	}

	@Override
	public Announcement save(Announcement announcement) {
		return announcementDao.save(announcement);
	}

	@Override
	public int update(Announcement announcement) {
		return announcementDao.update(announcement);
	}

	@Override
	public int delete(Long id) {
		return announcementDao.delete(id);
	}

}
