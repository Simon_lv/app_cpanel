package com.baplay.service.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baplay.dao.IPermissionDao;
import com.baplay.dao.IRoleDao;
import com.baplay.dao.IUserDao;
import com.baplay.entity.Function;
import com.baplay.entity.Role;
import com.baplay.entity.RoleFunction;
import com.baplay.entity.TEfunuser;
import com.baplay.form.RoleForm;
import com.baplay.form.UserForm;
import com.baplay.service.IPermissionManager;
import com.baplay.shiro.UserRealm;
import com.baplay.util.MD5Util;
import com.google.common.collect.Lists;

@Service("PermissionManager")
@Transactional
public class PermissionManager implements IPermissionManager {
	@Autowired
	private IPermissionDao permissionDao;
	@Autowired
	private IUserDao userDao;
	@Autowired
	private IRoleDao roleDao;
	@Autowired
	private UserRealm realm;

	@Override
	@Transactional(readOnly = true)
	public List<Function> getFunctions(long roleId, int all) {
		List<Long> roleIds = Lists.newArrayList();
		roleIds.add(roleId);
		return permissionDao.getFunctions(roleIds, all);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Role> getRole(long userId) {
		return roleDao.getRole(userId);
	}

	@Override
	public void addRole(RoleForm form) {
		long roleId = roleDao.addRole(form.getName());
		List<Long> funIds = Lists.newArrayList();
		for (RoleFunction rf : form.getRoleFunctions()) {
			funIds.add(rf.getFunctionId());
		}
		roleDao.addRoleFunction(roleId, funIds);
	}

	@Override
	public void updateRole(RoleForm form) {
		roleDao.updateRole(form.getRoleId(), form.getName());
		List<Long> funIds = Lists.newArrayList();
		for (RoleFunction rf : form.getRoleFunctions()) {
			if (rf.getFunctionId() != null) {
				funIds.add(rf.getFunctionId());
			}
		}
		roleDao.addRoleFunction(form.getRoleId(), funIds);
		realm.clearCache(SecurityUtils.getSubject().getPrincipals());
	}
	
	public UserForm getUser(Long id){
		return userDao.findById(id);		
	}

	@Override
	public void addUser(UserForm form) {
		form.setPassword(MD5Util.MD5(form.getPassword()));
		long userId = userDao.addUser(form);
		List<Long> roles = Lists.newArrayList();
		roles.add(form.getRoleId1());
		roles.add(form.getRoleId2());
		userDao.addUserRole(userId, roles);
		realm.clearCache(SecurityUtils.getSubject().getPrincipals());
	}

	@Override
	public void updateUser(UserForm form) {
		if (StringUtils.isNotBlank(form.getPassword())) {
			form.setPassword(MD5Util.MD5(form.getPassword()));
		} else {
			form.setPassword(null);
		}
		userDao.updateUser(form);
		List<Long> roles = Lists.newArrayList();
		roles.add(form.getRoleId1());
		roles.add(form.getRoleId2());
		userDao.addUserRole(form.getUserId(), roles);
		realm.clearCache(SecurityUtils.getSubject().getPrincipals());
	}

	@Override
	public void updatePwd(long userId, String password) {
		userDao.updatePwd(userId, password);
	}

	@Override
	public void updateRoleStatus(Long roleId, Integer status) {
		roleDao.updateRoleStatus(roleId, status);
		realm.clearCache(SecurityUtils.getSubject().getPrincipals());
	}

	@Override
	public void updateUserStatus(Integer status, Long userId) {
		userDao.updateUserStatus(userId, status);
		realm.clearCache(SecurityUtils.getSubject().getPrincipals());
	}

	@Override
	public TEfunuser findByUsername(String username) {
		return userDao.findByUsername(username);
	}

	@Override
	public List<Role> getRoles(int all) {
		return roleDao.getAllRole(all);
	}

	@Override
	public List<TEfunuser> getUsers(String userName) {
		return userDao.getAllUser(userName);
	}

}
