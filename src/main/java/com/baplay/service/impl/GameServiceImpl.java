package com.baplay.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baplay.dao.IGameDao;
import com.baplay.service.IGameService;
import com.google.common.base.Strings;

@Service
public class GameServiceImpl implements IGameService {

	@Autowired
	private IGameDao dao;

	@Override
	public Map<String, String> getGameMap() {
		return dao.getGameMap();
	}

	@Override
	public String getGameName(String gameCode) {
		if (Strings.isNullOrEmpty(gameCode)) {
			return "";
		} else {
			return dao.getGameName(gameCode);
		}
	}

	@Override
	public String getGameServerName(String gameCode, String serverCode) {
		if (Strings.isNullOrEmpty(gameCode)
				|| Strings.isNullOrEmpty(serverCode)) {
			return "";
		} else {
			return dao.getGameServerName(gameCode, serverCode);
		}
	}

	@Override
	public String getGameServerNameList(String gameCode, String serverCode) {
		return dao.getGameServerNameList(gameCode, serverCode);
	}

}
