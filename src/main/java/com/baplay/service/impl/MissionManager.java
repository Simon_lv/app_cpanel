package com.baplay.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IMissionDao;
import com.baplay.entity.Mission;
import com.baplay.form.MissionForm;
import com.baplay.service.IMissionManager;

@Service
@Transactional
public class MissionManager implements IMissionManager {
	@Autowired
	private IMissionDao missionDao;
	@Override
	public Mission add(Mission mission) {
		return missionDao.add(mission);
	}

	@Override
	public int update(Mission mission) {
		return missionDao.update(mission);
	}

	@Override
	public Mission findOneById(Long id) {
		return missionDao.findOneById(id);
	}

	@Override
	public Page<Mission> list(MissionForm missionForm) {
		return missionDao.list(missionForm);
	}

	@Override
	public int updateStatus(Long id, int status) {
		return missionDao.updateStatus(id, status);
	}

}
