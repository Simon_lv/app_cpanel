package com.baplay.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IExchangeRecordDao;
import com.baplay.entity.ExchangeRecord;
import com.baplay.form.ExchangeRecordForm;
import com.baplay.service.IExchangeRecordManager;

@Service
@Transactional
public class ExchangeRecordManager implements IExchangeRecordManager {

	@Autowired
	private IExchangeRecordDao exchangeRecordDao;
	
	@Override
	public Page<ExchangeRecord> list(ExchangeRecordForm exchangeRecordForm) {
		return exchangeRecordDao.list(exchangeRecordForm);
	}

}
