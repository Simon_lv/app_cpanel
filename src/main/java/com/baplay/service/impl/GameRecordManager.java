package com.baplay.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import com.baplay.dao.IGameRecordDao;
import com.baplay.entity.GameRecord;
import com.baplay.form.GameRecordForm;

@Service
@Transactional
public class GameRecordManager {
	@Autowired
	private IGameRecordDao gameRecordDao;

	public Page<GameRecord> list(GameRecordForm gameRecordForm) {
		return gameRecordDao.list(gameRecordForm);
	}

	public GameRecord findOneById(Long id) {
		return gameRecordDao.findOneById(id);
	}

	public Object add(GameRecord partnerRecord) throws Exception {
		return gameRecordDao.add(partnerRecord);
	}

	public int update(GameRecord partnerRecord) throws Exception {
		return gameRecordDao.update(partnerRecord);
	}

	public int delete(Long id, String modifier) {
		return gameRecordDao.delete(id, modifier);
	}

	public List<Map<String, Object>> getGames() {
		return gameRecordDao.getGames();
	}
}
