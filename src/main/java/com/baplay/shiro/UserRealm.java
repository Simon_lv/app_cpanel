package com.baplay.shiro;

import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.baplay.entity.Function;
import com.baplay.entity.Role;
import com.baplay.entity.TEfunuser;
import com.baplay.service.IPermissionManager;

/**
 * 
 * @author Haozi
 *
 */
public class UserRealm extends AuthorizingRealm {

	private static final Logger logger = LoggerFactory
			.getLogger(UserRealm.class);

	@Autowired
	private IPermissionManager permissionManager;

	@SuppressWarnings("unchecked")
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(
			PrincipalCollection principals) {
		if (principals == null) {
			throw new AuthorizationException(
					"PrincipalCollection method argument cannot be null.");
		}
		String username = (String) getAvailablePrincipal(principals);
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		TEfunuser user = (TEfunuser) SecurityUtils.getSubject().getSession()
				.getAttribute("loginUser");
		if (!username.equals(user.getUsername())) {
			return null;
		}
		try {
			List<Role> roles = (List<Role>) SecurityUtils.getSubject()
					.getSession().getAttribute("role");
			for (Role r : roles) {
				info.addRole(r.getName());
			}
			List<Function> fs = (List<Function>) SecurityUtils.getSubject()
					.getSession().getAttribute("functions");
			for (Function f : fs) {
				info.addStringPermission(f.getName());
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return info;
	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(
			AuthenticationToken token) throws AuthenticationException {
		UsernamePasswordToken t = (UsernamePasswordToken) token;
		String username = t.getUsername();
		TEfunuser user = permissionManager.findByUsername(username);
		SecurityUtils.getSubject().getSession().setAttribute("loginUser", user);
		if (user == null) {
			throw new UnknownAccountException();// 没找到帐号
		}
		if (1L == (user.getDelesign())) {
			throw new LockedAccountException(); // 帐号锁定
		}
		// 交给AuthenticatingRealm使用CredentialsMatcher进行密码匹配，如果觉得人家的不好可以自定义实现
		SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
				user.getUsername(), user.getPassword(), getName());
		return authenticationInfo;
	}

	@Override
	public void clearCachedAuthorizationInfo(PrincipalCollection principals) {
		super.clearCachedAuthorizationInfo(principals);
	}

	@Override
	public void clearCachedAuthenticationInfo(PrincipalCollection principals) {
		super.clearCachedAuthenticationInfo(principals);
	}

	@Override
	public void clearCache(PrincipalCollection principals) {
		super.clearCache(principals);
	}

	public void clearAllCachedAuthorizationInfo() {
		getAuthorizationCache().clear();
	}

	public void clearAllCachedAuthenticationInfo() {
		getAuthenticationCache().clear();
	}

	public void clearAllCache() {
		clearAllCachedAuthenticationInfo();
		clearAllCachedAuthorizationInfo();
	}
}
