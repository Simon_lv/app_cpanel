package com.efun.entity;

import java.io.Serializable;
import java.util.Date;

public class UsersRelation  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5885237272534001029L;
	private Long id;
	private Long userid;
	private Long gpid;
	private String gameCode;
	private String thirdPlate;
	private String plateForm;
	private String registerIp;
	private String address;
	private String advertiser;
	private String mac;
	private String imei;
	private String androidid;
	private String systemVersion;
	private String deviceType;
	private Date registerTime;
	private Integer flag;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserid() {
		return userid;
	}
	public void setUserid(Long userid) {
		this.userid = userid;
	}
	public Long getGpid() {
		return gpid;
	}
	public void setGpid(Long gpid) {
		this.gpid = gpid;
	}
	public String getGameCode() {
		return gameCode;
	}
	public void setGameCode(String gameCode) {
		this.gameCode = gameCode;
	}
	public String getThirdPlate() {
		return thirdPlate;
	}
	public void setThirdPlate(String thirdPlate) {
		this.thirdPlate = thirdPlate;
	}
	public String getPlateForm() {
		return plateForm;
	}
	public void setPlateForm(String plateForm) {
		this.plateForm = plateForm;
	}
	public String getRegisterIp() {
		return registerIp;
	}
	public void setRegisterIp(String registerIp) {
		this.registerIp = registerIp;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAdvertiser() {
		return advertiser;
	}
	public void setAdvertiser(String advertiser) {
		this.advertiser = advertiser;
	}
	public String getMac() {
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	
	public String getAndroidid() {
		return androidid;
	}
	public void setAndroidid(String androidid) {
		this.androidid = androidid;
	}
	public String getSystemVersion() {
		return systemVersion;
	}
	public void setSystemVersion(String systemVersion) {
		this.systemVersion = systemVersion;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public Date getRegisterTime() {
		return registerTime;
	}
	public void setRegisterTime(Date registerTime) {
		this.registerTime = registerTime;
	}
	public Integer getFlag() {
		return flag;
	}
	public void setFlag(Integer flag) {
		this.flag = flag;
	}
}