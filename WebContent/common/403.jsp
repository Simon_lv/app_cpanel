<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page session='false' %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>403 - 缺少權限</title>
</head>

<body>
<div>
	<div><h1>你没有访问该頁面的權限.</h1></div>
	<div><a href="#" onclick="javascript:history.back();">返回上一頁</a> <a href="<c:url value="/logout"/>">退出登入</a></div>
</div>
</body>
</html>