<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<aside class="left-side sidebar-offcanvas">
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="/images/logo.png" class="img-circle" alt="User Image" />
			</div>
			<div class="pull-left info">
				<p>Hello, ${loginUser.username }</p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<ul class="sidebar-menu">
			<c:forEach items="${sessionScope.functions}" var="fp">
				<c:if test="${fp.parentId==0}">
					<shiro:hasPermission name="${fp.name}">
						<li class="treeview active"><a href="#"> <i
								class="fa fa-edit"></i> <span>${fp.name}</span> <i
								class="fa fa-angle-left pull-right"></i>
						</a>
							<ul class="treeview-menu">
								<c:forEach items="${sessionScope.functions}" var="f">
									<c:if test="${fp.id==f.parentId}">
										<shiro:hasPermission name="${f.name}">
											<c:if test="${f.type == 0 }">
												<li id="blogListLi"><a href="${f.url}"><i
														class="fa fa-angle-double-right"></i>${f.name}</a></li>
											</c:if>
										</shiro:hasPermission>
									</c:if>
								</c:forEach>
							</ul></li>
					</shiro:hasPermission>
				</c:if>
			</c:forEach>
		</ul>
	</section>
</aside>