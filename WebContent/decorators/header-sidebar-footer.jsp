<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>8888play總後臺</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
         <link href="${ctx }/resources/boostrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${ctx }/resources/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="${ctx }/resources/adminlte/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
        <link href="${ctx }/resources/adminlte/css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="/js/select2/css/select2.min.css" type="text/css"></link>
		<script type="text/javascript" src="/js/jquery-1.8.2.js" ></script>
		<script type="text/javascript" src="${ctx }/resources/boostrap/js/bootstrap.min.js" ></script>
		<script type="text/javascript" src="${ctx }/resources/My97DatePicker/WdatePicker.js"></script>
		<script type="text/javascript" src="${ctx }/resources/adminlte/js/plugins/datatables/jquery.dataTables.js" ></script>
		<script type="text/javascript" src="${ctx }/resources/adminlte/js/plugins/datatables/dataTables.bootstrap.js" ></script>
		<script type="text/javascript" src="/js/select2/js/select2.full.js"></script>
		<script type="text/javascript" src="/js/moment.min.js"></script>
		<script type="text/javascript" src="/js/layer/layer.js"></script>
		<!-- 被渲染页的head内容 -->
		<sitemesh:write property='head'/>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body class="skin-blue">
        <jsp:include page="/include/include_header.jsp" />
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <jsp:include page="/include/include_leftside.jsp" />

            <!-- Right side column. Contains the navbar and content of the page -->
             <sitemesh:write property='body'/>
            <!-- /.right-side -->
        </div><!-- ./wrapper -->
        
		<!-- bottom -->
		<jsp:include page="/include/include_bottom.jsp" />
		<script type="text/javascript">
			$(document).ready(function(){
				
				//Enable sidebar toggle
			    $("[data-toggle='offcanvas']").click(function(e) {
			        e.preventDefault();

			        //If window is small enough, enable sidebar push menu
			        if ($(window).width() <= 992) {
			            $('.row-offcanvas').toggleClass('active');
			            $('.left-side').removeClass("collapse-left");
			            $(".right-side").removeClass("strech");
			            $('.row-offcanvas').toggleClass("relative");
			        } else {
			            //Else, enable content streching
			            $('.left-side').toggleClass("collapse-left");
			            $(".right-side").toggleClass("strech");
			        }
			    });
				
				$(".treeview").each(function(){
					var btn = $(this).children("a").first();
		            var menu = $(this).children(".treeview-menu").first();
		            var isActive = $(this).hasClass('active');

		            //initialize already active menus
		            if (isActive) {
		                menu.show();
		                btn.children(".fa-angle-left").first().removeClass("fa-angle-left").addClass("fa-angle-down");
		            }
		            //Slide open or close the menu on link click
		            btn.click(function(e) {
		                e.preventDefault();
		                if (isActive) {
		                    //Slide up to close menu
		                    menu.slideUp();
		                    isActive = false;
		                    btn.children(".fa-angle-down").first().removeClass("fa-angle-down").addClass("fa-angle-left");
		                    btn.parent("li").removeClass("active");
		                } else {
		                    //Slide down to open menu
		                    menu.slideDown();
		                    isActive = true;
		                    btn.children(".fa-angle-left").first().removeClass("fa-angle-left").addClass("fa-angle-down");
		                    btn.parent("li").addClass("active");
		                }
		            });

		            /* Add margins to submenu elements to give it a tree look */
		            menu.find("li > a").each(function() {
		                var pad = parseInt($(this).css("margin-left")) + 10;

		                $(this).css({"margin-left": pad + "px"});
		            });
					
				});
			});
		</script>
    </body>
</html>