<%@tag pageEncoding="UTF-8"%>
<%@tag import="com.baplay.dao.IGameTypeDao"%>
<%@tag import="com.baplay.infra.utils.SpringContextUtil"%>
<%@ attribute name="hasChoose"%>
<%@ attribute name="selected"%>
<%@ attribute name="attrs"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%
	//jsp 直接访问bean计算gamecode对应的游戏名
	IGameTypeDao bean = SpringContextUtil.getApplicationContext().getBean(IGameTypeDao.class);
	jspContext.setAttribute("gameTypes", bean.findAll());
	
%>
<select name="gameTypeId" id="gameTypeId" class="${clazz}" ${attrs}>
	<c:if test="${true == hasChoose}">
		<option value="">請選擇</option>
	</c:if>
		
	<c:forEach items="${gameTypes}" var="gts">
		<option value="${gts.id}"
			<c:if test="${gts.id==selected}">selected="selected"</c:if>>${gts.name}</option>
	</c:forEach>
</select>