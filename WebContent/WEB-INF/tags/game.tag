<%@tag pageEncoding="UTF-8"%>
<%@tag import="com.baplay.dao.IGameDao"%>
<%@tag import="com.baplay.dao.IPermissionDao"%>
<%@tag import="com.baplay.infra.utils.SpringContextUtil"%>
<%@tag import="org.apache.commons.lang3.StringUtils" %>
<%@ attribute name="isSimple"%>
<%@ attribute name="hasAll"%>
<%@ attribute name="hasChoose"%>
<%@ attribute name="selected"%>
<%@ attribute name="multiple"%>
<%@ attribute name="hasHK"%>
<%@ attribute name="clazz"%>
<%@ attribute name="attrs"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%
	//jsp 直接访问bean计算gamecode对应的游戏名
	IGameDao bean = SpringContextUtil.getApplicationContext().getBean(IGameDao.class);
	String key = (String) jspContext.getAttribute("isSimple");	
	String selected = jspContext.getAttribute("selected")==null ? "" : (String)jspContext.getAttribute("selected");
	
	String hasHK = (String) jspContext.getAttribute("hasHK");
	String area = request.getParameter("area");
	if ("HK".equalsIgnoreCase(area)) {
		jspContext.setAttribute("area", area);
		if ("true".equalsIgnoreCase(key)) {
			jspContext.setAttribute("games", bean.getSimpleHKGameMap());
		} else {
			jspContext.setAttribute("games", bean.getHKGameMap());
		}
	} else if ("true".equalsIgnoreCase(key)) {
		if ("false".equalsIgnoreCase(hasHK)) {
			jspContext.setAttribute("games", bean.getSimpleNoHKGameMap());
		} else {
			jspContext.setAttribute("games", bean.getSimpleGameMap());
		}
	} else {
		if(selected.equals("")){
			jspContext.setAttribute("games", bean.getGameMap());
		}else{
			jspContext.setAttribute("games", bean.getByGameCode(selected));
		}		
	}
%>
<select style="width: 200px;" name="gameCode" id="games" class="${clazz}" <c:if test="${true == multiple}">multiple="multiple"</c:if> ${attrs}>

	
	<c:if test="${true == hasAll}">
		<option value="">全部</option>
	</c:if>
	
	<c:if test="${true == hasChoose}">
		<option value="">請選擇</option>
	</c:if>
	
		
	<c:forEach items="${games}" var="game">
		<option value="${game.key}"
			<c:if test="${game.key ==selected}">selected="selected"</c:if>>${game.value}</option>
	</c:forEach>
</select>
<c:if test="${'HK' == param.area}">
	<input type="hidden" name="area" value="${area}" />
</c:if>