<%@tag pageEncoding="UTF-8"%>
<%@ attribute name="page" type="org.springside.modules.orm.Page" required="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="filter">
	第${page.pageNo}頁, 共${page.totalPages}頁,共${page.totalCount}筆 <a
		href="javascript:void(0);" onclick="toPage(1);">第一頁</a>
	<c:if test="${page.hasPre}">
		<a href="javascript:void(0);" onclick="toPage(${page.prePage});">上一頁</a>
	</c:if>
	<c:if test="${page.hasNext}">
		<a href="javascript:void(0);" onclick="toPage(${page.nextPage});">下一頁</a>
		<a href="javascript:void(0);" onclick="toPage(${page.totalPages});">最後一頁</a>
	</c:if>
	前往第<input size="2" id="pageNumber"></input>頁 <input class="liteoption"
		type="button" onClick="toPage($('#pageNumber').val());" value="Go" />
</div>

