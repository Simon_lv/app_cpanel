<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<style type="text/css">
	.container {
      width: 100%;
      overflow: scroll; /* showing scrollbars */
	}
</style>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<div class="container">
	<table class="table table-bordered table-full-width" id="list">
		<thead>
			<tr>
				<th>ID</th>
				<th>遊戲名稱</th>
				<th>banner</th>
				<th>建立日期</th>						
				<th>操作</th>
			</tr>
		</thead>
		<tbody>			
			<c:forEach items="${page.result}" var="o" varStatus="loop">
				<tr>
					<td>${o.id}</td>			
					<td>${o.gameName}</td>
					<td><img src="${o.bannerUrl }" height="100" width="100"/></td>				
					<td><fmt:formatDate value="${o.createTime }"/></td>
					<td>						
						<a href="javascript:;" onclick="edit(${o.id})">編輯</a>							
						<c:if test="${o.status == 1 }">
							<a href="javascript:;" onclick="deleteGR(${o.id})">刪除</a>
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</tbody>

	</table>
	</div>
</div>
<my:pagination page="${page}" />