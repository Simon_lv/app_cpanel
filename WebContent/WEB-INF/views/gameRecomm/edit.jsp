<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/gameRecomm/save" method="post" id="editForm">
		<div class="table-responsive">		
			<c:if test="${empty gameRecomm }">
			<table class="table">
				<c:forEach begin="1" end="4">				
					<tr>
						<td>遊戲名稱：</td>
						<td><my:game hasChoose="true"/></td>
						<td>Banner:</td>
						<td>
							<input name="bannerUrl" value="" style="width: 70%;"/>
							<input type="button" value="上傳圖片" class="green_btn fillet_5" id="bannerUrl${loop.index }"/>
						</td>			
					</tr>				
				</c:forEach>				
			</table>
			</c:if>		
			
			<c:if test="${not empty gameRecomm }">
			<table class="table">				
				<tr>
					<td>遊戲名稱：</td>
						<td><my:game selected="${gameRecomm.gameCode }"/></td>
					<td>Banner${loop.index+1 }:</td>
					<td colspan="3">
						<input type="hidden" name="id" value="${gameRecomm.id }">
						<input name="bannerUrl" value="${gameRecomm.bannerUrl }" style="width: 70%;"/>
						<input type="button" value="上傳圖片" class="green_btn fillet_5" id="bannerUrl${loop.index+1 }"/>
					</td>
				</tr>				
			</table>
			</c:if>
		</div>
	</form>
	<input type="button" value="保存" id="save" />
	<form name="photo" id="iconUploadForm" enctype="multipart/form-data" method="post">
		<div style="display: none">
			<input type="file" id="IconBrowse" name="file" style="border: 0;" accept="image/*" />
		</div>
	</form>		
</div>

<script type="text/javascript">
	
	$(document).ready(function(e) {
		$.fn.select2.amd.require([ 'select2/compat/matcher' ], function(
				oldMatcher) {
			$("select[name='gameCode']").select2({
				matcher : oldMatcher(MyMatcher)
			});
		});
		
		var upFile;
		
		$('[id^=bannerUrl]').each(function(){
			$(this).click(function(){
				upFile = $(this).prev();
				$('#IconBrowse').click();
			});
		});
		
		$('#save').click(function() {			
			$("#editForm").ajaxSubmit({
				success : function(e) {
					alert(e.message), $("#myModal").modal("toggle");
					$('#search').click();
				},
				error : function() {
					alert("\u9023\u63a5\u7db2\u7d61\u5931\u6557");
				}
			})
		});
		
		$('#iconUploadForm').on('submit', (function(e) {
			e.preventDefault();
			var formData = new FormData(this);

			$.ajax({
				async : false,
				type : 'POST',
				url : '${ctx}/gameRecomm/upload.do',
				data : formData,
				cache : false,
				contentType : false,
				processData : false,
				success : function(data) {
					if (data == "overSize") {
						alert("檔案超過3MB請更換圖片");						
						$("#ImageBrowse").val("");
						return;
					}
					$("#IconBrowse").val("");
					upFile.val(data.url);					
				},
				error : function(data) {
					alert("系統錯誤，請稍後再試");					
				}
			});
		}));

		$("#IconBrowse").on("change", function() {
			if (this.files[0].size >= 10485760) {
				alert("檔案超過3MB請更換圖片");				
				return;
			}
			$("#iconUploadForm").submit();
		});
	});
	
</script>