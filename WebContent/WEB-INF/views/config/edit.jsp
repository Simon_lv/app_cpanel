<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/config/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${cp.id}" />
		<div class="table-responsive">		
			<table class="table">
				<tr>
					<td>影片連結:</td>
					<td><input name="viedoUrl" value="${cp.viedoUrl }" size="30"/></td>
				</tr>
				<c:choose>
					<c:when test="${empty cp.appiosVersion }">
						<tr>
							<td>app版本:</td>
							<td>							
								<input name="appVersion" value="${cp.appVersion }" size="30"/>								
							</td>
						</tr>
					</c:when>
					<c:when test="${empty cp.appVersion }">
						<tr>
							<td>ios版本:</td>
							<td>
								<input name="appiosVersion" value="${cp.appiosVersion }" size="30"/>								
							</td>
						</tr>
					</c:when>
				</c:choose>
				
				<tr>
					<td>付費遮蔽</td>
					<td>
						<select name="isPay">
						<option value="1" <c:if test="${cp.isPay == 1 }">selected="selected"</c:if>>打開</option>
						<option value="0" <c:if test="${cp.isPay == 0 }">selected="selected"</c:if>>隱藏</option>						
						</select>
					</td>
				</tr>
				<tr>
					<td>禮包遮蔽</td>
					<td>
						<select name="isBonus">
						<option value="1" <c:if test="${cp.isBonus == 1 }">selected="selected"</c:if>>打開</option>
						<option value="0" <c:if test="${cp.isBonus == 0 }">selected="selected"</c:if>>隱藏</option>						
						</select>
					</td>
				</tr>
			</table>
		</div>
	</form>
	<input type="button" value="保存" id="save" />	
</div>

<script type="text/javascript">
	
	$(document).ready(function(e) {
		
		$('#save').click(function() {
			
			var appVersion = $('[name=appVersion]').val();
			var appiosVersion = $('[name=appiosVersion]').val();
			
			if(typeof(appVersion) != 'undefined'){
				if(appVersion == ''){
					alert("請輸入app版本");
					return;
				}
			}
			
			if(typeof(appiosVersion) != 'undefined'){
				if(appiosVersion == ''){
					alert("請輸入ios版本");
					return;
				}
			}
			
			$("#editForm").ajaxSubmit({
				success : function(e) {
					alert(e.message), $("#myModal").modal("toggle");
					$('#search').click();
				},
				error : function() {
					alert("\u9023\u63a5\u7db2\u7d61\u5931\u6557");
				}
			})
		});
	
	});
	
</script>