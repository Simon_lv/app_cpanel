<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<style type="text/css">
	.container {
      width: 100%;
      overflow: scroll; /* showing scrollbars */
	}
</style>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<div class="container">
	<table class="table table-bordered table-full-width" id="list">
		<thead>
			<tr>
				<th>ID</th>
				<th>影片連結</th>
				<th>類型</th>
				<th>遮蔽版本</th>				
				<th>付費遮蔽</th>
				<th>禮包遮蔽</th>
				<th>建立日期</th>					
				<th>操作</th>
			</tr>
		</thead>
		<tbody>			
			<c:forEach items="${page.result}" var="o" varStatus="loop">
				<tr>
					<td>${o.id}</td>					
					<td>${o.viedoUrl}</td>
					<td>
						<c:choose>
							<c:when test="${empty o.appVersion }">appios</c:when>
							<c:when test="${empty o.appiosVersion }">app</c:when>
						</c:choose>						
					</td>
					<td>
						<c:choose>
							<c:when test="${empty o.appVersion }">${o.appiosVersion }</c:when>
							<c:when test="${empty o.appiosVersion }">${o.appVersion }</c:when>
						</c:choose>
					</td>
					<td>
						<c:choose>
							<c:when test="${o.isPay == 0 }">隱藏</c:when>
							<c:when test="${o.isPay == 1 }">打開</c:when>
						</c:choose>
					</td>
					<td>
						<c:choose>
							<c:when test="${o.isBonus == 0 }">隱藏</c:when>
							<c:when test="${o.isBonus == 1 }">打開</c:when>
						</c:choose>
					</td>
					<td>${o.createTime }</td>
					<td>
						<a href="javascript:;" onclick="edit(${o.id})">編輯</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>

	</table>
	</div>
</div>
<my:pagination page="${page}" />