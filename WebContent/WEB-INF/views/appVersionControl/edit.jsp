<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/appVersionControl/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${cp.id}" />
		<div class="table-responsive">
			<table class="table">
				<tr>					
					<td>遊戲code：</td>
					<td>
						<div class="form-group col-sm-12 col-md-4 col-lg-3">
							<c:if test="${empty cp.gameCode }">
							<my:game hasChoose="true" selected="${cp.gameCode }"/>
							</c:if>
							<c:if test="${not empty cp.gameCode }">
							<my:game selected="${cp.gameCode }"/>
							</c:if>
						</div>
					</td>
				</tr>
				<tr>
					<td>版本:</td>
					<td><input name="version" value="${cp.version }"/></td>					
				</tr>
				<tr>
					<td>下載URL</td>
					<td><input name="storeUrl" value="${cp.storeUrl }"/></td>
				</tr>
				<tr>
					<td>是否強更:</td>
					<td>
						<select name="isUpgrade">
							<option value="1" <c:if test="${cp.isUpgrade == 1 }">selected="selected"</c:if>>是</option>
							<option value="2" <c:if test="${cp.isUpgrade == 2 }">selected="selected"</c:if>>否</option>
						</select>
					</td>
				</tr>						
			</table>
		</div>
	</form>
	<input type="button" value="保存" id="save" />	
</div>

<script type="text/javascript">	
	
	$(function() {
		$.fn.select2.amd.require([ 'select2/compat/matcher' ], function(
				oldMatcher) {
			$("select[name='gameCode']").select2({
				matcher : oldMatcher(MyMatcher)
			});
		});
		
		$('#save').click(function() {
			$("#editForm").ajaxSubmit({
				success : function(e) {
					alert(e.message), $("#myModal").modal("toggle");
					$('#search').click();
				},
				error : function() {
					alert("\u9023\u63a5\u7db2\u7d61\u5931\u6557");
				}
			});
		});
	});

	function changeArea(area) {
		var value = area.value;
		if (1 == value) {
			var money = $('#money').val();
			$('#tax').val(money * 0.05);
		}
		if (2 == value) {
			var money = $('#money').val();
			$('#tax').val(money * 0);
		}
		return true;
	}

	function changeMoney() {
		var area = $('#area').val();
		if (1 == area) {
			var money = $('#money').val();
			$('#tax').val(money * 0.05);
		}
		if (2 == area) {
			var money = $('#money').val();
			$('#tax').val(money * 0);
		}
		return true;
	}
	
</script>