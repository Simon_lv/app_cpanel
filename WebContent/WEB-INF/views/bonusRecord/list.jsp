<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>

<style type="text/css">
	.container {
      width: 100%;
      overflow: scroll; /* showing scrollbars */
	}
</style>

<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	
	<div class="container">
	
	<table class="table table-bordered table-full-width" id="list">
		<thead>
			<tr>				
				<th>ID</th>
				<th>任務ID</th>				
				<th>userId</th>
				<th>紅利點數</th>				
				<th>ip</th>
				<th>裝置imei/idfa</th>
				<th>建立日期</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${page.result}" var="o">
				<tr>
					<td>${o.id }</td>
					<td>${o.missionId }</td>
					<td>${o.userId }</td>					
					<td>${o.bonus }</td>
					<td>${o.sourceIp }</td>
					<td>${o.deviceId}</td>
					<td>${o.createTime}</td>
				</tr>
			</c:forEach>
		</tbody>

	</table>
	
	</div>
	
</div>
<my:pagination page="${page}" />