<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
<head>
<title>8888play總後臺</title>
<style type="text/css">
.row {
	margin-left: 5px;
	margin-right: 5px;
}
</style>
</head>
<body>
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				系統管理 <small>菜單管理</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="${ctx}/index"><i class="fa fa-dashboard"></i>8888play總後臺</a></li>
				<li class="active">菜單管理</li>
			</ol>
		</section>
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">菜單</h4>
					</div>
					<form id="updateForm" class="form form-horizontal">
						<div class="modal-body">
							<div class="form-group">
								<label for="parentId" class="col-sm-2 control-label">父菜單</label>
								<div class="col-sm-10">
									<select class="form-control" name="parentId" id="parentId">
										<option value="0">根菜單</option>
										<c:forEach items="${select}" var="f">
											<option value="${f.id }">${f.name}</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="funName" class="col-sm-2 control-label">名稱</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="funName"
										id="funName" />
								</div>
							</div>
							<div class="form-group">
								<label for="funURL" class="col-sm-2 control-label">URL</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="funURL"
										id="funURL" />
								</div>
							</div>
							<div class="form-group">
								<label for="parentId" class="col-sm-2 control-label">類型</label>
								<div class="col-sm-10">
									<select class="form-control" name="funType" id="funType">
										<option value="0">菜單</option>
										<option value="1">控件</option>
									</select>
								</div>
							</div>
							<input type="hidden" name="id" id="funId">

						</div>
						<div class="modal-footer">
							<button type="button" id="submitForm1" class="btn btn-primary">保存</button>
							<button type="button" class="btn btn-default"
								data-dismiss="modal">关闭</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- Main content -->
		<div class="container-fluid">
			<div class="row">
				<div
					class="col-lg-8 col-md-8 col-sm-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-1"
					style="margin-top: 20px;">
					<form class="form-inline" action="${ctx }/function/list"
						method="get">
						<div class="form-group">
							<select class="form-control" name="parentId" id="parentIdSearch">
								<option value="0">根菜單</option>
								<c:forEach items="${select}" var="f">
									<option value="${f.id }">${f.name}</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group">
							<input type="submit" class="btn btn-primary" id="search"
								value="搜尋" />
							<button type="button" class="btn btn-success" id="addFun">新增</button>
						</div>
					</form>
				</div>
				<div class="clearfix"></div>
				<div class="row">
					<div
						class="table-responsive col-lg-8 col-md-8 col-sm-10 col-lg-offset-2 col-md-offset-2 col-sm-offset-1"
						style="margin-top: 20px;">
						<table class="table table-striped table-bordered table-hover">
							<tr>
								<th>菜單名</th>
								<th>鏈接</th>
								<th>操作</th>
							</tr>
							<c:forEach items="${list}" var="f">
								<tr>
									<td><c:choose>
											<c:when test="${f.parentId == 0}">
												<a href="${ctx}/function/list?parentId=${f.id}">${f.name}</a>
											</c:when>
											<c:otherwise>${f.name}</c:otherwise>
										</c:choose></td>
									<td>${f.url}</td>
									<td><button type="button" class="btn btn-info"
											onclick="update(${f.id})">修改</button>
										<button type="button" class="btn btn-primary"
											onclick="deleteFunction(${f.id},<c:if test='${ f.status == 0 }'>1</c:if><c:if test='${ f.status == 1 }'>0</c:if>)">
											<c:if test="${ f.status == 0 }">停用</c:if>
											<c:if test="${ f.status == 1 }">启用</c:if>
										</button></td>
								</tr>
							</c:forEach>
						</table>
					</div>
				</div>
			</div>
		</div>
	</aside>
	<script type="text/javascript">
	function update(id) {
		$.get("${ctx }/function/get",{"id":id},function(data){				
			$("#parentId option[value='"+data.parentId+"']").attr('selected', 'selected');
			$("#funId").val(data.id);
			$("#funName").val(data.name);
			$("#funURL").val(data.url);
			$("#myModal").modal('show');
		});
	}
	function deleteFunction(id,status){
		$.post("${ctx }/function/delete",{"id":id,"status":status},function(data){
			alert(data.message);
			$("#search").click();
		});
	}
	$(function(){
		$("#parentIdSearch option[value='${parentId}']").attr('selected', 'selected');
		$("#submitForm1").click(function(){
			$.post("${ctx }/function/save",{
				id:$("#funId").val(),
				parentId:$("#parentId").val(),
				name:$("#funName").val(),
				url:$("#funURL").val(),
				type:$("#funType").val()
				},function(data){
				alert(data.message);
				$("#myModal").modal('hide');
				$("#search").click();
			})
		});
		$("#addFun").click(function(){
			$("#parentId option[value='0']").attr('selected', 'selected');
			$("#funId").val('');
			$("#funName").val('');
			$("#funURL").val('');
			$("#myModal").modal('show');
		});
		});
	</script>
</body>
</html>