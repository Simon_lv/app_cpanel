<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
<head>
<title>8888play總後臺</title>
</head>
<body>
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel"></h4>
				</div>
				<div class="modal-body" id="myModalBody"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="modalSubmit">關閉</button>
				</div>
			</div>
		</div>
	</div>
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				基本資料 <small>兌換紀錄</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="${ctx}/index"><i class="fa fa-dashboard"></i>8888play總後臺</a></li>
				<li class="active">兌換紀錄</li>
			</ol>
		</section>
		<!-- Main content -->
		<div class="container-fluid">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body">
						<form action="${ctx}/exchangeRecord/list" id="listForm" method="post">
							<input type="hidden" id="pageNo" name="pageNo" value="" />
							<div class="table-responsive">
								<div class="form-group col-sm-12 col-md-4 col-lg-3">
	                            	<label for="inputid">userId:</label>
	                            	<input name="userId" value=""/>           
	                            </div>
								
								<div class="form-group col-sm-12 col-md-4 col-lg-3">
	                            	<label for="inputid">類型:</label>
	                            	<select name="type">
	                            		<option value="0">全部</option>
	                            		<option value="1">免費</option>
										<option value="2">限時</option>
										<option value="3">紅利</option>			
	                            	</select>               
	                            </div>
							
								<div class="form-group col-sm-12 col-md-4 col-lg-3">
	                            	<label for="inputid">遊戲名稱:</label>
	                            	<my:game hasAll="true"/>                 
	                            </div>
							 	<hr>
								<div class="btn-group col-sm-12 col-md-12 col-lg-6" role="group">
		                        	<input class="btn btn-primary" type="button" id="search" value="搜尋"/>                            
		                        </div>
								
							</div>
						</form>
					</div>
				</div>
			</div>
			<div id="listContainer"></div>
		</div>
	</aside>

	<script type="text/javascript" src="${ctx }/js/jquery.form.js"></script>
	<script type="text/javascript" src="/kindeditor/kindeditor-min.js"></script>
	<script type="text/javascript" src="/kindeditor/lang/zh_TW.js"></script>
	<script type="text/javascript">
		$(function() {
			$.fn.select2.amd.require([ 'select2/compat/matcher' ], function(
					oldMatcher) {
				$("select[name='gameCode']").select2({
					matcher : oldMatcher(MyMatcher)
				});
			});
			
			$('#search').click(function() {				
				$('#pageNo').val("1");
				$('#listContainer').html('<h1>正在搜尋，請稍候。。。</h1>');
				$('#listForm').ajaxSubmit({
					success : function(data) {
						$('#listContainer').html(data);
					}
				});
			});
			$('#export').click(function() {				
				var temp = $('#listForm').attr('action');
				console.log(temp);
				$('#listForm').attr('action', '${ctx}/device/export');
				$('#listForm').submit();
				$('#listForm').attr('action', temp);
			});
			$('#modalSubmit').click(function() {
				$('#myModal').modal('toggle');
				$('#search').click();
			});

		});

		function toPage(pageNo) {			
			if (!pageNo || pageNo <= 0) {
				return;
			}
			$('#pageNo').val(pageNo);
			$('#listForm').ajaxSubmit({
				success : function(data) {
					$('#listContainer').html(data);
				},
				error : function() {
					alert('連接網絡失敗');
				}
			});
			$('#listContainer').html('<h1>正在搜尋，請稍候。。。</h1>');
		}		

		function edit(id) {
			$(".modal-dialog").css("width", "900px"), $(".modal-dialog").css(
					"maxheight", "800px"), $("#myModal").modal("toggle"), $(
					"#myModalLabel").html("新增"), $("#myModalBody").html(
					"<h3>\u6b63\u5728\u6253\u958b\u3002\u3002\u3002</h3>"), $
					.get("/exchangeRecord/edit?id=" + id, function(e) {
						$("#myModalBody").html(e);
					})
		}		
	</script>
</body>
</html>