<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->

	<table class="table table-bordered table-full-width" id="list">
		<thead>
			<tr>
				<th>類型</th>
				<th>遊戲名稱</th>
				<th>圖片</th>
				<th>獎勵名稱</th>
				<th>userId</th>
				<th>消耗紅利點數</th>				
				<th>ip</th>
				<th>裝置imei/idfa</th>
				<th>稅換日期</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${page.result}" var="o">
				<tr>
					<th>
						<c:choose>
							<c:when test="${o.type == 1 }">免費</c:when>
							<c:when test="${o.type == 2 }">限時</c:when>
							<c:when test="${o.type == 3 }">紅利</c:when>
						</c:choose>
					</th>
					<td>${o.gameName }</td>
					<td><img src="${o.iconUrl }" width="100" height="100"/></td>
					<td>${o.desc }</td>
					<td>${o.userId }</td>
					<td>${o.cost }</td>
					<td>${o.sourceIp }</td>
					<td>${o.deviceId }</td>
					<td>${o.createTime }</td>					
				</tr>
			</c:forEach>
		</tbody>

	</table>
</div>
<my:pagination page="${page}" />