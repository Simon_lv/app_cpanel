<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/banner/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${cp.id}" />
		<div class="table-responsive">
			<table class="table">
				<tr>
					<td>標題</td>
					<td><input name="title" value="${cp.title }"/></td>
					<td>順序:	</td>
					<td><input type="number" name="orderIndex" value="${cp.orderIndex }"/></td>
				</tr>
				<tr>
					<td>動作:</td>
					<td>
						<select name="isOpen">
						<option value="1" <c:if test="${cp.isOpen == 1 }">selected="selected"</c:if>>打開新窗</option>
						<option value="0" <c:if test="${cp.isOpen == 0 }">selected="selected"</c:if>>無</option>
						</select>
					</td>
					<td>位置:</td>
					<td>
						<select name="position">
						<option value="0" <c:if test="${cp.position == 0 }">selected="selected"</c:if>>平臺首頁</option>
						<option value="1" <c:if test="${cp.position == 1 }">selected="selected"</c:if>>其他</option>
						</select>
					</td>
				</tr>				
				<tr>					
					<td>圖片：</td>
					<td>
						<input name="bannerUrl" value="${cp.bannerUrl }" id="iconUrl" style="width: 70%;" />
						<input type="button" value="上傳圖片" class="green_btn fillet_5" onClick="javascript:$('#IconBrowse').click()" />
					</td>
					<td>連結:</td>
					<td><input name="linkUrl" value="${cp.linkUrl }" style="width: 100%;"/></td>
				</tr>
				<tr>
					<td>開始時間:</td>
					<td>
						<input type="text" value="${cp.startTime}" onfocus="WdatePicker({startTime:'%y-%M-%d 00:00:00', dateFmt:'yyyy-MM-dd HH:mm:ss'})"
											class="Wdate" name="startTime" id="startTime">
					</td>
					<td>結束時間:</td>
					<td>
						<input type="text" value="${cp.endTime}" onfocus="WdatePicker({endTime:'%y-%M-%d 23:59:59', dateFmt:'yyyy-MM-dd HH:mm:ss'})"
											class="Wdate" name="endTime" id="endTime">
					</td>
				</tr>
			</table>
		</div>
	</form>
	<input type="button" value="保存" id="save" />
	<form name="photo" id="iconUploadForm" enctype="multipart/form-data" method="post">
		<div style="display: none">
			<input type="file" id="IconBrowse" name="file" style="border: 0;" accept="image/*" />
		</div>
	</form>
</div>

<script type="text/javascript">
	$(function() {
		$.fn.select2.amd.require([ 'select2/compat/matcher' ], function(
				oldMatcher) {
			$("select[name='gameCode']").select2({
				matcher : oldMatcher(MyMatcher)
			});
		});
		
		$('#save').click(function() {
			
			if($("[name=gameCode]").val() != '' && $("[name=linkUrl]").val() != ''){
				alert("遊戲code和連結請擇一填寫");
				return;
			}
			
			$("#editForm").ajaxSubmit({
				success : function(e) {
					alert(e.message), $("#myModal").modal("toggle");
					$('#search').click();
				},
				error : function() {
					alert("\u9023\u63a5\u7db2\u7d61\u5931\u6557");
				}
			});
		});
	});

	function changeArea(area) {
		var value = area.value;
		if (1 == value) {
			var money = $('#money').val();
			$('#tax').val(money * 0.05);
		}
		if (2 == value) {
			var money = $('#money').val();
			$('#tax').val(money * 0);
		}
		return true;
	}

	function changeMoney() {
		var area = $('#area').val();
		if (1 == area) {
			var money = $('#money').val();
			$('#tax').val(money * 0.05);
		}
		if (2 == area) {
			var money = $('#money').val();
			$('#tax').val(money * 0);
		}
		return true;
	}

	$(document).ready(function(e) {
		$('#iconUploadForm').on('submit', (function(e) {
			e.preventDefault();
			var formData = new FormData(this);

			$.ajax({
				async : false,
				type : 'POST',
				url : '${ctx}/banner/upload.do',
				data : formData,
				cache : false,
				contentType : false,
				processData : false,
				success : function(data) {
					if (data == "overSize") {
						$("#msg").text("檔案超過3MB請更換圖片");
						$("#msg").show();
						$("#ImageBrowse").val("");
						return;
					}

					$("#showImg").attr("src", data.url);
					$("#showImg").show();
					$("#iconUrl").val(data.url);

					// Clear Error Message
					$("#urlMsg").text("");
					$("#urlMsg").hide();
				},
				error : function(data) {
					$("#msg").text("系統錯誤，請稍後再試");
					$("#msg").show();
				}
			});
		}));

		$("#IconBrowse").on("change", function() {
			if (this.files[0].size >= 10485760) {
				alert("檔案超過3MB請更換圖片");
				$("#msg").show();
				return;
			}
			$("#iconUploadForm").submit();
		});
	});
</script>