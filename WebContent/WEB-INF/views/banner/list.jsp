<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->

	<table class="table table-bordered table-full-width" id="list">
		<thead>
			<tr>
				<th>標題</th>
				<th>遊戲名稱</th>
				<th>圖片</th>
				<th>開始日期</th>
				<th>結束日期</th>				
				<th>建立時間</th>			
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${page.result}" var="o">
				<tr>
					<td>${o.title}</td>
					<td>${o.gameName }</td>
					<td><img src="${o.bannerUrl }" height="100" width="100"></td>
					<td><fmt:formatDate value="${o.startTime}" type="date" pattern="yyyy-MM-dd" /></td>
					<td><fmt:formatDate value="${o.endTime}" type="date" pattern="yyyy-MM-dd" /></td>
					<td>${o.createTime }</td>
					<td>
						<a href="javascript:;" onclick="edit(${o.id})">編輯</a>
						<a href="javascript:;" onclick="deleteDevice(${o.id})">刪除</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>

	</table>
</div>
<my:pagination page="${page}" />