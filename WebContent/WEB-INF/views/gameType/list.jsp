<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<style type="text/css">
	.container {
      width: 100%;
      overflow: scroll; /* showing scrollbars */
	}
</style>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<div class="container">
	<table class="table table-bordered table-full-width" id="list">
		<thead>
			<tr>
				<th>ID</th>
				<th>類型說明</th>
				<th>建立日期</th>
				<th>狀態</th>				
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${page.result}" var="o">
				<tr>
					<td>${o.id}</td>
					<td>${o.name}</td>
					<td><fmt:formatDate value="${o.createTime }"/></td>					
					<td>
						<c:choose>
							<c:when test="${o.status == 1 }">正常</c:when>
							<c:when test="${o.status == 2 }">停權</c:when>
						</c:choose>
					</td>
					<td>
						<a href="javascript:;" onclick="edit(${o.id})">編輯</a>
						<c:if test="${o.status == 1 }">
							<a href="javascript:'" onclick="deleteGT(${o.id},2)">停權</a>
						</c:if>	
						<c:if test="${o.status == 2 }">
							<a href="javascript:'" onclick="deleteGT(${o.id},1)">啟用</a>
						</c:if>			
					</td>
				</tr>
			</c:forEach>
		</tbody>

	</table>
	</div>
</div>
<my:pagination page="${page}" />