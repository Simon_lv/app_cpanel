<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/gameType/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${cp.id}" />
		<div class="table-responsive">
			<table class="table">
				<tr>
					<td>類型說明：</td>
					<td><input name="name" value="${cp.name }"/></td>
				</tr>
				<tr>
					<td>狀態：</td>
					<td>
						<c:choose>
							<c:when test="${cp.status == 1 }">正常</c:when>
							<c:when test="${cp.status == 2 }">停權</c:when>
						</c:choose>
					</td>
				</tr>
			</table>
		</div>
	</form>
	<input type="button" value="保存" id="save" />	
</div>

<script type="text/javascript">
	$(function() {
		$('#save').click(function() {
			if ($("input[name='name']").val() == '') {
				alert("類型說明");
				return;
			}
			$("#editForm").ajaxSubmit({
				success : function(e) {
					alert(e.message), $("#myModal").modal("toggle");
					$('#search').click();
				},
				error : function() {
					alert("\u9023\u63a5\u7db2\u7d61\u5931\u6557");
				}
			})
		});
	});
</script>