<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/gameRecord/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${cp.id}" />
		<div class="table-responsive">
			<table class="table">
				<tr>
					<td>遊戲code：</td>
					<td>
						<c:if test="${empty cp.gameCode }">
						<my:game hasChoose="true"/>
						</c:if>
						
						<c:if test="${not empty cp.gameCode }">
						<my:game selected="${cp.gameCode }"/>
						</c:if>								
					</td>
					<td>遊戲包</td>
					<td><input name="packageName" value="${cp.packageName}" /></td>
				</tr>
				<tr>
					<td>類型：</td>
					<td><select name="type" onchange="changeArea(this)" id="area">							
							<option value="1" <c:if test="${cp.type == 1}">selected="selected"</c:if>>新上架</option>
							<option value="2" <c:if test="${cp.type == 2}">selected="selected"</c:if>>推薦</option>
							<option value="3" <c:if test="${cp.type == 3}">selected="selected"</c:if>>試玩</option>
							<option value="4" <c:if test="${cp.type == 4}">selected="selected"</c:if>>無</option>
					</select></td>
					<td>圖片：</td>
					<td><input name="iconUrl" value="${cp.iconUrl}" id="iconUrl" style="width: 70%;" /><input type="button" value="上傳圖片"
						class="green_btn fillet_5" onClick="javascript:$('#IconBrowse').click()" /></td>
				</tr>				
				<tr>
					<td>遊戲特色：</td>
					<td colspan="3"><textarea name="content" id="content" style="width: 70%; height: 100px;">${cp.content }</textarea></td>
				</tr>
				<tr>
					<td>大小(MB)：</td>
					<td><input type="number" name="size" value="${cp.size}" /></td>
					<td>遊戲類型：</td>
					<td>
						<my:gameType hasChoose="true" selected="${cp.gameTypeId }"/>
					</td>
				</tr>
				<tr>
					<td>簡介：</td>
					<td><input name="title" value="${cp.title }"/></td>
					<td>app首頁簡介</td>
					<td><input name="subTitle" value="${cp.subTitle }"/></td>
				</tr>
				<tr>
					<td>android下載 URL：</td>
					<td><input name="apkUrl" value="${cp.apkUrl}" /></td>
					<td>iOS下載URL</td>
					<td><input name="iosUrl" value="${cp.iosUrl}" /></td>
				</tr>
				<tr>
					<td>版號：</td>
					<td><input name="version" value="${cp.version}" /></td>
					<td>順序：</td>
					<td><input type="number" name="orderIndex" value="${cp.orderIndex }"/></td>
				</tr>
				<tr>
					<td>影片 Url：</td>
					<td><input name="videoUrl" value="${cp.videoUrl}" id="videoUrl" style="width: 70%;" /></td>
					<td>影片預覽圖 Url：</td>
					<td><input name="videoImgUrl" value="${cp.videoImgUrl}" id="videoImgUrl" style="width: 70%;" /><input type="button" value="上傳圖片"
						class="green_btn fillet_5" onClick="javascript:$('#ImageBrowse').click()" /></td>				
				</tr>
				<tr>
					<td>圖片1 Url：</td>
					<td><input name="imgUrl1" value="${cp.imgUrl1}" id="imgUrl1" style="width: 70%;" /><input type="button" value="上傳圖片"
						class="green_btn fillet_5" onClick="javascript:$('#ImageBrowse1').click()" /></td>
					<td>圖片2 Url：</td>
					<td><input name="imgUrl2" value="${cp.imgUrl2}" id="imgUrl2" style="width: 70%;" /><input type="button" value="上傳圖片"
						class="green_btn fillet_5" onClick="javascript:$('#ImageBrowse2').click()" /></td>
				</tr>
				<tr>
					<td>圖片3 Url：</td>
					<td><input name="imgUrl3" value="${cp.imgUrl3}" id="imgUrl3" style="width: 70%;" /><input type="button" value="上傳圖片"
						class="green_btn fillet_5" onClick="javascript:$('#ImageBrowse3').click()" /></td>
					<td>圖片4 Url：</td>
					<td><input name="imgUrl4" value="${cp.imgUrl4}" id="imgUrl4" style="width: 70%;" /><input type="button" value="上傳圖片"
						class="green_btn fillet_5" onClick="javascript:$('#ImageBrowse4').click()" /></td>
					
				</tr>
				<tr>
					<td>圖片5 Url：</td>
					<td><input name="imgUrl5" value="${cp.imgUrl5}" id="imgUrl5" style="width: 70%;" /><input type="button" value="上傳圖片"
						class="green_btn fillet_5" onClick="javascript:$('#ImageBrowse5').click()" /></td>
				</tr>				
			</table>
		</div>
	</form>
	<input type="button" value="保存" id="save" />
	
	<form name="photo" id="iconUploadForm" enctype="multipart/form-data" method="post">
		<div style="display: none">
			<input type="file" id="IconBrowse" name="file" style="border: 0;" accept="image/*" />
		</div>
	</form>
	<form name="photo" id="imageUploadForm" enctype="multipart/form-data" method="post">
		<div style="display: none">
			<input type="file" id="ImageBrowse" name="file" style="border: 0;" accept="image/*" />
		</div>
	</form>
	<form name="photo" id="imageUploadForm1" enctype="multipart/form-data" method="post">
		<div style="display: none">
			<input type="file" id="ImageBrowse1" name="file" style="border: 0;" accept="image/*" />
		</div>
	</form>
	<form name="photo" id="imageUploadForm2" enctype="multipart/form-data" method="post">
		<div style="display: none">
			<input type="file" id="ImageBrowse2" name="file" style="border: 0;" accept="image/*" />
		</div>
	</form>
	<form name="photo" id="imageUploadForm3" enctype="multipart/form-data" method="post">
		<div style="display: none">
			<input type="file" id="ImageBrowse3" name="file" style="border: 0;" accept="image/*" />
		</div>
	</form>
	<form name="photo" id="imageUploadForm4" enctype="multipart/form-data" method="post">
		<div style="display: none">
			<input type="file" id="ImageBrowse4" name="file" style="border: 0;" accept="image/*" />
		</div>
	</form>
	<form name="photo" id="imageUploadForm5" enctype="multipart/form-data" method="post">
		<div style="display: none">
			<input type="file" id="ImageBrowse5" name="file" style="border: 0;" accept="image/*" />
		</div>
	</form>
</div>

<script type="text/javascript">
	$(function() {		
		$('#save').click(function() {
			if ($('#gameTypeId').val() == '') {
				alert("請輸入遊戲類型");
				return;
			}
			
			if(($('[name=videoUrl]').val() == '' && $('[name=videoImgUrl]').val() != '') || ($('[name=videoUrl]').val() != '' && $('[name=videoImgUrl]').val() == '')){
				alert("影片Url或影片預覽圖Url未填寫");
				return;
			}
			
			if($("#content").val().length > 3000) {
				var str = "遊戲特色有"+$("#content").val().length+"個字,只能放1000個字喔~";
				alert(str);
				return;
			}
			
			$("#editForm").ajaxSubmit({
				success : function(e) {
					alert(e.message),
					$('#myModal').modal('hide');
					$('#myModal').on('hidden', function () {
					    // 关闭Dialog前移除编辑器
					    KindEditor.remove('textarea[name="content"]');
					});
					$('#search').click();
				},
				error : function() {
					alert("\u9023\u63a5\u7db2\u7d61\u5931\u6557");
				}
			})
		});
	});

	function changeArea(area) {
		var value = area.value;
		if (1 == value) {
			var money = $('#money').val();
			$('#tax').val(money * 0.05);
		}
		if (2 == value) {
			var money = $('#money').val();
			$('#tax').val(money * 0);
		}
		return true;
	}

	function changeMoney() {
		var area = $('#area').val();
		if (1 == area) {
			var money = $('#money').val();
			$('#tax').val(money * 0.05);
		}
		if (2 == area) {
			var money = $('#money').val();
			$('#tax').val(money * 0);
		}
		return true;
	}

	$(document).ready(function(e) {
		$.fn.select2.amd.require([ 'select2/compat/matcher' ], function(
				oldMatcher) {
			$("select[name='gameCode']").select2({
				matcher : oldMatcher(MyMatcher)
			});
		});
		
		keditor = KindEditor.create('textarea[name="content"]', {			
			resizeType : 1,
			langType : 'zh_TW',
			allowPreviewEmoticons : false,
			filterMode : false,
			allowFileManager : true,
			filePostName : "file",
			uploadJson  : '/fileUpload/save',
			extraFileUploadParams : {
	            module : "gameRecord"
	        },
	        afterBlur: function(){this.sync();},
			
			items : [ 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline', 'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist', 'insertunorderedlist', '|', 'emoticons', 'image',
					'link', '|', 'source', 'fullscreen', '|', 'preview' ]
		});
	});
	
	$(document).ready(function(e) {	
		$('#iconUploadForm').on('submit', (function(e) {
			e.preventDefault();
			var formData = new FormData(this);

			$.ajax({
				async : false,
				type : 'POST',
				url : '${ctx}/gameRecord/upload.do',
				data : formData,
				cache : false,
				contentType : false,
				processData : false,
				success : function(data) {
					if (data == "overSize") {
						$("#msg").text("檔案超過3MB請更換圖片");
						$("#msg").show();
						$("#ImageBrowse").val("");
						return;
					}

					$("#showImg").attr("src", data.url);
					$("#showImg").show();
					$("#iconUrl").val(data.url);

					// Clear Error Message
					$("#urlMsg").text("");
					$("#urlMsg").hide();
				},
				error : function(data) {
					$("#msg").text("系統錯誤，請稍後再試");
					$("#msg").show();
				}
			});
		}));

		$("#IconBrowse").on("change", function() {
			if (this.files[0].size >= 10485760) {
				alert("檔案超過3MB請更換圖片");
				$("#msg").show();
				return;
			}
			$("#iconUploadForm").submit();
		});
	});

	$(document).ready(function(e) {
		$('#imageUploadForm').on('submit', (function(e) {
			e.preventDefault();
			var formData = new FormData(this);

			$.ajax({
				async : false,
				type : 'POST',
				url : '${ctx}/gameRecord/upload.do',
				data : formData,
				cache : false,
				contentType : false,
				processData : false,
				success : function(data) {
					if (data == "overSize") {
						$("#msg").text("檔案超過3MB請更換圖片");
						$("#msg").show();
						$("#videoImgUrl").val("");
						return;
					}

					$("#showImg").attr("src", data.url);
					$("#showImg").show();
					$("#videoImgUrl").val(data.url);

					// Clear Error Message
					$("#urlMsg").text("");
					$("#urlMsg").hide();
				},
				error : function(data) {
					$("#msg").text("系統錯誤，請稍後再試");
					$("#msg").show();
				}
			});
		}));

		$("#ImageBrowse").on("change", function() {
			if (this.files[0].size >= 10485760) {
				$("#msg").text("檔案超過3MB請更換圖片");
				$("#msg").show();
				return;
			}
			$("#imageUploadForm").submit();
		});
	});
	
	$(document).ready(function(e) {
		$('#imageUploadForm1').on('submit', (function(e) {
			e.preventDefault();
			var formData = new FormData(this);

			$.ajax({
				async : false,
				type : 'POST',
				url : '${ctx}/gameRecord/upload.do',
				data : formData,
				cache : false,
				contentType : false,
				processData : false,
				success : function(data) {
					if (data == "overSize") {
						$("#msg").text("檔案超過3MB請更換圖片");
						$("#msg").show();
						$("#imgUrl1").val("");
						return;
					}

					$("#showImg").attr("src", data.url);
					$("#showImg").show();
					$("#imgUrl1").val(data.url);

					// Clear Error Message
					$("#urlMsg").text("");
					$("#urlMsg").hide();
				},
				error : function(data) {
					$("#msg").text("系統錯誤，請稍後再試");
					$("#msg").show();
				}
			});
		}));

		$("#ImageBrowse1").on("change", function() {
			if (this.files[0].size >= 10485760) {
				$("#msg").text("檔案超過3MB請更換圖片");
				$("#msg").show();
				return;
			}
			$("#imageUploadForm1").submit();
		});
	});

	$(document).ready(function(e) {
		$('#imageUploadForm2').on('submit', (function(e) {
			e.preventDefault();
			var formData = new FormData(this);

			$.ajax({
				async : false,
				type : 'POST',
				url : '${ctx}/gameRecord/upload.do',
				data : formData,
				cache : false,
				contentType : false,
				processData : false,
				success : function(data) {
					if (data == "overSize") {
						$("#msg").text("檔案超過3MB請更換圖片");
						$("#msg").show();
						$("#imgUrl2").val("");
						return;
					}

					$("#showImg").attr("src", data.url);
					$("#showImg").show();
					$("#imgUrl2").val(data.url);

					// Clear Error Message
					$("#urlMsg").text("");
					$("#urlMsg").hide();
				},
				error : function(data) {
					$("#msg").text("系統錯誤，請稍後再試");
					$("#msg").show();
				}
			});
		}));

		$("#ImageBrowse2").on("change", function() {
			if (this.files[0].size >= 10485760) {
				$("#msg").text("檔案超過3MB請更換圖片");
				$("#msg").show();
				return;
			}
			$("#imageUploadForm2").submit();
		});
	});

	$(document).ready(function(e) {
		$('#imageUploadForm3').on('submit', (function(e) {
			e.preventDefault();
			var formData = new FormData(this);

			$.ajax({
				async : false,
				type : 'POST',
				url : '${ctx}/gameRecord/upload.do',
				data : formData,
				cache : false,
				contentType : false,
				processData : false,
				success : function(data) {
					if (data == "overSize") {
						$("#msg").text("檔案超過3MB請更換圖片");
						$("#msg").show();
						$("#imgUrl3").val("");
						return;
					}

					$("#showImg").attr("src", data.url);
					$("#showImg").show();
					$("#imgUrl3").val(data.url);

					// Clear Error Message
					$("#urlMsg").text("");
					$("#urlMsg").hide();
				},
				error : function(data) {
					$("#msg").text("系統錯誤，請稍後再試");
					$("#msg").show();
				}
			});
		}));

		$("#ImageBrowse3").on("change", function() {
			if (this.files[0].size >= 10485760) {
				$("#msg").text("檔案超過3MB請更換圖片");
				$("#msg").show();
				return;
			}
			$("#imageUploadForm3").submit();
		});
	});

	$(document).ready(function(e) {
		$('#imageUploadForm4').on('submit', (function(e) {
			e.preventDefault();
			var formData = new FormData(this);

			$.ajax({
				async : false,
				type : 'POST',
				url : '${ctx}/gameRecord/upload.do',
				data : formData,
				cache : false,
				contentType : false,
				processData : false,
				success : function(data) {
					if (data == "overSize") {
						$("#msg").text("檔案超過3MB請更換圖片");
						$("#msg").show();
						$("#imgUrl4").val("");
						return;
					}

					$("#showImg").attr("src", data.url);
					$("#showImg").show();
					$("#imgUrl4").val(data.url);

					// Clear Error Message
					$("#urlMsg").text("");
					$("#urlMsg").hide();
				},
				error : function(data) {
					$("#msg").text("系統錯誤，請稍後再試");
					$("#msg").show();
				}
			});
		}));

		$("#ImageBrowse4").on("change", function() {
			if (this.files[0].size >= 10485760) {
				$("#msg").text("檔案超過3MB請更換圖片");
				$("#msg").show();
				return;
			}
			$("#imageUploadForm4").submit();
		});
	});

	$(document).ready(function(e) {
		$('#imageUploadForm5').on('submit', (function(e) {
			e.preventDefault();
			var formData = new FormData(this);

			$.ajax({
				async : false,
				type : 'POST',
				url : '${ctx}/gameRecord/upload.do',
				data : formData,
				cache : false,
				contentType : false,
				processData : false,
				success : function(data) {
					if (data == "overSize") {
						$("#msg").text("檔案超過3MB請更換圖片");
						$("#msg").show();
						$("#imgUrl5").val("");
						return;
					}

					$("#showImg").attr("src", data.url);
					$("#showImg").show();
					$("#imgUrl5").val(data.url);

					// Clear Error Message
					$("#urlMsg").text("");
					$("#urlMsg").hide();
				},
				error : function(data) {
					$("#msg").text("系統錯誤，請稍後再試");
					$("#msg").show();
				}
			});
		}));

		$("#ImageBrowse5").on("change", function() {
			if (this.files[0].size >= 10485760) {
				$("#msg").text("檔案超過3MB請更換圖片");
				$("#msg").show();
				return;
			}
			$("#imageUploadForm5").submit();
		});
	});
</script>