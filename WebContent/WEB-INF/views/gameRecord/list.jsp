<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<style type="text/css">
	.container {
      width: 100%;
      overflow: scroll; /* showing scrollbars */
	}
</style>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<div class="container">
	<table class="table table-bordered table-full-width" id="list">
		<thead>
			<tr>
				<th>遊戲</th>
				<th>遊戲包</th>
				<th>類型</th>
				<th>圖示</th>
				<th>android下載URL</th>
				<th>iOS下載URL</th>
				<th>遊戲類型</th>
				<th>簡介</th>
				<th>app副標題</th>				
				<th>版號</th>
				<th>大小(MB)</th>
				<th>影片</th>
				<th>影片預覽圖</th>
				<th>圖片1</th>
				<th>圖片2</th>
				<th>圖片3</th>
				<th>圖片4</th>
				<th>圖片5</th>
				<th>順序</th>
				<th>建立時間</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${page.result}" var="o">
				<tr>
					<td>${o.gameName}</td>
					<td>${o.packageName}</td>
					<td>
						<c:if test="${o.type == 1}">新上架</c:if>
						<c:if test="${o.type == 2}">推薦</c:if>
						<c:if test="${o.type == 3}">試玩</c:if>
						<c:if test="${o.type == 4}">無</c:if>
					</td>
					<th><img src="${o.iconUrl}" height="100" width="100"></th>
					<td><a href="${o.apkUrl}" target="_blank">link</a></td>
					<td><a href="${o.iosUrl}" target="_blank">link</a></td>
					<td>${o.gameType }</td>
					<td>${o.title }</td>
					<td>${o.subTitle }</td>					
					<td>${o.version}</td>
					<td>${o.size}</td>
					<td>
						<c:if test="${not empty o.videoUrl }">
						<a href="${o.videoUrl}" target="_blank">link</a>
						</c:if>
					</td>
					<td>
						<c:if test="${not empty o.videoImgUrl }">
						<img src="${o.videoImgUrl}" height="100" width="100">
						</c:if>						
					</td>
					<td><img src="${o.imgUrl1}" height="100" width="100"></td>
					<td><img src="${o.imgUrl2}" height="100" width="100"></td>					
					<td><img src="${o.imgUrl3}" height="100" width="100"></td>
					<td><img src="${o.imgUrl4}" height="100" width="100"></td>
					<td><img src="${o.imgUrl5}" height="100" width="100"></td>
					<td>${o.orderIndex }</td>
					<td>${o.createTime}</td>
					<td><a href="javascript:;" onclick="edit(${o.id})">編輯</a> <a href="javascript:;" onclick="deleteDevice(${o.id})">刪除</a></td>
				</tr>
			</c:forEach>
		</tbody>

	</table>
	</div>
</div>
<my:pagination page="${page}" />