<%@page import="com.baplay.entity.TEfunuser"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	TEfunuser user = (TEfunuser) request.getSession().getAttribute("loginUser");
	if (user != null){
		//response.sendRedirect(request.getContextPath()+"/index");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>8888play總後臺  | 登 入</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resources/boostrap/css/bootstrap.min.css" />
<style type="text/css">
	body {
	  padding-top: 40px;
	  padding-bottom: 40px;
	  background-color: #eee;
	}
	
	.form-signin {
	  max-width: 330px;
	  padding: 15px;
	  margin: 0 auto;
	}
	.form-signin .form-signin-heading,
	.form-signin .checkbox {
	  margin-bottom: 10px;
	}
	.form-signin .checkbox {
	  font-weight: normal;
	}
	.form-signin .form-control {
	  position: relative;
	  height: auto;
	  -webkit-box-sizing: border-box;
	     -moz-box-sizing: border-box;
	          box-sizing: border-box;
	  padding: 10px;
	  font-size: 16px;
	}
	.form-signin .form-control:focus {
	  z-index: 2;
	}
	.form-signin input[type="email"] {
	  margin-bottom: -1px;
	  border-bottom-right-radius: 0;
	  border-bottom-left-radius: 0;
	}
	.form-signin input[type="password"] {
	  margin-bottom: 10px;
	  border-top-left-radius: 0;
	  border-top-right-radius: 0;
	}
</style>
</head>
<body>
 <div class="container">
 	<%-- <p class="text-right"><a href="${pageContext.request.contextPath }/register">Sign up</a></p> --%>
 	<form id="loginForm" action="login" method="post" name="loginForm" class="form-signin" role="form">
        <h2 class="form-signin-heading">請 登 入</h2>
        <input type="text" name="username" id="username" class="form-control" placeholder="用戶名" data-container="body" data-trigger="manual" data-toggle="popover" data-placement="right" />
        <input type="password" name="password" id="password" class="form-control" placeholder="密碼" data-container="body" data-trigger="manual" data-toggle="popover" data-placement="right" >
        <!-- <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div> -->
        <button id="loginBtn" class="btn btn-lg btn-primary btn-block" type="button">登 入</button>
	  <br/>
 	</form>
	  <c:if test="${not empty message_login }">
	  	<div class="alert alert-danger" role="alert">
			<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<strong>Error!</strong> ${message_login}
		</div>
	  </c:if>
 	<!-- bottom -->
<jsp:include page="/include/include_bottom.jsp"></jsp:include>
   </div>
    <script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.8.2.js" ></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/resources/boostrap/js/bootstrap.min.js" ></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/resources/boostrap/js/tooltip.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/resources/boostrap/js/popover.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$(document).keyup(function(event){
			if (event.keyCode == 13){
				$("#loginBtn").trigger("click");
			}
		});
		
		$("#loginBtn").click(function(){
			var username = $.trim($("#username").val());
			if (username == ''){
				 $("#username").attr("data-content","请输入用户名");
				 $("#username").popover('show');
				return;
			}else{
				$("#username").popover('hide');
			}
			var password = $.trim($("#password").val());
			if (password == ''){
				 $("#password").attr("data-content","请输入密码");
				 $("#password").popover('show');
				return;
			}else{
				$("#password").popover('hide');
			}
			//提交
			$("#loginForm").submit();
		});
	});
</script>
</body>
</html>