<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	
	<table
		class="table table-bordered table-full-width"
		id="list">
		<thead>
			<tr>
				<th>ID</th>				
				<th>類型</th>
				<th>遊戲</th>
				<th>圖示URL</th>
				<th>獲取方式</th>
				<th>增加點數</th>
				<th>兌換獎品ID</th>
				<th>順序</th>
				<th>建立日期</th>
				<th>狀態</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${page.result}" var="o">
					<tr>
						<td>${o.id }</td>
						<td>
							<c:choose>
								<c:when test="${o.type == '1'}">免費</c:when>
								<c:when test="${o.type == '2'}">限時</c:when>
								<c:when test="${o.type == '3'}">試玩紅利</c:when>
								<c:when test="${o.type == '4'}">手機綁定</c:when>
							</c:choose>
						</td>
						<td>
							<c:if test="${empty o.gameName}">全部</c:if>
							<c:if test="${not empty o.gameName }">${o.gameName }</c:if>
						</td>
						<td><img src="${o.iconUrl}" height="100" width="100"/></td>
						<td>${o.content}</td>
						<td>${o.bonus}</td>
						<td>${o.prizeId}</td>
						<td>${o.orderIndex}</td>
						<td>${o.createTime}</td>
						<td>
							<c:if test="${o.status == 1}">正常</c:if>
							<c:if test="${o.status == 2}">停權</c:if>
						</td>					
						<td>
							<a href="javascript:;" onclick="edit(${o.id})">編輯</a>
							
							<c:choose>
								<c:when test="${o.status == '1'}"><a href="javascript:;" onclick="updateStatus(${o.id}, 2)">下架</a></c:when>
								<c:when test="${o.status == '2'}"><a href="javascript:;" onclick="updateStatus(${o.id}, 1)">上架</a></c:when>
							</c:choose>
						</td>
					</tr>		
			</c:forEach>
		</tbody>

	</table>
</div>
<my:pagination page="${page}" />