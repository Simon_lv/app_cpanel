<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>

<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/mission/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${mission.id}" />
		<div class="table-responsive">
			<table class="table">				
				<tr>
					<td>類型：</td>
					<td>
						<select name="type">
							<option value="1" <c:if test="${mission.type == 1 }">selected="selected"</c:if>>免費</option>							
							<option value="2" <c:if test="${mission.type == 2 }">selected="selected"</c:if>>限時</option>
							<option value="3" <c:if test="${mission.type == 3 }">selected="selected"</c:if>>試玩紅利</option>
							<option value="4" <c:if test="${mission.type == 4 }">selected="selected"</c:if>>手機綁定</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>遊戲code:</td>
					<td>
						<div class="form-group col-sm-12 col-md-4 col-lg-3">
							<c:if test="${empty mission.gameCode }">
							<my:game hasChoose="true" selected="${mission.gameCode }"/>
							</c:if>
							<c:if test="${not empty mission.gameCode }">
							<my:game selected="${mission.gameCode }"/>
							</c:if>
						</div>
					</td>
				</tr>
				<tr>
					<td>圖示URL：</td>
					<td><input name="iconUrl" value="${mission.iconUrl}"
						id="iconUrl" style="width: 70%;" /><br /> <input type="button"
						value="上傳圖片" onClick="javascript:$('#ImageBrowse').click()" /></td>
				</tr>
				<tr>
					<td>獲取方式：</td>
					<td><textarea name="content" id="content" rows="5" cols="100">${mission.content}</textarea></td>
				</tr>
				<tr>
					<td>增加點數：</td>
					<td><input type="number" name="bonus" id="bonus"
						value="${mission.bonus}"></td>
				</tr>
				<tr>
					<td>兌換獎品ID：</td>
					<td><input type="number" name="prizeId" id="prizeId"
						value="${mission.prizeId}"></td>
				</tr>
				<tr>
					<td>順序：</td>
					<td><input type="number" name="orderIndex" id="orderIndex"
						value="${mission.orderIndex}"></td>
				</tr>
				<tr>
					<td>狀態</td>
					<td><c:choose>
							<c:when test="${mission.status == '1'}">
								上架
							</c:when>
							<c:when test="${mission.status == '2'}">
								下架
							</c:when>
						</c:choose></td>
				</tr>
			</table>
		</div>
	</form>
	<input type="button" value="保存" id="save"/>

	<form name="photo" id="imageUploadForm" enctype="multipart/form-data"
		method="post">
		<div style="display: none">
			<input type="file" id="ImageBrowse" name="file" style="border: 0;"
				accept="image/*" />
		</div>
	</form>
</div>

<script type="text/javascript">

	$(function(){
		$('#save').click(function() {
			
			if ($("#content").val() == '') {
				alert("請輸入獲取方式");
				return;
			}
			if ($("#bonus").val() == '') {
				alert("請輸入增加點數");
				return;
			}			
			
			$("#editForm").ajaxSubmit({
				success : function(e) {
					alert(e.message),
					$('#myModal').modal('hide');
					$('#myModal').on('hidden', function () {					    
					   
					});
					$('#search').click();
				},
				error : function() {
					alert("\u9023\u63a5\u7db2\u7d61\u5931\u6557");
				}
			});
		});
		
	});		
	
	$(document).ready(function(e) {
		
		$.fn.select2.amd.require([ 'select2/compat/matcher' ], function(
				oldMatcher) {
			$("select[name='gameCode']").select2({
				matcher : oldMatcher(MyMatcher)
			});
		});
		
		$('#imageUploadForm').on('submit', (function(e) {
			e.preventDefault();
			var formData = new FormData(this);

			$.ajax({
				async : false,
				type : 'POST',
				url : '${ctx}/mission/upload.do',
				data : formData,
				cache : false,
				contentType : false,
				processData : false,
				success : function(data) {
					if (data == "overSize") {
						alert("檔案超過3MB請更換圖片");
						$("#ImageBrowse").val("");
						return;
					}

					$("#showImg").attr("src", data.url);
					$("#showImg").show();
					$("#iconUrl").val(data.url);

					// Clear Error Message
					$("#urlMsg").text("");
					$("#urlMsg").hide();
				},
				error : function(data) {
					alert("系統錯誤，請稍後再試");

				}
			});
		}));

		$("#ImageBrowse").on("change", function() {
			if (this.files[0].size >= 10485760) {
				alert("檔案超過3MB請更換圖片");

				return;
			}
			$("#imageUploadForm").submit();
		});
	});
</script>