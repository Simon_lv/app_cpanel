<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
<head>
<title>8888play總後臺</title>

<style type="text/css">
.Wdate {
	width: 45%;
}
</style>
</head>
<body>
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel"></h4>
				</div>
				<div class="modal-body" id="myModalBody"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="modalSubmit">關閉</button>
				</div>
			</div>
		</div>
	</div>
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				基本資料 <small>任務管理</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="${ctx}/index"><i class="fa fa-dashboard"></i>8888play總後臺</a></li>
				<li class="active">任務管理</li>
			</ol>
		</section>
		<!-- Main content -->
		<div class="container-fluid">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body">
						<form action="${ctx}/mission/list" id="listForm" method="post">
							<input type="hidden" id="pageNo" name="pageNo" value="" />
							<div class="table-responsive">
								<table class="table">
									<tr>
										<td width="8%">遊戲Code:</td>
										<td width="10%">
											<my:game hasAll="true"/>
										</td>
										<td width="8%">類型：</td>
										<td>
											<select name="type">
												<option value="">全部</option>
												<option value="1">免費</option>
												<option value="2">限時</option>
												<option value="3">試玩紅利</option>
												<option value="4">手機綁定</option>
											</select>
										</td>
										<td>
											<input type="button" id="search" value="搜尋"/>
											<input id="add" type="button" value="新增" />											
										</td>										
									</tr>									
								</table>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div id="listContainer"></div>
		</div>
	</aside>

	<script type="text/javascript" src="${ctx }/js/jquery.form.js"></script>
	<script type="text/javascript">
		$(function() {
			$.fn.select2.amd.require([ 'select2/compat/matcher' ], function(
					oldMatcher) {
				$("select[name='gameCode']").select2({
					matcher : oldMatcher(MyMatcher)
				});
			});
			
			$("#add").click(function() {
		        $(".modal-dialog").css("width", "900px"), $(".modal-dialog").css("maxheight", "800px"), $("#myModal").modal("toggle"), $("#myModalLabel").html("新增"), $("#myModalBody").html("<h3>\u6b63\u5728\u6253\u958b\u3002\u3002\u3002</h3>"), $.get("/mission/edit?id=", function(e) {
		            $("#myModalBody").html(e);
		        })
		    });
		    
			$('#search').click(function() {
				var flag = dayCheck();
				if (!flag) {
					return;
				}
				$('#pageNo').val("1");
				$('#listContainer').html('<h1>正在搜尋，請稍候。。。</h1>');
				$('#listForm').ajaxSubmit({
					success : function(data) {
						$('#listContainer').html(data);
					}
				});
			});
			$('#export').click(function() {
				if (!dayCheck()) {
					return;
				}
				var temp = $('#listForm').attr('action');
				console.log(temp);
				$('#listForm').attr('action', '${ctx}/mission/export');
				$('#listForm').submit();
				$('#listForm').attr('action', temp);
			});
			$('#modalSubmit').click(function() {
				$('#myModal').modal('toggle');
				$('#search').click();
			});

			
		});

		function toPage(pageNo) {
			if (!dayCheck()) {
				return;
			}
			if (!pageNo || pageNo <= 0) {
				return;
			}
			$('#pageNo').val(pageNo);
			$('#listForm').ajaxSubmit({
				success : function(data) {
					$('#listContainer').html(data);
				},
				error : function() {
					alert('連接網絡失敗');
				}
			});
			$('#listContainer').html('<h1>正在搜尋，請稍候。。。</h1>');
		}
		function dayCheck() {
			var beginTime = $("#beginTime").val();
			if (!beginTime) {
				var beginTimeDate = new Date(beginTime);
				var endTime = $("#endTime").val();
				var endTimeDate = new Date();
				if (endTime) {
					endTimeDate = new Date(endTime);
				}
				if (beginTimeDate > endTimeDate) {
					alert("結束時間不能大於開始時間!");
					return false;
				}
			}
			return true;
		}
		
		function edit(id) {
			$(".modal-dialog").css("width", "900px"), $(".modal-dialog").css("maxheight", "800px"), $("#myModal").modal("toggle"), $("#myModalLabel").html("編輯"), $("#myModalBody").html("<h3>\u6b63\u5728\u6253\u958b\u3002\u3002\u3002</h3>"), $.get("/mission/edit?id="+id, function(e) {
	            $("#myModalBody").html(e);
	        })
		}
		
		function updateStatus(id, status) {
			var text = "";
			if(status == 1) {
				text = "上架";
			} else if(status == 2) {
				text = "下架";
			}
			
			if(!confirm("確定要"+text+"?")) {
				return;
			}
			$.ajax({
				url : '${ctx}/mission/updateStatus',
				dataType : 'json',
				data : {
					'id' : id,
					'status' : status
				},
				success : function(data) {
					alert(data.message);
					$('#search').click();
				},
				error : function() {
					alert('網絡連接失敗，請聯繫管理員');
				}
			});
		}		
		
	</script>
</body>
</html>