<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
    <head>
        <title>后台管理 - 修改密碼</title>
    </head>
    <body>
   	<aside class="right-side">
      <!-- Content Header (Page header) -->
      <section class="content-header">
          <h1>
              	首頁
              <small>修改密碼</small>
          </h1>
          <ol class="breadcrumb">
              <li><a href="${ctx }/index"><i class="fa fa-dashboard"></i> 首頁</a></li>
              <li class="active">修改密碼</li>
          </ol>
      </section>
      <!-- Main content -->
      <section class="content">
          <div class="row">
              
              <div class="col-md-6" style="margin-left: 300px;">
              	<c:if test="${not empty msg }">
              		<div class="alert alert-warning alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					  <strong>提示!</strong> ${msg }
					</div>
              	</c:if>
                  <div class="box box-primary" >
                      <div class="box-header">
                          <h3 class="box-title">修改密碼</h3>
                      </div>
                      <div class="box-body">
                      	<form id="updateForm" name="updateForm" method="post">
                          <div class="form-group">
                              <label>原密碼:</label>
                              <div class="input-group">
                                  <div class="input-group-addon">
                                      <i class="fa fa-lock"></i>
                                  </div>
                                  <input type="password" autocomplete="off" id="oldpassword" name="oldpassword" class="form-control" data-container="body" data-trigger="manual" data-toggle="popover" data-placement="right"  />
                              </div>
                               <label>新密碼:</label>
                              <div class="input-group">
                                  <div class="input-group-addon">
                                      <i class="fa fa-lock"></i>
                                  </div>
                                  <input type="password" autocomplete="off" id="password" name="password" class="form-control" data-container="body" data-trigger="manual" data-toggle="popover" data-placement="right"  />
                              </div>
                              <label>確認新密碼:</label>
                              <div class="input-group">
                                  <div class="input-group-addon">
                                      <i class="fa fa-lock"></i>
                                  </div>
                                  <input type="password" autocomplete="off" id="passwordcfm" name="passwordcfm" class="form-control" data-container="body" data-trigger="manual" data-toggle="popover" data-placement="right"  />
                              </div>
                          </div>
                      	</form>
                          <div class="input-group-btn" style="text-align: center;">
                          	<button id="update" class="btn btn-flat btn-primary">送 出</button>
                          </div>
                          <p class="text-warning" id="message">
	             	      </p>
                      </div>
                  </div>

              </div>
          </div>
      </section>
  </aside>
  <script type="text/javascript" src="${ctx }/resources/boostrap/js/tooltip.js"></script>
<script type="text/javascript" src="${ctx }/resources/boostrap/js/popover.js"></script>
<script type="text/javascript" src="${ctx }/js/validate/jquery.validate.js" ></script>
<script type="text/javascript" src="${ctx }/js/validate/messages_cn.js" ></script>
<script type="text/javascript" src="${ctx }/js/jquery.form.js" ></script>
  	<script type="text/javascript">
  		$(document).ready(function(){
  			var e;
   			jQuery.validator.addMethod("isUserName", function(value, element) {
   				return this.optional(element) || /^\w{3,15}$/.test(value);
   			}, "請修正帳號");
   			//註冊validate函數
   			$("#updateForm").validate({
   				rules : {
   					password : {
   						required : true,
   						minlength : 6
   					},
   					passwordcfm : {
   						required : true,
   						minlength : 6,
   						equalTo : "#password"
   					},
   					oldpassword : {
   						required : true,
   						minlength : 6,
   						remote : "${ctx}/user/checkpwd"
   					}
   					
   				},
   				messages : {
   					password : {
   						required : "請輸入新密碼",
   						minlength : "新密碼至少為6位"
   					},
   					passwordcfm : {
   						required : "請確認新密碼",
   						minlength : "確認新密碼至少為为6位",
   						equalTo	: "兩次密碼不一致"
   					},
   					oldpassword : {
   						required : "請輸入原密碼",
   						minlength : "原密碼至少為6位",
   						remote : "原密碼不正確"
   					}
   				},
   				errorPlacement: function(error, element) {
   					e = element.attr('id');
   					$("#"+e).parent().addClass("has-error");
   					$("#"+e).attr("data-content",error.html());
   					$("#"+e).popover('show');
   				},
   				success: function(label) {
   					$("#"+e).parent().removeClass("has-error");
   					$("#"+e).popover('hide');
   				}
   			});
 			//新增 			
  			$("#update").click(function(){
  				if ($("#updateForm").valid()){
  					$("#updateForm").ajaxSubmit({
  						url : "${ctx }/user/updatepwd",
  						type : "POST",
  		   				dataType : "text",
  		   				clearForm : true,
	   					resetForm : true,
  		   				success : function(data){
  		   					switch (data){
  		   					case "nullparam" :
  		   						$("#message").html("請確認信息是否錄入完整");
  		   						break;
  		   					case "true" :
  		   						$("#message").html("密碼修改成功");
  		   						break;
  		   					case "false" :
  		   						$("#message").html("密碼修改失敗");
  		   						break;
  		   					case "differ" :
  		   						$("#message").html("新密碼不一致");
  		   						break;
  		   					case "old" :
		   						$("#message").html("原密碼不一致");
		   						break;
  		   					case "notlogin" :
  		   						$("#message").html("請先登錄再進行該項操作");
  		   						default:
  		   							alert(data);
  		   					}
  		   				}
  					});
  				}
  			});
  		});
  	</script>
    </body>
</html>