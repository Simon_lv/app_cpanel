<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
    <head>
        <title>后台管理</title>
    </head>
    <body>
   	<aside class="right-side">
          <!-- Content Header (Page header) -->
          <section class="content-header">
              <h1>
                	用戶管理
                  <small>待審用戶</small>
              </h1>
              <ol class="breadcrumb">
                  <li><a href="#"><i class="fa fa-dashboard"></i> 用戶管理</a></li>
                  <li class="active">待審用戶</li>
              </ol>
          </section>
          <!-- Main content -->
          <section class="content">
              <!-- Main row -->
              <div class="row">
                  <div class="col-xs-12">
                      <div class="box">
                          <div class="box-header">
                              <h3 class="box-title">待審用戶列表</h3>
                              <div class="box-tools">
                                  <div class="input-group">
                                  	<form action="${ctx }/user/unaudited" name="searchForm" id="searchForm" method="post">
                                      <input type="text" id="search" name="search" value="${search }" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                                  	</form>
                                      <div class="input-group-btn">
                                          <button id="searchBtn" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                                      </div>
                                      <div class="input-group-btn">
                                          <button id="auditUser" style="margin-left: 5px;" class="btn btn-sm btn-primary">激活用戶</button>
                                          <!-- <button id="modifyGroup" style="margin-left: 5px;" class="btn btn-sm btn-info">修改用戶</button> -->
                                      	  <button id="deleteUser" style="margin-left: 5px;" class="btn btn-sm btn-danger">刪除用戶</button>
                                      </div>
                                  </div>
                              </div>
                          </div><!-- /.box-header -->
                          <div class="box-body table-responsive no-padding">
                              <form name="submitForm" id="submitForm">
	                              <table class="table table-hover">
	                                  <tr>
	                                  	  <th><input type="checkbox" id="selectAll" /></th>
	                                      <th>編號</th>
	                                      <th>用戶名</th>
	                                      <th>所屬組</th>
	                                      <th>角色名</th>
	                                      <th>暱稱</th>
	                                      <th>郵箱</th>
	                                      <th>創建時間</th>
	                                      <th>最後登錄時間</th>
	                                      <th>狀態</th>
	                                      <th>登錄次數</th>
	                                  </tr>
	                                   <c:forEach var="user" varStatus="i" items="${list }">
		                                  <tr>
		                                  	  <td style="width: 15px;"><input name="checkbox" id="checkbox" value="${user.id }" type="checkbox" /></td>
		                                      <td>${i.index + 1 }</td>
		                                      <td>
		                                      	<c:choose>
		                                      		<c:when test="${fn:containsIgnoreCase(user.loginName,search) && not empty search }">
		                                      			<font color="red">${user.loginName }</font>
		                                      		</c:when>
		                                      		<c:otherwise>
		                                      			${user.loginName }
		                                      		</c:otherwise>
		                                      	</c:choose>
		                                      </td>
		                                      <td>${user.groupName }</td>
		                                      <td>${user.roleName }</td>
		                                      <td>${user.nickname }</td>
		                                      <td>
		                                      	<c:choose>
		                                      		<c:when test="${fn:containsIgnoreCase(user.email,search) && not empty search }">
		                                      			<font color="red">${user.email }</font>
		                                      		</c:when>
		                                      		<c:otherwise>${user.email }</c:otherwise>
		                                      	</c:choose>
		                                      	
		                                      </td>
		                                      <td>${user.createTime }</td>
		                                      <td>${user.lastLogin }</td>
		                                      <td>
		                                      	<c:if test="${user.status == 0 }">未激活</c:if>
		                                      </td>
		                                      <td>${user.loginCount }</td>
		                                  </tr>
	                                  </c:forEach>
	                              </table>
                              </form>
                          </div><!-- /.box-body -->
                      </div><!-- /.box -->
                  </div>
              </div>
              <!-- /.row (main row) -->

          </section><!-- /.content -->
      </aside>
      <script type="text/javascript" src="${ctx }/js/jquery.form.js" ></script>
      <script type="text/javascript">
      	$(document).ready(function(){
      		$("#usermanageLi").addClass("active");
  			$("#unuseableLi").addClass("active");
  			
  			$("#selectAll").click(function(){
  				if ($(this).attr("checked")){
	  				$("input[name='checkbox']").attr("checked",true);
  				}else{
  					$("input[name='checkbox']").attr("checked",false);
  				}
  			});
  			
  			$("#searchBtn").click(function(){
  				var search = $.trim($("#search").val());
  				if (search == ''){
  					alert("請輸入查詢內容");
  					return;
  				}
  				$("#searchForm").submit();
  			});
  			
  			$("#auditUser").click(function(){
  				var length = $("input[name='checkbox']:checked").size();
  				if (length <= 0){
  					alert("請至少選擇一條記錄進行激活");
  					return;
  				}
  				
  			//異步提交表單
			$("#submitForm").ajaxSubmit({
				url : "${ctx }/user/audit",
				type : "POST",
   				dataType : "text",
   				success : function(data){
   					switch (data){
   					case "nullparam" :
   						alert("請至少選擇一項進行操作");
   						break;
   					case "success" :
   						window.location.reload(); 
   						break;
   					case "error" :
   						alert("激活用戶失敗");
   						default:
   							alert(data);
   					}
   				}
			});
  			});
  			
  			//刪除用戶
  			$("#deleteUser").click(function(){
  				var length = $("input[name='checkbox']:checked").size();
  				if (length == 0){
  					alert("請至少選擇一條記錄進行操作");
  					return;
  				}
  				if (confirm("刪除用戶，數據將無法恢復？")){
  				//異步提交表單
  					$("#submitForm").ajaxSubmit({
  						url : "${ctx }/user/deleteusers",
  						type : "POST",
  		   				dataType : "text",
  		   				success : function(data){
  		   					switch (data){
  		   					case "nullparam" :
  		   						alert("請至少選擇一項進行操作");
  		   						break;
  		   					case "success" :
  		   						window.location.reload(); 
  		   						break;
  		   					case "error" :
  		   						alert("刪除用戶失敗");
  		   						default:
  		   							alert(data);
  		   					}
  		   				}
  					});
  				}
  			});
      	});
      	
      </script>
    </body>
</html>