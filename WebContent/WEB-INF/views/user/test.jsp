<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
    <head>
        <title>后台管理</title>
    </head>
    <body>
   	<aside class="right-side">
          <!-- Content Header (Page header) -->
          <section class="content-header">
              <h1>
                  	用戶管理
                  <small>用戶列表</small>
              </h1>
              <ol class="breadcrumb">
                  <li><a href="#"><i class="fa fa-dashboard"></i> 用戶管理</a></li>
                  <li class="active">用戶列表</li>
              </ol>
          </section>
          <!-- Main content -->
          <section class="content">
              <!-- Main row -->
              <div class="row">
                  <div class="col-xs-12">
                      <div class="box">
                          <div class="box-header">
                              <h3 class="box-title">用戶列表(已激活用戶)</h3>
                              <div class="box-tools">
                                  <div class="input-group">
                                  	<form id="searchForm" action="${ctx }/user/list/" method="post">
                                      <input type="text" value="" id="search" name="search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                                  	</form>
                                      <div class="input-group-btn">
                                          <button id="searchBtn" class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                                      </div>
                                      <div class="input-group-btn">
                                          <button id="addUser" style="margin-left: 5px;" class="btn btn-sm btn-info">新增用戶</button>
                                          <button id="lockUser" style="margin-left: 5px;" class="btn btn-sm btn-warning">禁用用戶</button>
                                          <button id="promoteUser" style="margin-left: 5px;" class="btn btn-sm btn-primary">提升組長</button>
                                          <button id="moveUser" style="margin-left: 5px;" class="btn btn-sm btn-success">用户转移组</button>
                                      	  <button id="deleteUser" style="margin-left: 5px;" class="btn btn-sm btn-danger">刪除用户</button>
                                      	  <button id="modifyPwd" style="margin-left: 5px;" class="btn btn-sm btn-default">修改密码</button>
                                      </div>
                                  </div>
                              </div>
                          </div><!-- /.box-header -->
                          <div class="box-body table-responsive no-padding">
                              <form name="submitForm" id="submitForm">
	                              <table class="table table-hover">
	                                  <tr>
	                                  	  <th><input type="checkbox" id="selectAll" /></th>
	                                      <th>編號</th>
	                                      <th>用戶名</th>
	                                      <th>所屬組</th>
	                                      <th>角色名</th>
	                                      <th>暱稱</th>
	                                      <th>郵箱</th>
	                                      <th>創建時間</th>
	                                      <th>最後登錄時間</th>
	                                      <th>狀態</th>
	                                      <th>登錄次數</th>
	                                  </tr>
	                                 
	                              </table>
	                              <input type="hidden" id="roleId" name="roleId" value="" />
	                              <input type="hidden" id="groupId" name="groupId" value="" />
                              </form>
                         
                          </div>
                      </div>
                  </div>
              </div>
          </section>
      </aside>
      
		
		
      <script type="text/javascript" src="${ctx }/js/jquery.form.js" ></script>

    </body>
</html>