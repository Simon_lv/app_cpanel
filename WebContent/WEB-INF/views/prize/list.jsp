<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->

	<table class="table table-bordered table-full-width" id="list">
		<thead>
			<tr>
				<th>獎勵ID</th>
				<th>遊戲</th>
				<th>類型</th>
				<th>圖示</th>				
				<th>獎勵名稱</th>
				<th>獎勵內容</th>
				<th>使用規則</th>
				<th>消耗點數</th>
				<th>開始日期</th>
				<th>結束日期</th>
				<th>序號結束時間</th>
				<th>建立時間</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${page.result}" var="o">
				<tr>
					<td>${o.id }</td>
					<td>${o.gameName}</td>
					<td>
						<c:if test="${o.type == 1}">免費</c:if>
						<c:if test="${o.type == 2}">限時</c:if>
						<c:if test="${o.type == 3}">紅利</c:if>
					</td>
					<td><img src="${o.iconUrl}" height="100" width="100"></td>					
					<td>${o.desc}</td>
					<td>${o.content}</td>
					<td>${o.usage}</td>
					<td>${o.cost}</td>
					<td><fmt:formatDate value="${o.startTime}" type="date" pattern="yyyy-MM-dd" /></td>
					<td><fmt:formatDate value="${o.endTime}" type="date" pattern="yyyy-MM-dd" /></td>
					<td><fmt:formatDate value="${o.limitTime }" type="date" pattern="yyyy-MM-dd" /></td>
					<td>${o.createTime}</td>
					<td><a href="javascript:;" onclick="edit(${o.id})">編輯</a> <a href="javascript:;" onclick="deleteDevice(${o.id})">刪除</a></td>
				</tr>
			</c:forEach>
		</tbody>

	</table>
</div>
<my:pagination page="${page}" />