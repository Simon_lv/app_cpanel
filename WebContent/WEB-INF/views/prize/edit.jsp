<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/prize/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${cp.id}" /> <input type="hidden" name="status" id="status" value="${cp.status}" />
		<div class="table-responsive">
			<table class="table">
				<tr>
					<td>遊戲code：</td>
					<td>
						<div class="form-group col-sm-12 col-md-4 col-lg-3">
							<c:if test="${empty cp.gameCode }">
								<my:game hasChoose="true"/>
							</c:if>
							<c:if test="${not empty cp.gameCode }">
								<my:game selected="${cp.gameCode }"/>
							</c:if>
						</div>
					</td>
					<td>類型：</td>
					<td><select name="type" onchange="changeArea(this)" id="area">
							<option value="1" <c:if test="${cp.type == 1}">selected="selected"</c:if>>免費</option>
							<option value="2" <c:if test="${cp.type == 2}">selected="selected"</c:if>>限時</option>
							<option value="3" <c:if test="${cp.type == 3}">selected="selected"</c:if>>紅利</option>
					</select></td>
				</tr>
				<tr>
					<td>圖片：</td>
					<td><input name="iconUrl" value="${cp.iconUrl}" id="iconUrl" style="width: 70%;" /><input type="button" value="上傳圖片"
						class="green_btn fillet_5" onClick="javascript:$('#IconBrowse').click()" /></td>
					<td>獎勵名稱：</td>
					<td><input name="desc" value="${cp.desc}" /></td>
				</tr>
				<tr>
					<td>獎勵內容：</td>
					<td><input name="content" value="${cp.content}" /></td>
					<td>使用規則：</td>
					<td><textarea name="usage">${cp.usage }</textarea></td>
				</tr>
				<tr>
					<td>消耗點數：</td>
					<td><input type="number" id="cost" name="cost" readonly <c:if test="${empty cp }">value="0"</c:if><c:if test="${not empty cp }">value="${cp.cost}"</c:if> /></td>
					<td>順序：</td>
					<td><input name="order_index" value="${cp.order_index}" /></td>
				</tr>
				<tr>
					<td>開始日期：</td>
					<td><input type="text" value="<fmt:formatDate value="${cp.startTime}" type="date" pattern="yyyy-MM-dd" />" onfocus="WdatePicker({startDate:'%y-%M-%d 00:00:00', dateFmt:'yyyy-MM-dd'})"
						class="Wdate" name="startTime" id="startTime"></td>
					<td>結束日期：</td>
					<td><input type="text" value="<fmt:formatDate value="${cp.endTime}" type="date" pattern="yyyy-MM-dd" />" onfocus="WdatePicker({startDate:'%y-%M-%d 23:59:59', dateFmt:'yyyy-MM-dd'})"
						class="Wdate" name="endTime" id="endTime"></td>
				</tr>
				<tr>
					<td>序號結束日期：</td>
					<td><input type="text" value="<fmt:formatDate value="${cp.limitTime}" type="date" pattern="yyyy-MM-dd" />" onfocus="WdatePicker({limitTime:'%y-%M-%d 00:00:00', dateFmt:'yyyy-MM-dd'})"
						class="Wdate" name="limitTime" id="limitTime"></td>
						<td colspan="2"></td>
				</tr>
				<!-- 
				<tr>
					<td>狀態：</td>
					<td><select name="status" >
						<option value="1" <c:if test="${1 == cp.status}">selected="selected"</c:if>>正常</option>
						<option value="2" <c:if test="${2 == cp.status}">selected="selected"</c:if>>停權</option>
					</select></td>
				</tr>
				-->
			</table>
		</div>
	</form>
	<input type="button" value="保存" id="save" />
	<form name="photo" id="iconUploadForm" enctype="multipart/form-data" method="post">
		<div style="display: none">
			<input type="file" id="IconBrowse" name="file" style="border: 0;" accept="image/*" />
		</div>
	</form>
</div>

<script type="text/javascript">
	$(function() {
		$.fn.select2.amd.require([ 'select2/compat/matcher' ], function(
				oldMatcher) {
			$("select[name='gameCode']").select2({
				matcher : oldMatcher(MyMatcher)
			});
		});
			
		$('#save').click(function() {			
			
			if($("[name=order_index]").val() == ''){
				alert("請輸入順序");
				return;
			}
			
			if($("[name=type]").val() == 2){
				if($("[name=startTime]").val() == '' || $("[name=endTime]").val() == ''){
					alert("請輸入起訖時間");
					return;
				}
			}
			
			if($("[name=startTime]").val() != '' && $("[name=endTime]").val() != ''){
				
				if ( (Date.parse($("[name=endTime]").val())).valueOf() < (Date.parse($("[name=startTime]").val())).valueOf())
				{
					alert("開始時間不可大於結束時間");
					return;
				}	
				
			}
			
			if($("[name=limitTime]").val() == '' ){
				alert("請輸入序號結束時間");
				return;				
			}			
			
			$("#editForm").ajaxSubmit({
				success : function(e) {
					alert(e.message), $("#myModal").modal("toggle");
					$('#search').click();
				},
				error : function() {
					alert("\u9023\u63a5\u7db2\u7d61\u5931\u6557");
				}
			})
		});
	});

	$("#cost").attr("style","background-color:lightgrey;");
	
	function changeArea(area) {
		var value = area.value;		
		
		if (1 == value || 2 == value) {
			$('#cost').val("0");
			$('#cost').attr("readonly","readonly");
			$("#cost").attr("style","background-color:lightgrey;");
			
		}
		if (3 == value) {			
			$('#cost').removeAttr("readonly");
			$("#cost").removeAttr("style");
			$("#cost").attr("style","background-color:white;");
		}
		
		return true;
	}

	$(document).ready(function(e) {
		$('#iconUploadForm').on('submit', (function(e) {
			e.preventDefault();
			var formData = new FormData(this);

			$.ajax({
				async : false,
				type : 'POST',
				url : '${ctx}/gameRecord/upload.do',
				data : formData,
				cache : false,
				contentType : false,
				processData : false,
				success : function(data) {
					if (data == "overSize") {
						$("#msg").text("檔案超過3MB請更換圖片");
						$("#msg").show();
						$("#ImageBrowse").val("");
						return;
					}

					$("#showImg").attr("src", data.url);
					$("#showImg").show();
					$("#iconUrl").val(data.url);

					// Clear Error Message
					$("#urlMsg").text("");
					$("#urlMsg").hide();
				},
				error : function(data) {
					$("#msg").text("系統錯誤，請稍後再試");
					$("#msg").show();
				}
			});
		}));

		$("#IconBrowse").on("change", function() {
			if (this.files[0].size >= 10485760) {
				alert("檔案超過3MB請更換圖片");
				$("#msg").show();
				return;
			}
			$("#iconUploadForm").submit();
		});
	});
</script>