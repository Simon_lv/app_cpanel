<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/systemNotify/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${cp.id}" /> <input type="hidden" name="status" id="status" value="${cp.status}" />
		<div class="table-responsive">
			<table class="table">
				<tr>
					<td>會員uid</td>
					<td><input name="userId" value="${cp.userId}"<c:if test="${edit == 'edit'}">readonly style="background-color:LightGray;"</c:if> /></td>
				</tr>
				<tr>
					<td>抬頭：</td>
					<td><input name="title" value="${cp.title}" /></td>
				</tr>
				<tr>
					<td>訊息：</td>
					<td><textarea style="width: 80%;" name="message">${cp.message}</textarea></td>
				</tr>
			</table>
		</div>
	</form>
	<input type="button" value="保存" id="save" />
</div>

<script type="text/javascript">
	$(function() {
		$('#save').click(function() {
			if ($("input[name='name']").val() == '') {
				alert("請輸入原廠名稱");
				return;
			}
			$("#editForm").ajaxSubmit({
				success : function(e) {
					alert(e.message), $("#myModal").modal("toggle");
					$('#search').click();
				},
				error : function() {
					alert("\u9023\u63a5\u7db2\u7d61\u5931\u6557");
				}
			})
		});
	});

	function changeArea(area) {
		var value = area.value;
		if (1 == value) {
			var money = $('#money').val();
			$('#tax').val(money * 0.05);
		}
		if (2 == value) {
			var money = $('#money').val();
			$('#tax').val(money * 0);
		}
		return true;
	}

	function changeMoney() {
		var area = $('#area').val();
		if (1 == area) {
			var money = $('#money').val();
			$('#tax').val(money * 0.05);
		}
		if (2 == area) {
			var money = $('#money').val();
			$('#tax').val(money * 0);
		}
		return true;
	}
</script>