<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>



<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	
	<table class="table table-bordered table-full-width" id="list">
		<thead>
			<tr>
				<th>ID</th>
				<th>會員uid</th>
				<th>抬頭</th>
				<th>訊息</th>
				<th>建立時間</th>
				<th>狀態</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${page.result}" var="o">
				<tr>
					<td>${o.id }</td>				
					<td>${o.userId}</td>
					<td>${o.title}</td>
					<td>
						<div style="width: 400px;word-wrap: break-word;word-break: break-all;">${o.message}</div>						
					</td>
					<td>${o.createTime}</td>
					<td>
						<c:choose>
							<c:when test="${o.status == '1' }">未讀</c:when>
							<c:when test="${o.status == '2' }">已讀</c:when>							
						</c:choose>
					</td>
					<td>
						<a href="javascript:;" onclick="edit(${o.id})">編輯</a> 
					</td>
				</tr>
			</c:forEach>
		</tbody>

	</table>	
</div>
<my:pagination page="${page}" />