<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
<title>8888play總後臺</title>
</head>
<body>
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>首頁</h1>
			<ol class="breadcrumb">
				<li class="active"><a href="#"><i class="fa fa-home"></i>
						首頁</a></li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<!-- Main row -->
			<div class="row">
				<div class="col-xs-12" style="padding: 100px 0 100px 0;">
					<!-- /.box-header -->
					<shiro:authenticated>
						<h1 class="text-center">歡迎${loginUser.username }登入8888play總後臺</h1>
					</shiro:authenticated>
					<!-- /.box-body -->
					<!-- /.box -->
				</div>
			</div>
			<!-- /.row (main row) -->

		</section>
		<!-- /.content -->
	</aside>
</body>
</html>