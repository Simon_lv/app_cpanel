<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
<head>
<title>8888play總後臺</title>

<style type="text/css">
.Wdate {
	width: 45%;
}
</style>
</head>
<body>
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel"></h4>
				</div>
				<div class="modal-body" id="myModalBody"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="modalSubmit">關閉</button>
				</div>
			</div>
		</div>
	</div>
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				基本資料 <small>推播管理</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="${ctx}/index"><i class="fa fa-dashboard"></i>8888play總後臺</a></li>
				<li class="active">推播管理</li>
			</ol>
		</section>
		<!-- Main content -->
		<div class="container-fluid">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body">
						<form action="${ctx}/push/list" id="listForm" method="post">
							<input type="hidden" id="pageNo" name="pageNo" value="" />
							<div class="table-responsive">
								<table class="table">
									<tr>
										<td>推播通知抬頭：</td>
										<td><input name="title"></td>
										<td>推播預約時間：</td>
										<td><input type="text" value=""
											onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd HH:mm:ss'})"
											class="Wdate" name="pushTimeStart" id="pushTimeStart">~
										<input type="text" value=""
											onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd HH:mm:ss'})"
											class="Wdate" name="pushTimeEnd" id="pushTimeEnd"></td>
										<td>推播時間：</td>
										<td><input type="text" value=""
											onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd HH:mm:ss'})"
											class="Wdate" name="runTimeStart" id="runTimeStart">~
										<input type="text" value=""
											onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd HH:mm:ss'})"
											class="Wdate" name="runTimeEnd" id="runTimeEnd"></td>
									</tr>
									<tr>
										<td>iOS推播時間：</td>
										<td><input type="text" value=""
											onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd HH:mm:ss'})"
											class="Wdate" name="iosRunTimeStart" id="iosRunTimeStart">~
										<input type="text" value=""
											onfocus="WdatePicker({startDate:'%y-%M-%d', dateFmt:'yyyy-MM-dd HH:mm:ss'})"
											class="Wdate" name="iosRunTimeEnd" id="iosRunTimeEnd"></td>
									</tr>
									<tr>
										<td><input id="add" type="button" value="新增" />
										<input type="button" id="search" value="搜尋">
										<!-- <input type="button" id="export" value="導出列表"></td> -->
									</tr>
								</table>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div id="listContainer"></div>
		</div>
	</aside>

	<script type="text/javascript" src="${ctx }/js/jquery.form.js"></script>
	<script type="text/javascript">
		$(function() {
			$("#add").click(function() {
		        $(".modal-dialog").css("width", "900px"), $(".modal-dialog").css("maxheight", "800px"), $("#myModal").modal("toggle"), $("#myModalLabel").html("新增"), $("#myModalBody").html("<h3>\u6b63\u5728\u6253\u958b\u3002\u3002\u3002</h3>"), $.get("/push/edit?id=", function(e) {
		            $("#myModalBody").html(e);
		        })
		    });
		    
			$('#search').click(function() {
				var flag = dayCheck();
				if (!flag) {
					return;
				}
				$('#pageNo').val("1");
				$('#listContainer').html('<h1>正在搜尋，請稍候。。。</h1>');
				$('#listForm').ajaxSubmit({
					success : function(data) {
						$('#listContainer').html(data);
					}
				});
			});
			$('#export').click(function() {
				if (!dayCheck()) {
					return;
				}
				var temp = $('#listForm').attr('action');
				console.log(temp);
				$('#listForm').attr('action', '${ctx}/push/export');
				$('#listForm').submit();
				$('#listForm').attr('action', temp);
			});
			$('#modalSubmit').click(function() {
				$('#myModal').modal('toggle');
				$('#search').click();
			});

			
		});

		function toPage(pageNo) {
			if (!dayCheck()) {
				return;
			}
			if (!pageNo || pageNo <= 0) {
				return;
			}
			$('#pageNo').val(pageNo);
			$('#listForm').ajaxSubmit({
				success : function(data) {
					$('#listContainer').html(data);
				},
				error : function() {
					alert('連接網絡失敗');
				}
			});
			$('#listContainer').html('<h1>正在搜尋，請稍候。。。</h1>');
		}
		function dayCheck() {
			var beginTime = $("#pushTimeStart").val();
			if (beginTime) {
				var beginTimeDate = new Date(beginTime);
				var endTime = $("#pushTimeEnd").val();
				var endTimeDate = new Date();
				if (endTime) {
					endTimeDate = new Date(endTime);
				}
				if (beginTimeDate > endTimeDate) {
					alert("推播預約時間結束時間不能大於開始時間!");
					return false;
				}
			}
			var beginTime = $("#runTimeStart").val();
			if (beginTime) {
				var beginTimeDate = new Date(beginTime);
				var endTime = $("#runTimeEnd").val();
				var endTimeDate = new Date();
				if (endTime) {
					endTimeDate = new Date(endTime);
				}
				if (beginTimeDate > endTimeDate) {
					alert("推播時間結束時間不能大於開始時間!");
					return false;
				}
			}
			return true;
		}
		
		function edit(id) {
			$(".modal-dialog").css("width", "900px"), $(".modal-dialog").css("maxheight", "800px"), $("#myModal").modal("toggle"), $("#myModalLabel").html("編輯"), $("#myModalBody").html("<h3>\u6b63\u5728\u6253\u958b\u3002\u3002\u3002</h3>"), $.get("/push/edit?id="+id, function(e) {
	            $("#myModalBody").html(e);
	        })
		}
		
		function deleteDevice(name, id) {
			if(!confirm("確定刪除"+name+"?")) {
				return;
			}
			$.ajax({
				url : '${ctx}/push/delete',
				dataType : 'json',
				data : {
					'id' : id
				},
				success : function(data) {
					alert(data.message);
					$('#search').click();
				},
				error : function() {
					alert('網絡連接失敗，請聯繫管理員');
				}
			});
		}
		
		function save() {
			if ($("input[name='title']").val() == '') {
				alert("請輸入推播通知抬頭");
				return;
			}
			if ($("input[name='content']").val() == '') {
				alert("請輸入推播內文");
				return;
			}
			if ($("input[name='pushTime']").val() == '') {
				alert("請輸入推播預約時間");
				return;
			}
			if ($("input[name='imgUrl']").val() == '') {
				alert("請上傳圖片");
				return;
			}
			$("#editForm").ajaxSubmit({
				success : function(e) {
					alert(e.message), $("#myModal").modal("toggle");
					$("#search").click();
				},
				error : function() {
					alert("\u9023\u63a5\u7db2\u7d61\u5931\u6557");
				}
			})
		}
	</script>
</body>
</html>