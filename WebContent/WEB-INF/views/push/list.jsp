<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<style type="text/css">
	.container {
      width: 100%;
      overflow: scroll; /* showing scrollbars */
	}
</style>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<div class="container">
	<table
		class="table table-bordered table-full-width"
		id="list">
		<thead>
			<tr>
				<th>ID</th>
				<th>推播通知抬頭</th>
				<th>推播內文</th>				
				<th>推播預約時間</th>
				<th>推播時間</th>
				<th>iOS推播時間</th>
				<th>圖片URL</th>
				<th>後台編輯帳號</th>
				<th>修改時間</th>
				<th>後台新增帳號</th>
				<th>創建時間</th>
			<!-- 	<th>狀態</th>  -->
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${page.result}" var="o">
					<tr>
					<td>${o.id}</td>
					<td>${o.title}</td>
					<td>${o.content}</td>
					<!-- 
					<td><c:if test="${o.type == 1}">內頁</c:if>
						<c:if test="${o.type == 2}">APP CODE</c:if>
					</td>
					<td>${o.traceId}</td>
					 -->
					<td>${o.pushTime}</td>
					<td>${o.runTime}</td>
					<td>${o.iosRunTime}</td>
					<td><img src="${o.imgUrl}" width="100" height="100" /></td>
					<td>${o.modifier}</td>
					<td>${o.updateTime}</td>
					<td>${o.creator}</td>
					<td>${o.createTime}</td>				
					<td>
						<a href="javascript:;" onclick="edit(${o.id})">編輯</a>
						<a href="javascript:;" onclick="deleteDevice('${o.title}', ${o.id})">刪除</a>
					</td>
					</tr>		
			</c:forEach>
		</tbody>

	</table>
	</div>
</div>
<my:pagination page="${page}" />