<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>

<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/push/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${push.id}" />
		<div class="table-responsive">
			<table class="table">
				<tr>
					<td width="40%">推播通知抬頭：</td>
					<td><input name="title" value="${push.title}"></td>
				</tr>
				<tr>
					<td>推播內文：</td>
					<td><textarea rows="10" cols="50" name="content"> ${push.content}</textarea></td>
				</tr>
				<tr style="display:none;">
					<td>類型：</td>
					<td><select name="type">
						<option value="1" <c:if test="${push.type == 1}">selected="selected"</c:if>>內頁</option>
						<option value="2" <c:if test="${push.type == 2}">selected="selected"</c:if>>APP CODE</option>
					</select></td>
				</tr>
				<tr style="display:none;">
					<td>追蹤ID：</td>
					<td><input name="traceId" value="${push.traceId}"></td>
				</tr>
				<tr>
					<td>推播預約時間：</td>
					<td>
					<input style="width: 45%;"
						onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" type="text"
						name="pushTime" value="${push.pushTime}" id="endTime" class="Wdate"></td>
				</tr>
				<tr>
					<td>圖片URL：</td>
					<td><input name="imgUrl" value="${push.imgUrl}" id="imgUrl" style="width: 70%;" /><br/>
							<input type="button" value="上傳圖片" class="green_btn fillet_5" onClick="javascript:$('#ImageBrowse').click()" />
					</td>
				</tr>
				
			</table>
		</div>
	</form>
	<input type="button" value="保存" onclick="save()" />
	<c:if test="${not empty push && (empty push.runTime || empty push.iosRunTime) }">
	<input type="button" value="推送" id="send"/>
	</c:if>
	
	<form name="photo" id="imageUploadForm" enctype="multipart/form-data" method="post">
		    <div style="display: none"><input type="file" id="ImageBrowse" name="file" style="border: 0;" accept="image/*" /></div>
	</form>
</div>

<script type="text/javascript">
$(document).ready(function (e) {
	
	$('#send').click(function() {
		$("#editForm").attr("action", "${ctx}/push/doPush");
		
		$("#editForm").ajaxSubmit({
			success : function(e) {
				alert(e.message), $("#myModal").modal("toggle");
				$('#search').click();
			},
			error : function() {
				alert("\u9023\u63a5\u7db2\u7d61\u5931\u6557");
			}
		})
	});
	
    $('#imageUploadForm').on('submit',(function(e) {
        e.preventDefault();
        var formData = new FormData(this);

        $.ajax({
        	async:false,
            type:'POST',
            url: '${ctx}/push/upload.do',
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
            	if(data == "overSize") {
            		alert("檔案超過3MB請更換圖片");
            		$("#ImageBrowse").val("");
            		return;
            	}
            	
            	$("#showImg").attr("src", data.url);
            	$("#showImg").show();
            	$("#imgUrl").val(data.url);
            	
            	// Clear Error Message
            	$("#urlMsg").text("");
            	$("#urlMsg").hide();
            },
            error: function(data){
            	alert("系統錯誤，請稍後再試");
        		
            }
        });
    }));

    $("#ImageBrowse").on("change", function() {
    	if(this.files[0].size >= 10485760) {
    		alert("檔案超過3MB請更換圖片");
    		
    		return;
    	}
        $("#imageUploadForm").submit();
    });   
    
});
</script>