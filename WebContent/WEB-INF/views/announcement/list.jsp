<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>

<style type="text/css">
	.container {
      width: 1024px;
      overflow: scroll; /* showing scrollbars */
	}
</style>

<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	
	<div class="container">
	
	<table class="table table-bordered table-full-width" id="list">
		<thead>
			<tr>
				<th>類型</th>
				<th>isVip</th>
				<th>遊戲名稱</th>				
				<th>標題</th>
				<th>位置</th>
				<th>內容</th>	
				<th>順序</th>			
				<th>開始日期</th>
				<th>結束日期</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${page.result}" var="o">
				<tr>
					<td>
						<c:choose>
							<c:when test="${o.type == 1 }">系統</c:when>
							<c:when test="${o.type == 2 }">活動</c:when>
							<c:when test="${o.type == 3 }">攻略</c:when>
						</c:choose>
					</td>
					<td>
						<c:choose>
							<c:when test="${o.isVip == 1 }">是</c:when>
							<c:when test="${o.isVip == 0 }">否</c:when>							
						</c:choose>
					</td>
					<td>${o.gameName }</td>					
					<td>${o.title }</td>
					<td>
						<c:if test="${cp.position == 0 }">平臺首頁</c:if>
						<c:if test="${cp.position == 1 }">其他</c:if>
					</td>
					<td>${o.content }</td>
					<td>${o.orderIndex }</td>
					<td><fmt:formatDate value="${o.startTime}" type="date" pattern="yyyy-MM-dd" /></td>
					<td><fmt:formatDate value="${o.endTime}" type="date" pattern="yyyy-MM-dd" /></td>					
					<td>
						<a class="a_class" href="javascript:;" onclick="edit(${o.id})">編輯</a>
						<a class="a_class" href="javascript:;" onclick="deleteDevice(${o.id})">刪除</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>

	</table>
	
	</div>
	
</div>
<my:pagination page="${page}" />