<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/announcement/save" method="post" id="editForm">
		<input type="hidden" name="id" id="id" value="${cp.id}" />
		<div class="table-responsive">
			<table class="table">
				<tr>
					<td>標題</td>
					<td><input name="title" value="${cp.title }"/></td>
					<td>遊戲code：</td>
					<td>
						<div class="form-group col-sm-12 col-md-4 col-lg-3">
							<c:if test="${empty cp.gameCode }">
							<my:game hasChoose="true"/>
							</c:if>
							<c:if test="${not empty cp.gameCode }">
							<my:game selected="${cp.gameCode }"/>
							</c:if>
						</div>
					</td>
				</tr>
				<tr>
					<td>類型:</td>
					<td>
						<select name="type">
							<option value="1" <c:if test="${cp.type == 1 }">selected="selected"</c:if>>系統</option>
							<option value="2" <c:if test="${cp.type == 2 }">selected="selected"</c:if>>活動</option>
							<option value="3" <c:if test="${cp.type == 3 }">selected="selected"</c:if>>攻略</option>
						</select>
					</td>
					<td>is Vip:</td>
					<td>
						<select name="isVip">
							<option value="1" <c:if test="${cp.isVip == 1 }">selected="selected"</c:if>>是</option>
							<option value="0" <c:if test="${cp.isVip == 2 }">selected="selected"</c:if>>否</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>serverCode:</td>
					<td><input name="serverCode" value="${cp.serverCode }"/></td>
					<td>位置:</td>
					<td>
						<select name="position">
							<option value="0" <c:if test="${cp.position == 0 }">selected="selected"</c:if>>平臺首頁</option>
							<option value="1" <c:if test="${cp.position == 1 }">selected="selected"</c:if>>其他</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>開始時間:</td>
					<td>
						<input type="text" value="${cp.startTime}" onfocus="WdatePicker({startTime:'%y-%M-%d 00:00:00', dateFmt:'yyyy-MM-dd HH:mm:ss'})"
											class="Wdate" name="startTime" id="startTime">
					</td>
					<td>結束時間:</td>
					<td>
						<input type="text" value="${cp.endTime}" onfocus="WdatePicker({endTime:'%y-%M-%d 23:59:59', dateFmt:'yyyy-MM-dd HH:mm:ss'})"
											class="Wdate" name="endTime" id="endTime">
					</td>
				</tr>				
				<tr>
					<td>順序:	</td>
					<td><input type="number" name="orderIndex" value="${cp.orderIndex }"/></td>
					<td></td>
					<td></td>
				</tr>
				<tr>					
					<td colspan="4">						
						<textarea style="width: 700px; height: 200px;" id="content" name="content">${cp.content }</textarea>					
					</td>
				</tr>				
			</table>
		</div>
	</form>
	<input type="button" value="保存" id="save" />
</div>

<script type="text/javascript">
	
	$(function() {
		keditor = KindEditor.create('textarea[name="content"]', {			
			resizeType : 1,
			langType : 'zh_TW',
			allowPreviewEmoticons : false,
			filterMode : false,
			allowFileManager : true,
			filePostName : "file",
			uploadJson  : '/fileUpload/save',
			extraFileUploadParams : {
	            module : "announcement"
	        },
	        afterBlur: function(){this.sync();},
			
			items : [ 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline', 'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist', 'insertunorderedlist', '|', 'emoticons', 'image',
					'link', '|', 'source', 'fullscreen', '|', 'preview' ]
		});		
		
		$.fn.select2.amd.require([ 'select2/compat/matcher' ], function(
				oldMatcher) {
			$("select[name='gameCode']").select2({
				matcher : oldMatcher(MyMatcher)
			});
		});
		
		$('#save').click(function() {
			$("#editForm").ajaxSubmit({
				success : function(e) {
					alert(e.message),
					$('#myModal').modal('hide');
					$('#myModal').on('hidden', function () {
					    // 关闭Dialog前移除编辑器
					    KindEditor.remove('textarea[name="content"]');
					});
					$('#search').click();
				},
				error : function() {
					alert("\u9023\u63a5\u7db2\u7d61\u5931\u6557");
				}
			});
		});
	});

	function changeArea(area) {
		var value = area.value;
		if (1 == value) {
			var money = $('#money').val();
			$('#tax').val(money * 0.05);
		}
		if (2 == value) {
			var money = $('#money').val();
			$('#tax').val(money * 0);
		}
		return true;
	}

	function changeMoney() {
		var area = $('#area').val();
		if (1 == area) {
			var money = $('#money').val();
			$('#tax').val(money * 0.05);
		}
		if (2 == area) {
			var money = $('#money').val();
			$('#tax').val(money * 0);
		}
		return true;
	}
	
</script>