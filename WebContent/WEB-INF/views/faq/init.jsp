<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
<head>
<title>8888play總後臺</title>
</head>
<body>
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel"></h4>
				</div>
				<div class="modal-body" id="myModalBody"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="modalSubmit">關閉</button>
				</div>
			</div>
		</div>
	</div>
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				基本資料 <small>常見問題</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="${ctx}/index"><i class="fa fa-dashboard"></i>8888play總後臺</a></li>
				<li class="active">常見問題</li>
			</ol>
		</section>
		<!-- Main content -->
		<div class="container-fluid">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body">
						<form action="${ctx}/faq/list" id="listForm" method="post">
							<input type="hidden" id="pageNo" name="pageNo" value="" />
							<div class="table-responsive">
							
								<table class="table">
									<tr>
										<td width="5%"><label for="inputid">類型:</label></td>
										<td width="10%">
											<select name="type">
												<option value="0">全部</option>
												<option value="4">安裝排除</option>
												<option value="2">儲值教學</option>
												<option value="1">帳號相關</option>
												<option value="5">盜用幫助</option>
												<option value="6">VIP問題</option>
												<option value="3">其它問題</option>
											</select>
			                            </td>
			                            <td></td>			                            
			                        </tr>
								</table>								
								<div class="btn-group col-sm-12 col-md-12 col-lg-6" role="group">
		                        	<input class="btn btn-primary" type="button" id="search" value="搜尋"/>
		                            <input class="btn btn-success" type="button" id="add" value="新增"/>		                            
		                        </div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div id="listContainer"></div>
		</div>
	</aside>

	<script type="text/javascript" src="${ctx }/js/jquery.form.js"></script>
	
	<script type="text/javascript">
		$(function() {
			$("#add").click(function() {
				$(".modal-dialog").css("width", "800px"),
				$(".modal-dialog").css("maxheight", "800px"),
				$("#myModal").modal("toggle"),
				$("#myModalLabel").html("新增"),				
				$.get("/faq/edit?id=", function(e) {
					$("#myModalBody").html(e);
				});
			});
			
			$('#search').click(function() {				
				$('#pageNo').val("1");
				$('#listContainer').html('<h1>正在搜尋，請稍候。。。</h1>');
				$('#listForm').ajaxSubmit({
					success : function(data) {
						$('#listContainer').html(data);
					}
				});
			});
			$('#export').click(function() {				
				var temp = $('#listForm').attr('action');
				console.log(temp);
				$('#listForm').attr('action', '${ctx}/device/export');
				$('#listForm').submit();
				$('#listForm').attr('action', temp);
			});
			$('#modalSubmit').click(function() {
				$('#myModal').modal('toggle');
				$('#search').click();
			});

		});

		function toPage(pageNo) {
			if (!pageNo || pageNo <= 0) {
				return;
			}
			$('#pageNo').val(pageNo);
			$('#listForm').ajaxSubmit({
				success : function(data) {
					$('#listContainer').html(data);
				},
				error : function() {
					alert('連接網絡失敗');
				}
			});
			$('#listContainer').html('<h1>正在搜尋，請稍候。。。</h1>');
		}
		
		function edit(id) {
			$(".modal-dialog").css("width", "800px"), 
			$(".modal-dialog").css("maxheight", "800px"), 
			$("#myModal").modal("toggle"), 
			$("#myModalLabel").html("編輯"),
			$.get("/faq/edit?id=" + id, function(e) {
				$("#myModalBody").html(e);
			});
		}
		
		function deleteF(id) {
			if (!confirm("確定刪除?")) {
				return;
			}
			$.ajax({
				url : '${ctx}/faq/delete',
				dataType : 'json',
				data : {
					'id' : id
				},
				success : function(data) {
					alert(data.message);
					$('#search').click();
				},
				error : function() {
					alert('網絡連接失敗，請聯繫管理員');
				}
			});
		}
		
	</script>
</body>
</html>