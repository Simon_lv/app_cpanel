<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	<form action="${ctx}/faq/save" method="post" id="editForm">
		<input type="hidden" name="id" value="${cp.id }"/>
		<div class="table-responsive">		
			<c:if test="${empty gameRecomm }">
			<table class="table">
				<tr>
					<td>類型：</td>
					<td>
						<select name="type">
							<option value="">選擇</option>
							<option value="4" <c:if test="${cp.type == 1 }">selected="selected"</c:if>>安裝排除</option>
							<option value="2" <c:if test="${cp.type == 2 }">selected="selected"</c:if>>儲值教學</option>
							<option value="1" <c:if test="${cp.type == 3 }">selected="selected"</c:if>>帳號相關</option>
							<option value="5" <c:if test="${cp.type == 4 }">selected="selected"</c:if>>盜用幫助</option>
							<option value="6" <c:if test="${cp.type == 5 }">selected="selected"</c:if>>VIP問題</option>
							<option value="3" <c:if test="${cp.type == 6 }">selected="selected"</c:if>>其它問題</option>
						</select>
					</td>
					<td>標題:</td>
					<td><input name="title" value="${cp.title }"/></td>			
				</tr>
				<tr>
					<td>順序:</td>
					<td><input type="number" name="orderIndex" value="${cp.orderIndex }"/></td>
					<td>位置:</td>
					<td>
						<select name="position">
							<option value="0" <c:if test="${cp.position == 0 }">selected="selected"</c:if>>全部</option>
							<option value="1" <c:if test="${cp.position == 1 }">selected="selected"</c:if>>平台</option>
							<option value="2" <c:if test="${cp.position == 2 }">selected="selected"</c:if>>SDK</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>內容:</td>
					<td colspan="3"><textarea name="content" id="content" style="width: 70%;">${cp.content }</textarea></td>
				</tr>
			</table>
			</c:if>
		</div>
	</form>
	<input type="button" value="保存" id="save" />
	<form name="photo" id="iconUploadForm" enctype="multipart/form-data" method="post">
		<div style="display: none">
			<input type="file" id="IconBrowse" name="file" style="border: 0;" accept="image/*" />
		</div>
	</form>		
</div>

<script type="text/javascript">
	
	$(document).ready(function(e) {
				
		$('#save').click(function() {
			
			if($('[name=type]').val() == ''){
				alert("請選擇類型");
				return;
			}
			
			$("#editForm").ajaxSubmit({
				success : function(e) {
					alert(e.message), $("#myModal").modal("toggle");
					$('#search').click();
				},
				error : function() {
					alert("\u9023\u63a5\u7db2\u7d61\u5931\u6557");
				}
			});
		});		
		
	});
	
</script>