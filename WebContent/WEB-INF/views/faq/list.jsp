<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>

<style type="text/css">
	.container {
      width: 100%;
      overflow: scroll; /* showing scrollbars */
	}
</style>

<div class="box-body table-responsive no-padding">
	<!-- portlet-body -->
	
	<div class="container">
	
	<table class="table table-bordered table-full-width" id="list">
		<thead>
			<tr>
				<th>ID</th>
				<th>類型</th>				
				<th>標題</th>
				<th>內容</th>	
				<td>位置</td>		
				<th>順序</th>
				<th>建立日期</th>
				<th>操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${page.result}" var="o">
				<tr>
					<td>${o.id }</td>
					<td>
						<c:choose>
							<c:when test="${o.type == 4 }">安裝排除</c:when>
							<c:when test="${o.type == 2 }">儲值教學</c:when>
							<c:when test="${o.type == 1 }">帳號相關</c:when>
							<c:when test="${o.type == 5 }">盜用幫助</c:when>
							<c:when test="${o.type == 6 }">VIP問題</c:when>
							<c:when test="${o.type == 3 }">其它問題</c:when>
						</c:choose>
					</td>					
					<td>${o.title }</td>
					<td>${o.content }</td>
					<td>
						<c:if test="${o.position == 0 }">全部</c:if>
						<c:if test="${o.position == 1 }">平台</c:if>
						<c:if test="${o.position == 2 }">SDK</c:if>
					</td>
					<td>${o.orderIndex }</td>
					<td>${o.createTime}</td>
					<td>
						<a href="javascript:;" onclick="edit(${o.id})">編輯</a>							
						<c:if test="${o.status == 1 }">
							<a href="javascript:;" onclick="deleteF(${o.id})">刪除</a>
						</c:if>
					</td>					
				</tr>
			</c:forEach>
		</tbody>

	</table>
	
	</div>
	
</div>
<my:pagination page="${page}" />