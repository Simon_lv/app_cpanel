<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
<head>
<title>8888play總後臺</title>
</head>
<body>
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				系統管理 <small>權限管理</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="${ctx}/index"><i class="fa fa-dashboard"></i>8888play總後臺</a></li>
				<li class="active">權限管理</li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<button class="btn btn-default" id="addBtn" type="button">新增</button>
			<div class="table-responsive">
				<table class="table table-striped table-hover table-bordered">
					<tr>
						<th>職務權限</th>
						<th>變更權限</th>
						<th>管理</th>
					</tr>
					<c:forEach items="${roles}" var="r">
						<tr>
							<td>${r.name}</td>
							<td><a href="javascript:void(0);"
								onclick="updateRole('${r.name}',${r.id});">修改</a></td>
							<td><c:if test="${r.status==1}">
									<a
										href="${ctx}/permission/updateRoleStatus?roleId=${r.id}&status=2">停權</a>
								</c:if> <c:if test="${r.status==2}">
									<a
										href="${ctx}/permission/updateRoleStatus?roleId=${r.id}&status=1">啟用</a>
								</c:if></td>
						</tr>
					</c:forEach>
				</table>
			</div>


			<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<form id="roleForm" action="/permission/addRole" method="post">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<h4 class="modal-title" id="myModalLabel"></h4>
							</div>
							<div class="modal-body">
								<div class="row">
									<div class="col-md-3">職務權限：</div>
									<div class="col-md-3">
										<input name="name" id="roleName"> <input name="roleId"
											id="roleId" type="hidden">
									</div>
								</div>
								<c:set var="rfIndex" value="0" />
								<c:forEach items="${functions}" var="fp">
									<c:if test="${fp.parentId==0}">
										<div class="row">
											<div class="col-md-12">
												${fp.name} <input type="checkbox" class="${fp.id} hidden"
													name="roleFunctions[${rfIndex}].functionId"
													value="${fp.id}">
												<c:set var="rfIndex" value="${rfIndex+1}" />
											</div>
										</div>
										<div class="row">
											<c:forEach items="${functions}" var="f">
												<c:if test="${f.parentId==fp.id}">
													<div class="col-md-4">
														<input type="checkbox" value="${f.id}" id="${f.id}"
															class="${f.id}" parentClass="${fp.id}"
															name="roleFunctions[${rfIndex}].functionId"> <label
															for="${f.id}">${f.name}</label>
														<c:set var="rfIndex" value="${rfIndex+1}" />
													</div>
												</c:if>
											</c:forEach>
										</div>
									</c:if>
								</c:forEach>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default"
									onclick="toggleModal();">取消</button>
								<input type="submit" class="btn btn-primary" value="送出">
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
	</aside>

	<script type="text/javascript" src="${ctx }/js/jquery.form.js"></script>
	<script type="text/javascript">
		$(function() {
			$('#addBtn').click(function() {
				addRole();
			});
			$('#myModal :checkbox').change(function(){
				var parentClass=$(this).attr('parentClass');
				var parent=$('.'+parentClass);
				var clazz=$(this).attr('class');
				$('.'+clazz).attr('checked',this.checked);
				parent.attr('checked',false);
				$('#myModal :checkbox[parentClass='+parentClass+']').each(function(idx, e){
					if(e.checked){
						parent.attr('checked',true);
						return;
					}
				});
			});
			$('#roleForm').submit(function() {
				if($('#roleName').val()==''){
					alert('請填寫職務權限！');
					return false;
				}
				
				var allow=false;
				$('#myModal :checkbox').each(function(idx,e){
					if(e.checked){
						allow=true;
						return;
					}
				});
				if(!allow){
					alert('請勾選權限');
					return false;
				}
				
				$('#roleForm').ajaxSubmit({
					success : function(data) {
						if(data.code==0){
							alert('操作成功！');
							location.reload(); 
						}
					},
					error : function() {
						alert('鏈接服務器失敗！');
					}
				});
				return false;
			});
		});
		function toggleModal() {
			$('#myModal').modal('toggle');
		}
		function addRole(){
			$('#roleForm').attr('action','/permission/addRole');
			$('#myModal :checkbox').attr('checked',false);
			$('#roleName').val('');
			toggleModal();
		}
		function updateRole(name,roleId){
			$('#roleForm').attr('action','/permission/updateRole');
			$.ajax({
				url:'/permission/getFunctions',
				data:'roleId='+roleId,
				dataType:'json',
				success:function(data){
					$('#myModal :checkbox').attr('checked',false);
					$('#roleName').val(name);
					$('#roleId').val(roleId);
					$('#myModal :checkbox[parentClass]').each(function(idx,e){
						for(var i=0;i<data.length;++i){
							if($(e).val()==data[i].id){
								$(e).click();
								break;
							}
						}
					});
					toggleModal();
				}
			});
		}
	</script>
</body>
</html>