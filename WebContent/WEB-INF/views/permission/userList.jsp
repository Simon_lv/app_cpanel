<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags/form"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
<head>
<title>8888play總後臺</title>
</head>
<body>
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				系統管理 <small>帳號管理</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="${ctx}/index"><i class="fa fa-dashboard"></i>8888play總後臺</a></li>
				<li class="active">帳號管理</li>
			</ol>
		</section>
		<!-- Main content -->
		<section class="content">
			<div class="panel panel-default">
				<div class="panel-body">
					<form action="${ctx}/permission/userList" method="post">
						<div class="row">
							<div class="col-md-2">帳號：</div>
							<div class="col-md-3">
								<input name="userName" value="${param.userName}">
							</div>
							<div class="col-md-1">
								<input type="submit" value="搜尋">
							</div>
							<div class="col-md-1">
								<input type="button" value="新增" id="addBtn">
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="table-responsive">
				<table class="table table-striped table-hover table-bordered">
					<tr>
						<th>職務權限</th>
						<th>帳號</th>
						<th>姓名</th>
						<th>手機</th>
						<th>編輯</th>
						<th>管理</th>
					</tr>
					<c:forEach items="${list}" var="u">
						<tr>
							<td>${u.name}</td>
							<td>${u.username}</td>
							<td>${u.funname}</td>
							<td>${u.mobile}</td>
							<td><a href="javascript:void(0);"
								onclick="getUser('${u.userid}');">編輯</a></td>
							<td><c:if test="${u.status==0}">
									<a
										href="${ctx}/permission/updateUserStatus?status=1&userId=${u.userid}">停權</a>
								</c:if> <c:if test="${u.status==1}">
									<a
										href="${ctx}/permission/updateUserStatus?status=0&userId=${u.userid}">啟用</a>
								</c:if></td>
						</tr>
					</c:forEach>
				</table>
			</div>
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body" id="myModalBody"></div>
						<div class="modal-footer">
							<a href="javascript:;" class="btn btn-default"
								data-dismiss="modal">关闭</a> <a href="javascript:;"
								id="submitForm" class="btn btn-primary">確定</a>
						</div>
					</div>
				</div>
			</div>
		</section>
	</aside>

	<script type="text/javascript" src="${ctx }/js/jquery.form.js"></script>
	<script type="text/javascript" src="${ctx }/js/md5.min.js"></script>
	<script type="text/javascript">
		var flag = true;
		$(function() {
			$('#addBtn').click(function() {
				getUser();
			});
			$("#submitForm").click(function() {
				$('#userForm').ajaxSubmit({
					beforeSubmit : function() {
						if ($('#funname').val() == '') {
							alert('請輸入姓名');
							return false;
						}
						if ($('#username').val() == '') {
							alert('請輸入帳號');
							return false;
						}
						if ($('#mobile').val() == '') {
							alert('請輸入手機');
							return false;
						}
						if (!flag && $('#password').val() == '') {
							alert('請輸入密碼');
							return false;
						}
					},
					success : function(data) {
						if (data.code == 0) {
							alert('操作成功！');
							$('#myModal').modal('toggle');
							location.href = "${ctx}/permission/userList";
						}
					}
				});
				return false;
			});
		});
		function getUser(userId) {
			$('#myModal').modal('toggle');
			$("#myModalBody").html("正在打開。。。");
			var url = "${ctx}/permission/getUser";
			if (userId) {
				url += "?id=" + userId;
			}
			$.get(url, function(data) {
				$("#myModalBody").html(data);
			});
		}
	</script>
</body>
</html>