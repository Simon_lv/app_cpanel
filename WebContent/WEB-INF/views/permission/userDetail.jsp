<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"></c:set>
<form id="userForm" action="${ctx}/permission/saveOrUpdate"
	method="post">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<h4 class="modal-title" id="myModalLabel"></h4>
	</div>
	<div class="modal-body">
		<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<tr>
					<td>職務權限</td>
					<td><c:if test="${user==null || user.roleId1!=null}">身份1：<select
								name="roleId1"><option value="">請選擇</option>
								<c:forEach items="${roles}" var="r">
									<option value="${r.id}"
										<c:if test="${user.roleId1==r.id}">selected="selected"</c:if>>${r.name}</option>
								</c:forEach>
							</select>
						</c:if> <c:if test="${user==null || user.roleId2!=null}">
					身份2：<select name="roleId2"><option value="">請選擇</option>
								<c:forEach items="${roles}" var="r2">
									<option value="${r2.id}"
										<c:if test="${user.roleId2==r2.id}">selected="selected"</c:if>>${r2.name}</option>
								</c:forEach>
							</select>
						</c:if> <c:if
							test="${user!= null&&user.roleId2==null && user.roleId2==null}">
					身份：<select name="roleId1"><option value="">請選擇</option>
								<c:forEach items="${roles}" var="r2">
									<option value="${r2.id}">${r2.name}</option>
								</c:forEach>
							</select>
						</c:if></td>
				</tr>
				<tr>
					<td>姓名</td>
					<td><input name="funname" id="funname" value="${user.funname}"></td>
				</tr>
				<tr>
					<td>帳號</td>
					<td><input name="username" id="username"
						value="${user.username}" /></td>
				</tr>
				<tr>
					<td>密碼</td>
					<td><input name="password" type="password" id="password" /></td>
				</tr>
				<tr>
					<td>手機</td>
					<td><input name="mobile" id="mobile" value="${user.mobile}" /></td>
				</tr>
			</table>
		</div>
		<input type="hidden" name="userId" id="userId" value="${user.userId}" />
	</div>
</form>